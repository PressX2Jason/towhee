package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class PrintStatementNode extends ParseNode {

	public PrintStatementNode(Token token)
	{
		super(token);
		assert (token.isLextant(Keyword.PRINT, Keyword.PRINTLN, Keyword.SPLAT, Keyword.SPLATLN));
	}

	public PrintStatementNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// attributes

	public boolean hasSpaces()
	{
		return token.isLextant(Keyword.PRINT, Keyword.PRINTLN);
	}

	public boolean hasNewline()
	{
		return token.isLextant(Keyword.PRINTLN, Keyword.SPLATLN);
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
