package parseTree.nodeTypes;

import java.util.ArrayList;
import java.util.List;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class CallStatementNode extends ParseNode implements FunctionNode{

	public CallStatementNode(Token token)
	{
		super(token);
		assert token.isLextant(Keyword.CALL);
	}
	
	public CallStatementNode(ParseNode node)
	{
		super(node);
	}
	
	// /////////////////////////////////////////////////////////
	// attributes
	
	public List<ParseNode> getArguments(){
		List<ParseNode> arguements = new ArrayList<ParseNode>();
		for(int i = 1; i < nChildren(); i++){
			arguements.add(child(i));
		}
		return arguements;
	}
	
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
