package parseTree.nodeTypes;

import lexicalAnalyzer.Lextant;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import tokens.LextantToken;
import tokens.Token;

public class UnaryOperatorNode extends OperatorNode {

	private Type ResultType;

	public UnaryOperatorNode(Token token)
	{
		super(token);
		assert (token instanceof LextantToken);

	}

	public UnaryOperatorNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// attributes

	public Lextant getOperator()
	{
		return lextantToken().getLextant();
	}

	public LextantToken lextantToken()
	{
		return (LextantToken) token;
	}
	
	public Type getResultType()
	{
		return ResultType;
	}

	public void setResultType(Type resultType)
	{
		assert (resultType instanceof PrimitiveType);
		ResultType = resultType;
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static UnaryOperatorNode withChild(Token token, ParseNode child, Type resultType)
	{
		UnaryOperatorNode node = new UnaryOperatorNode(token);
		node.appendChild(child);
		node.setResultType(resultType);
		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
