package parseTree.nodeTypes;

import java.util.ArrayList;
import java.util.List;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.Type;
import tokens.Token;

public class ContainerDeclarationNode extends ParseNode {

	private List<Integer>	referenceVariablesOffsetList	= new ArrayList<Integer>();

	public ContainerDeclarationNode(Token token)
	{
		super(token);
	}

	public ContainerDeclarationNode(ParseNode node)
	{
		super(node);
	}

	// /////////////////////////////////////////////////////////
	// attributes

	public List<FunctionDeclarationNode> getContainedFunctions()
	{
		List<FunctionDeclarationNode> functionList = new ArrayList<FunctionDeclarationNode>();
		for (ParseNode child : getChildren()) {
			if (child instanceof FunctionDeclarationNode) {
				functionList.add((FunctionDeclarationNode) child);
			}
		}
		return functionList;
	}

	public List<Integer> getReferenceVariablesOffsetList()
	{
		return referenceVariablesOffsetList;
	}

	public ParseNode getBodyNode()
	{
		return (child(1) instanceof ParameterListNode) ? child(2) : child(1);
	}

	public boolean hasParameters()
	{
		for (ParseNode child : getChildren()) {
			if (child instanceof ParameterListNode) {
				return child.nChildren() > 0;
			}
		}
		return false;
	}

	public List<Type> getParameterTypes()
	{
		List<Type> paramTypeList = new ArrayList<Type>();

		if (!hasParameters()) {
			return paramTypeList;
		}

		for (ParseNode child : child(1).getChildren()) {
			paramTypeList.add(child.getType());
		}

		return paramTypeList;
	}
	
	public List<ParseNode> getParameters()
	{
		List<ParseNode> paramTypeList = new ArrayList<ParseNode>();

		if (!hasParameters()) {
			return paramTypeList;
		}

		for (ParseNode child : child(1).getChildren()) {
			paramTypeList.add(child);
		}

		return paramTypeList;
	}
	
	public int getParamterScopeSize(){
		return getScope().getAllocatedSize();
	}

	public List<ParseNode> getContainedVariables()
	{
		List<ParseNode> functionList = new ArrayList<ParseNode>();
		for (ParseNode child : getBodyNode().getChildren()) {
			if (child instanceof DeclarationNode) {
				functionList.add(child.child(0));
			}
		}
		return functionList;
	}
	
	public int getContainedVariableSize()
	{
		int size = 0;
		for (ParseNode child : getBodyNode().getChildren()) {
			if (child instanceof DeclarationNode) {
				size += child.getType().getSize();
			}
		}
		return size;
	}
	
	
	public boolean isMainContainer(){
		IdentifierNode name = (IdentifierNode) child(0);
		return name.getIdentifier().equals("main");
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
