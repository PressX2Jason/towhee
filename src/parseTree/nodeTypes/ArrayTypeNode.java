package parseTree.nodeTypes;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class ArrayTypeNode extends ParseNode {

	public ArrayTypeNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.OPEN_SQUARE_BRACKET);
	}

	public ArrayTypeNode(ParseNode node)
	{
		super(node);
		assert node instanceof ArrayTypeNode || node instanceof BooleanConstantNode || node instanceof FloatConstantNode || node instanceof IntegerConstantNode
				|| node instanceof CharacterConstantNode;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
