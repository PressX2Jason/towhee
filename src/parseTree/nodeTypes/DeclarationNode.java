package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.LextantToken;
import tokens.Token;

public class DeclarationNode extends ParseNode {

	private boolean	isMutable;

	public DeclarationNode(Token token)
	{
		super(token);
		assert (token.isLextant(Keyword.IMMUTABLE, Keyword.MUTABLE));
		if (token.isLextant(Keyword.IMMUTABLE)) {
			isMutable = false;
		}
		else if (token.isLextant(Keyword.MUTABLE)) {
			isMutable = true;
		}
	}

	public DeclarationNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// attributes

	public Lextant getDeclarationType()
	{
		return lextantToken().getLextant();
	}

	public LextantToken lextantToken()
	{
		return (LextantToken) token;
	}

	public boolean isMutable()
	{
		return isMutable;
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static DeclarationNode withChildren(Token token, ParseNode declaredName, ParseNode initializer)
	{
		DeclarationNode node = new DeclarationNode(token);
		node.appendChild(declaredName);
		node.appendChild(initializer);
		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
