package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class ReturnStatementNode extends ParseNode {

	public ReturnStatementNode(Token token)
	{
		super(token);
		assert token.isLextant(Keyword.RETURN);
	}
	
	public ReturnStatementNode(ParseNode node)
	{
		super(node);
	}
	
	// //////////////////////////////////////////////////////////
	// attribute
	
	public ValueBodyNode findOwner(){
		for(ParseNode parent : pathToRoot()){
			if(parent instanceof ValueBodyNode){
				return (ValueBodyNode) parent;
			}
		}
		return null;
	}
	
	// //////////////////////////////////////////////////////////
	// convenience factory

	public static ReturnStatementNode withChild(Token token, ParseNode body)
	{
		ReturnStatementNode node = new ReturnStatementNode(token);
		node.appendChild(body);
		return node;
	}
	
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
