package parseTree.nodeTypes;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class ArrayIndexNode extends ParseNode {

	public ArrayIndexNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.OPEN_SQUARE_BRACKET);
	}
	
	// //////////////////////////////////////////////////////////
	// convenience factory
	
	public static ArrayIndexNode withChildren(Token token, ParseNode right, ParseNode left)
	{
		ArrayIndexNode node = new ArrayIndexNode(token);		
		node.appendChild(right);
		node.appendChild(left);
		return node;
	}
	
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
