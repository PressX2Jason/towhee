package parseTree.nodeTypes;

import parseTree.ParseNode;
import semanticAnalyzer.FunctionSignature;
import tokens.Token;

public class OperatorNode extends ParseNode {
	
	protected FunctionSignature	signature;

	public OperatorNode(ParseNode node)
	{
		super(node);
	}
	
	public OperatorNode(Token token)
	{
		super(token);
	}
	
	// //////////////////////////////////////////////////////////
	// attributes
	
	public FunctionSignature getSignature()
	{
		return signature;
	}

	public void setSignature(FunctionSignature signature)
	{
		this.signature = signature;
	}

}
