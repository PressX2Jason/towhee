package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class UpdateNode extends ParseNode {

	public UpdateNode(Token token)
	{
		super(token);
		assert (token.isLextant(Keyword.UPDATE));
	}

	public UpdateNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static UpdateNode withChildren(Token token, ParseNode declaredName, ParseNode updatedValue)
	{
		UpdateNode node = new UpdateNode(token);
		node.appendChild(declaredName);
		node.appendChild(updatedValue);
		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
