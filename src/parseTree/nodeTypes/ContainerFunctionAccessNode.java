package parseTree.nodeTypes;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import tokens.Token;

public class ContainerFunctionAccessNode extends ParseNode {

	public ContainerFunctionAccessNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.MEMBER_ACCESS);
	}
	
	public ContainerFunctionAccessNode(ParseNode node)
	{
		super(node);
	}



}
