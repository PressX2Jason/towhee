package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.ArrayType;
import semanticAnalyzer.Type;
import tokens.Token;

public class FunctionDeclarationNode extends ParseNode {

	public FunctionDeclarationNode(Token token)
	{
		super(token);
		assert token.isLextant(Keyword.DEFINE);
	}

	public FunctionDeclarationNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// attributes

	public Type getDeclaredReturnType()
	{
		return getChildType(child(2));
	}

	public Type getChildType(ParseNode child)
	{
		if (child instanceof ArrayTypeNode) {
			return new ArrayType(getChildType(child.child(0)));
		}
		else {
			return child.getType();
		}
	}

	public Type getValueBodyReturnType()
	{
		return getChildType(child(3));
	}

	public int getArgumentsSize()
	{
		int counter = 0;
		for (ParseNode argument : child(3).getChildren()) {
			counter += argument.getType().getSize();
		}
		return counter;
	}

	public int getParameterScope()
	{
		return getScope().getAllocatedSize();
	}

	public int getProcedureScopeSize()
	{
		return child(3).getScope().getAllocatedSize();
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static FunctionDeclarationNode withChildren(Token token, ParseNode identifier, ParseNode returnType, ParseNode valueBody)
	{
		FunctionDeclarationNode node = new FunctionDeclarationNode(token);
		assert identifier instanceof IdentifierNode;
		node.appendChild(identifier);

		assert returnType instanceof TypeConstantNode;
		node.appendChild(returnType);

		assert valueBody instanceof ValueBodyNode;
		node.appendChild(valueBody);

		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
