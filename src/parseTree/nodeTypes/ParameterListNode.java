package parseTree.nodeTypes;

import java.util.ArrayList;
import java.util.List;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.ArrayType;
import semanticAnalyzer.Type;
import tokens.Token;

public class ParameterListNode extends ParseNode {

	public ParameterListNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.OPEN_PARENTHESE);
	}
	
	public ParameterListNode(ParseNode node)
	{
		super(node);
	}
	// /////////////////////////////////////////////////////////
	// attributes
	
	public List<Type> getFunctionSignature(){
		List<Type> signature = new ArrayList<Type>();
		
		for(ParseNode child : getChildren()){
			signature.add(getChildType(child));
		}
		return signature;
	}

	private Type getChildType(ParseNode child)
	{
		if(child.child(0) instanceof ArrayTypeNode){
			return new ArrayType(getChildType(child.child(0)));
		}
		return child.child(0).getType();
	}
		
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
