package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import tokens.Token;

public class TypeConstantNode extends ParseNode {

	private Type	type;

	public TypeConstantNode(Token token)
	{
		super(token);
		assert (token.isLextant(Keyword.BOOLEAN, Keyword.CHAR, Keyword.INTEGER, Keyword.FLOAT, Keyword.VOID));
		
		if (token.isLextant(Keyword.BOOLEAN)) {
			type = PrimitiveType.BOOLEAN;
		}
		else if (token.isLextant(Keyword.CHAR)) {
			type = PrimitiveType.CHAR;
		}
		else if (token.isLextant(Keyword.INTEGER)) {
			type = PrimitiveType.INTEGER;
		}
		else if (token.isLextant(Keyword.FLOAT)) {
			type = PrimitiveType.FLOAT;
		}else if (token.isLextant(Keyword.VOID)){
			type = PrimitiveType.VOID;
		}
	}

	public TypeConstantNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// attributes
	
	public Type getType(){
		return type;
	}
	
	// /////////////////////////////////////////////////////////
	// accept a visitor

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visit(this);
	}
}
