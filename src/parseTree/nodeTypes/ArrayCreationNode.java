package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class ArrayCreationNode extends ParseNode {

	private boolean	persist	= false;

	public ArrayCreationNode(Token arrayToken)
	{
		super(arrayToken);
		assert token.isLextant(Punctuator.OPEN_SQUARE_BRACKET, Punctuator.AT_SIGN);
	}

	// /////////////////////////////////////////////////////////
	// attributes

	public boolean isPersistant()
	{
		return persist;
	}

	public void setPersistToken(Token token)
	{
		if (token == null) {
			return;
		}
		assert token.isLextant(Keyword.PERSIST);
		persist = true;
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static ArrayCreationNode createEmptyArray(Token arrayToken, Token persistToken, ParseNode arrayType, ParseNode arrayLength, ParseNode lowerIndex)
	{
		assert (arrayLength != null);

		ArrayCreationNode node = new ArrayCreationNode(arrayToken);
		node.setPersistToken(persistToken);
		node.appendChild(arrayType);
		if (lowerIndex != null) {
			node.appendChild(lowerIndex);
		}
		node.appendChild(arrayLength);
		return node;
	}

	public static ArrayCreationNode createPopulatedArray(Token arrayToken, Token persistToken)
	{
		ArrayCreationNode node = new ArrayCreationNode(arrayToken);
		node.setPersistToken(persistToken);
		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	@Override
	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
