package parseTree.nodeTypes;

import lexicalAnalyzer.Keyword;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class WhileStatementNode extends ParseNode {
	
	public WhileStatementNode(Token token)
	{
		super(token);
		assert (token.isLextant(Keyword.WHILE));
	}
	
	public WhileStatementNode(ParseNode node)
	{
		super(node);
	}

	// //////////////////////////////////////////////////////////
	// convenience factory

	public static WhileStatementNode withChildren(Token token, ParseNode condition, ParseNode whileBody)
	{
		WhileStatementNode node = new WhileStatementNode(token);
		node.appendChild(condition);
		node.appendChild(whileBody);
		return node;
	}

	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
