package parseTree.nodeTypes;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class MemberAccessNode extends ParseNode {
	
	private Members member;
	
	public enum Members{
		LOW,
		HIGH,
		LENGTH,
		INVALID;
	}

	public MemberAccessNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.MEMBER_ACCESS);
	}
	
	// //////////////////////////////////////////////////////////
	// convenience factory

	public static MemberAccessNode withChildren(Token token, ParseNode expression, ParseNode identifier)
	{
		MemberAccessNode node = new MemberAccessNode(token);
		node.appendChild(expression);
		node.appendChild(identifier);
		return node;
	}
	
	// /////////////////////////////////////////////////////////
	// Attributes
	
	public Members getMember()
	{
		return member;
	}

	public void setMember(Members member)
	{
		this.member = member;
	}
	
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}

}
