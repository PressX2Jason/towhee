package parseTree.nodeTypes;

import java.util.ArrayList;
import java.util.List;

import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import tokens.Token;

public class ContainerCreationNode extends ParseNode {

	private boolean	persist = false;

	public ContainerCreationNode(Token arrayToken)
	{
		super(arrayToken);
		assert token.isLextant(Punctuator.AT_SIGN);
	}

	// /////////////////////////////////////////////////////////
	// Attributes

	public boolean isPersistant()
	{
		return persist;
	}

	public void setPersistToken(Token token)
	{
		if(token == null){
			return;
		}
		assert token.isLextant(Keyword.PERSIST);
		persist = true;
	}
	
	public List<Type> getParameterTypes(){
		List<Type> typeList = new ArrayList<Type>();
		
		if(nChildren() == 1){
			typeList.add(PrimitiveType.VOID);
			return typeList;
		}
		
		for(int i = 1; i < nChildren(); i++){
			typeList.add(child(i).getType());
		}
		return typeList;
	}
	public int getCreationParamterSize(){
		int size = 0;
		
		if(nChildren() == 1){
			return 0;
		}
		
		for(int i = 1; i < nChildren(); i++){
			size =+ child(i).getType().getSize();
		}
		return size;
	}
	
	public List<ParseNode> getArguments(){
		List<ParseNode> argumentList = new ArrayList<ParseNode>();
		
		if(nChildren() == 1){
			return argumentList;
		}
		
		for(int i = 1; i < nChildren(); i++){
			argumentList.add(child(i));
		}
		return argumentList;
	}
	
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}