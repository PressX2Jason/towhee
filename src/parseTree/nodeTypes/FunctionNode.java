package parseTree.nodeTypes;

import java.util.List;

import parseTree.ParseNode;
import semanticAnalyzer.Type;
import symbolTable.Binding;

public interface FunctionNode {
	
	public List<ParseNode> getArguments();
	
	public Type getType();

	public Iterable<ParseNode> pathToRoot();

	public Binding getBinding();

	public ParseNode child(int i);
	
}
