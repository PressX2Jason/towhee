package parseTree.nodeTypes;

import java.util.ArrayList;
import java.util.List;

import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class ValueBodyNode extends ParseNode {
	
	private String endBodyLabel = "";
	private List<ReturnStatementNode> returnStatementList = new ArrayList<ReturnStatementNode>();

	public ValueBodyNode(Token token)
	{
		super(token);
		assert token.isLextant(Punctuator.OPEN_VALUE_BODY);
	}
	
	public ValueBodyNode(ParseNode node)
	{
		super(node);
	}
	
	// /////////////////////////////////////////////////////////
	// attributes
	
	public boolean lastChildIsReturnStmt(){
		if(this.nChildren() == 0 ){
			return false;
		}else{
			if (child(nChildren() -1) instanceof ReturnStatementNode){
				return true;
			}
		}
		return false;
	}
	
	public String getEndBodyLabel()
	{
		assert !endBodyLabel.equals("");
		return endBodyLabel;
	}

	public void setEndBodyLabel(String endBodyLabel)
	{
		this.endBodyLabel = endBodyLabel;
	}
	
	public List<ReturnStatementNode> getReturnStatement()
	{
		return returnStatementList;
	}

	public void addReturnStatement(ReturnStatementNode returnStatement)
	{
		returnStatementList.add(returnStatement);
	}	
		
	// /////////////////////////////////////////////////////////
	// boilerplate for visitors

	public void accept(ParseNodeVisitor visitor)
	{
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
