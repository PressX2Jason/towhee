package parseTree;

import parseTree.nodeTypes.ArrayCreationNode;
import parseTree.nodeTypes.ArrayIndexNode;
import parseTree.nodeTypes.ArrayTypeNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BodyNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallStatementNode;
import parseTree.nodeTypes.CharacterConstantNode;
import parseTree.nodeTypes.ContainerCreationNode;
import parseTree.nodeTypes.ContainerDeclarationNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.FloatConstantNode;
import parseTree.nodeTypes.FunctionDeclarationNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IfStatementNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.MemberAccessNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReleaseStatementNode;
import parseTree.nodeTypes.ReturnStatementNode;
import parseTree.nodeTypes.TypeConstantNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.UpdateNode;
import parseTree.nodeTypes.ValueBodyNode;
import parseTree.nodeTypes.WhileStatementNode;

// Visitor pattern with pre- and post-order visits
public interface ParseNodeVisitor {

	// non-leaf nodes: visitEnter and visitLeave
	void visitEnter(BinaryOperatorNode node);

	void visitLeave(BinaryOperatorNode node);

	void visitEnter(UnaryOperatorNode node);

	void visitLeave(UnaryOperatorNode node);

	void visitEnter(BodyNode node);

	void visitLeave(BodyNode node);

	void visitEnter(DeclarationNode node);

	void visitLeave(DeclarationNode node);

	void visitEnter(UpdateNode node);

	void visitLeave(UpdateNode node);

	void visitEnter(MemberAccessNode node);

	void visitLeave(MemberAccessNode node);

	void visitEnter(IfStatementNode node);

	void visitLeave(IfStatementNode node);

	void visitEnter(WhileStatementNode node);

	void visitLeave(WhileStatementNode node);

	void visitEnter(ParseNode node);

	void visitLeave(ParseNode node);

	void visitEnter(PrintStatementNode node);

	void visitLeave(PrintStatementNode node);

	void visitEnter(ReleaseStatementNode node);

	void visitLeave(ReleaseStatementNode node);

	void visitEnter(ArrayCreationNode node);

	void visitLeave(ArrayCreationNode node);
	
	void visitEnter(ContainerCreationNode node);

	void visitLeave(ContainerCreationNode node);

	void visitEnter(ArrayIndexNode node);

	void visitLeave(ArrayIndexNode node);

	void visitEnter(ArrayTypeNode node);

	void visitLeave(ArrayTypeNode node);

	void visitEnter(ProgramNode node);

	void visitLeave(ProgramNode node);

	void visitEnter(ReturnStatementNode node);

	void visitLeave(ReturnStatementNode node);

	void visitEnter(ValueBodyNode node);

	void visitLeave(ValueBodyNode node);

	void visitEnter(FunctionDeclarationNode node);

	void visitLeave(FunctionDeclarationNode node);

	void visitEnter(ParameterListNode node);

	void visitLeave(ParameterListNode node);

	void visitEnter(CallStatementNode node);

	void visitLeave(CallStatementNode node);

	void visitEnter(FunctionInvocationNode node);

	void visitLeave(FunctionInvocationNode node);
	
	void visitEnter(ContainerDeclarationNode node);

	void visitLeave(ContainerDeclarationNode node);

	// leaf nodes: visitLeaf only
	void visit(BooleanConstantNode node);

	void visit(ErrorNode node);

	void visit(IdentifierNode node);

	void visit(IntegerConstantNode node);

	void visit(FloatConstantNode floatConstantNode);

	void visit(CharacterConstantNode node);

	void visit(TypeConstantNode node);

	public static class Default implements ParseNodeVisitor {
		public void defaultVisit(ParseNode node)
		{
		}

		public void defaultVisitEnter(ParseNode node)
		{
			defaultVisit(node);
		}

		public void defaultVisitLeave(ParseNode node)
		{
			defaultVisit(node);
		}

		public void defaultVisitForLeaf(ParseNode node)
		{
			defaultVisit(node);
		}

		public void visitEnter(BinaryOperatorNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(BinaryOperatorNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(UnaryOperatorNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(UnaryOperatorNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(BodyNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(BodyNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(DeclarationNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(DeclarationNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(UpdateNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(UpdateNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(MemberAccessNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(MemberAccessNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(IfStatementNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(IfStatementNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(WhileStatementNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(WhileStatementNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ParseNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ParseNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(PrintStatementNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(PrintStatementNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ReleaseStatementNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ReleaseStatementNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ArrayCreationNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ArrayCreationNode node)
		{
			defaultVisitLeave(node);
		}
		

		@Override
		public void visitEnter(ContainerCreationNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(ContainerCreationNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ArrayIndexNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ArrayIndexNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ArrayTypeNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ArrayTypeNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ProgramNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ProgramNode node)
		{
			defaultVisitLeave(node);
		}

		public void visitEnter(ReturnStatementNode node)
		{
			defaultVisitEnter(node);
		}

		public void visitLeave(ReturnStatementNode node)
		{
			defaultVisitLeave(node);
		}

		@Override
		public void visitEnter(ValueBodyNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(ValueBodyNode node)
		{
			defaultVisitLeave(node);
		}

		@Override
		public void visitEnter(FunctionDeclarationNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(FunctionDeclarationNode node)
		{
			defaultVisitLeave(node);
		}

		@Override
		public void visitEnter(ParameterListNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(ParameterListNode node)
		{
			defaultVisitLeave(node);
		}

		@Override
		public void visitEnter(CallStatementNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(CallStatementNode node)
		{
			defaultVisitLeave(node);
		}

		@Override
		public void visitEnter(FunctionInvocationNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(FunctionInvocationNode node)
		{
			defaultVisitLeave(node);
		}
		
		@Override
		public void visitEnter(ContainerDeclarationNode node)
		{
			defaultVisitEnter(node);
		}

		@Override
		public void visitLeave(ContainerDeclarationNode node)
		{
			defaultVisitLeave(node);
		}

		// leafs
		public void visit(BooleanConstantNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(ErrorNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(IdentifierNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(IntegerConstantNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(FloatConstantNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(CharacterConstantNode node)
		{
			defaultVisitForLeaf(node);
		}

		public void visit(TypeConstantNode node)
		{
			defaultVisitForLeaf(node);
		}

	}
}
