package tests;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

import junit.framework.TestCase;

public abstract class FileFixturesTestCase extends TestCase {
	public interface Command {
		void run(PrintStream out) throws Exception;
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// string i/o

	public String outputFor(Command command) throws Exception
	{
		System.setProperty("line.separator", "\r\n"); // Path separator character used in
														// java.class.path
		/*
		 * This class implements an output stream in which the data is written into a byte array. The buffer automatically grows as data is written to it. The data can be retrieved using toByteArray()
		 * and toString().
		 */
		OutputStream byteArrayOS = new ByteArrayOutputStream();
		/*
		 * A PrintStream adds functionality to another output stream, namely the ability to print representations of various data values conveniently. Two other features are provided as well. Unlike
		 * other output streams, a PrintStream never throws an IOException; instead, exceptional situations merely set an internal flag that can be tested via the checkError method. Optionally, a
		 * PrintStream can be created so as to flush automatically; this means that the flush method is automatically invoked after a byte array is written, one of the println methods is invoked, or a
		 * newline character or byte ('\n') is written. All characters printed by a PrintStream are converted into bytes using the platform's default character encoding. The PrintWriter class should
		 * be used in situations that require writing characters rather than bytes.
		 */
		PrintStream out = new PrintStream(byteArrayOS);
		command.run(out);
		return byteArrayOS.toString();
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// file i/o

	public InputStreamReader readerForFilename(String filename) throws FileNotFoundException
	{
		FileInputStream sourceStream = new FileInputStream(filename);
		return new InputStreamReader(sourceStream);
	}

	public String getContents(String filename) throws IOException
	{
		InputStreamReader reader = readerForFilename(filename);
		return contentsAsString(reader);
	}

	private String contentsAsString(InputStreamReader reader) throws IOException
	{
		StringBuffer result = new StringBuffer();
		char[] buffer = new char[1024];
		int amount;
		while ((amount = reader.read(buffer)) != -1) {
			result.append(buffer, 0, amount);
		}
		reader.close();

		return result.toString();
	}
}
