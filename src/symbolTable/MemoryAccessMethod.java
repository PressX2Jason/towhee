package symbolTable;

import static asmCodeGenerator.codeStorage.ASMOpcode.Add;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadI;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushD;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public enum MemoryAccessMethod
{
	NULL_ACCESS()
	{
		public void generateAddress(ASMCodeFragment code, String baseAddress, int offset, String comment, int n)
		{
			code.add(PushI, 0, comment);
		}
	},

	// base is the label of the start of the memory block.
	DIRECT_ACCESS_BASE()
	{
		protected void generateBaseAddress(ASMCodeFragment code, String baseAddress, int n)
		{
			code.add(PushD, baseAddress);
		}
	},

	// base is the label of the memory holding a pointer to the start of the memory block.
	INDIRECT_ACCESS_BASE()
	{
		protected void generateBaseAddress(ASMCodeFragment code, String baseAddress, int n)
		{
			code.add(PushD, baseAddress);
			for (int i = 0; i < n + 1; i++) {
				code.add(LoadI);
			}
		}
	},

	CONTAINER_ACCESS_BASE()
	{
		protected void generateBaseAddress(ASMCodeFragment code, String baseAddress, int n)
		{
			code.add(PushD, baseAddress);
			code.add(LoadI);
			code.add(PushI, 4);
			code.add(Add);
			code.add(LoadI);
			code.add(PushI, 8);
			code.add(Add);
		}
	};

	public void generateAddress(ASMCodeFragment code, String baseAddress, int offset, String comment, int n)
	{
		generateBaseAddress(code, baseAddress, n);
		code.add(PushI, offset);
		code.add(Add, "", comment);
	}

	public void generateAddress(ASMCodeFragment code, String baseAddress, int offset, int n)
	{
		generateAddress(code, baseAddress, offset, "", n);
	}

	protected void generateBaseAddress(ASMCodeFragment code, String baseAddress, int n)
	{
	}

}
