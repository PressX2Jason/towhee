package symbolTable;

import inputHandler.TextLocation;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import asmCodeGenerator.ASMCodeGenerator;
import asmCodeGenerator.Labeller;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class Binding {
	private Type			type;
	private TextLocation	textLocation;
	private MemoryLocation	memoryLocation;
	private String			lexeme;
	private String			uniqueLabel = "";
	private boolean			mutable;

	public Binding(Type type, TextLocation location, MemoryLocation memoryLocation, String lexeme, boolean mutable)
	{
		super();
		this.type = type;
		this.textLocation = location;
		this.memoryLocation = memoryLocation;
		this.lexeme = lexeme;
		this.mutable = mutable;
	}

	public String toString()
	{
		return "[" + lexeme + " " + type + // " " + textLocation +
				" " + memoryLocation + "]";
	}

	public String getLexeme()
	{
		return lexeme;
	}

	public Type getType()
	{
		return type;
	}

	public TextLocation getLocation()
	{
		return textLocation;
	}

	public MemoryLocation getMemoryLocation()
	{
		return memoryLocation;
	}

	public boolean isMutable()
	{
		return mutable;
	}

	public void generateAddress(ASMCodeFragment code, int snl)
	{
		//System.out.print(lexeme);
		memoryLocation.generateAddress(code, "%% " + lexeme, snl);
	}
	
	public String getUniqueLabel(){
		makeUniqueLabel();
		return uniqueLabel;
	}

	private void makeUniqueLabel()
	{
		if(!uniqueLabel.equals("")){
			return;
		}
		Labeller labeller = ASMCodeGenerator.getLabeller();
		uniqueLabel = labeller.newLabel("$-" + lexeme + "-start", "");
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// Null Binding object
	// //////////////////////////////////////////////////////////////////////////////////

	public static Binding nullInstance()
	{
		return NullBinding.getInstance();
	}

	private static class NullBinding extends Binding {
		private static NullBinding	instance	= null;

		private NullBinding()
		{
			super(PrimitiveType.ERROR, TextLocation.nullInstance(), MemoryLocation.nullInstance(), "the-null-binding", false);
		}

		public static NullBinding getInstance()
		{
			if (instance == null) instance = new NullBinding();
			return instance;
		}
	}
}
