package symbolTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import logging.TowheeLogger;
import semanticAnalyzer.ArrayType;
import tokens.Token;

public class SymbolTable {
	private Map<String, Binding>	table;

	public SymbolTable()
	{
		table = new HashMap<String, Binding>();
	}

	// //////////////////////////////////////////////////////////////
	// installation and lookup of identifiers

	public Binding install(String identifier, Binding binding)
	{
		table.put(identifier, binding);
		return binding;
	}

	public Binding lookup(String identifier)
	{
		Binding binding = table.get(identifier);
		if (binding == null) {
			return Binding.nullInstance();
		}
		return binding;
	}
	
	public String definedSymbols()
	{
		String result = "";
		for(String id : table.keySet()){
			result += id + " ";
		}
		return result;
	}

	// /////////////////////////////////////////////////////////////////////
	// Map delegates

	public boolean containsKey(String identifier)
	{
		return table.containsKey(identifier);
	}

	public Set<String> keySet()
	{
		return table.keySet();
	}

	public Collection<Binding> values()
	{
		return table.values();
	}

	public boolean keyIsMutable(Token token)
	{
		Binding binding = table.get(token.getLexeme());
		return binding.isMutable();
	}
	
	public List<Binding> getAllArrays(){
		List<Binding> bindings = new ArrayList<Binding>();
		for(Binding binding : table.values()){
			if(binding.getType() instanceof ArrayType){
				bindings.add(binding);
			}
		}
		return bindings;
	}
	

	// /////////////////////////////////////////////////////////////////////
	// error reporting

	public void errorIfAlreadyDefined(Token token)
	{
		if (containsKey(token.getLexeme())) {
			multipleDefinitionError(token);
		}
	}

	protected static void multipleDefinitionError(Token token)
	{
		TowheeLogger log = TowheeLogger.getLogger("complier.symbolTable");
		log.severe("variable \"" + token.getLexeme() + "\" multiply defined at " + token.getLocation());
	}

	public static void errorIfSymbolImmuatable(Token token)
	{
		TowheeLogger log = TowheeLogger.getLogger("complier.symbolTable");
		log.severe("final variable \"" + token.getLexeme() + "\" cannot be updated at " + token.getLocation());
	}

	// /////////////////////////////////////////////////////////////////////
	// toString

	public String toString()
	{
		String result = "    symbol table: \n";
		for (Entry<String, Binding> entry : table.entrySet()) {
			result += "        " + entry + "\n";
		}
		return result;
	}
}
