package symbolTable;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.runtime.RunTime;

public class MemoryLocation {
	public static final String	GLOBAL_VARIABLE_BLOCK	= RunTime.GLOBAL_MEMORY_BLOCK;
	public static final String	FRAME_POINTER			= RunTime.FRAME_POINTER;

	private MemoryAccessMethod	accessor;
	private String				baseAddress;
	private int					offset;
	private int					staticNestingLevel;

	public MemoryLocation(MemoryAccessMethod accessor, String baseAddress, int offset, int staticNestingLevel)
	{
		super();
		this.accessor = accessor;
		this.baseAddress = baseAddress;
		this.offset = offset;
		this.staticNestingLevel = staticNestingLevel;
	}

	public MemoryAccessMethod getAccessor()
	{
		return accessor;
	}

	public String getBaseAddress()
	{
		return baseAddress;
	}

	public int getOffset()
	{
		return offset;
	}

	public void increaseOffSet(int increase)
	{
		this.offset += increase;
	}

	public String toString()
	{
		return "M-" + accessor + "(" + baseAddress + ") +" + offset + "  ";
	}

	public void generateAddress(ASMCodeFragment code, String comment, int callerSNL)
	{
		//System.out.println("\tcaller : " + callerSNL + " callee: " + staticNestingLevel + " diff: " + (callerSNL - staticNestingLevel));
		accessor.generateAddress(code, baseAddress, offset, comment, callerSNL - staticNestingLevel);
	}	
	
	public int getSNL(){
		return this.staticNestingLevel;
	}
	
	// //////////////////////////////////////////////////////////////////////////////////
	// Null MemoryLocation object
	// //////////////////////////////////////////////////////////////////////////////////

	public static MemoryLocation nullInstance()
	{
		return NullMemoryLocation.getInstance();
	}

	private static class NullMemoryLocation extends MemoryLocation {
		private static final int			NULL_OFFSET	= 0;
		private static NullMemoryLocation	instance	= null;

		private NullMemoryLocation()
		{
			super(MemoryAccessMethod.NULL_ACCESS, "", NULL_OFFSET, 0);
		}

		public static NullMemoryLocation getInstance()
		{
			if (instance == null) instance = new NullMemoryLocation();
			return instance;
		}
	}
}
