package symbolTable;

import java.util.ArrayList;
import java.util.List;

public class ParameterMemoryAllocator implements MemoryAllocator {
	
	private List<MemoryLocation> memLocationList = new ArrayList<MemoryLocation>();
	MemoryAccessMethod	accessor;
	final int			startingOffset;
	int					currentOffset;
	int					minOffset;
	String				baseAddress;
	List<Integer>		bookmarks;

	public ParameterMemoryAllocator(MemoryAccessMethod accessor, String baseAddress, int startingOffset)
	{
		this.accessor = accessor;
		this.baseAddress = baseAddress;
		this.startingOffset = startingOffset;
		this.currentOffset = startingOffset;
		this.minOffset = startingOffset;
		this.bookmarks = new ArrayList<Integer>();
	}

	public ParameterMemoryAllocator(MemoryAccessMethod accessor, String baseAddress)
	{
		this(accessor, baseAddress, 0);
	}

	@Override
	public MemoryLocation allocate(int sizeInBytes, int snl)
	{
		currentOffset -= sizeInBytes;
		updateMin();
		MemoryLocation memLocation = new MemoryLocation(accessor, baseAddress, currentOffset, snl);
		memLocationList.add(memLocation);
		return memLocation;
	}

	private void updateMin()
	{
		if (minOffset > currentOffset) {
			minOffset = currentOffset;
		}
	}

	@Override
	public String getBaseAddress()
	{
		return baseAddress;
	}

	@Override
	public int getMaxAllocatedSize()
	{
		return startingOffset - minOffset;
	}

	@Override
	public void saveState()
	{
		bookmarks.add(currentOffset);
	}

	@Override
	public void restoreState()
	{
		assert bookmarks.size() > 0;
		int bookmarkIndex = bookmarks.size() - 1;
		currentOffset = (int) bookmarks.remove(bookmarkIndex);
		if(bookmarks.size() == 0){
			for(MemoryLocation memLocation : memLocationList){
				 memLocation.increaseOffSet(getMaxAllocatedSize());
			}
		}
	}
}
