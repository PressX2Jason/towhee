package symbolTable;

import inputHandler.TextLocation;
import logging.TowheeLogger;
import parseTree.nodeTypes.IdentifierNode;
import semanticAnalyzer.Type;
import tokens.Token;

public class Scope {
	private Scope			baseScope;
	private MemoryAllocator	allocator;
	private SymbolTable		symbolTable;

	// ////////////////////////////////////////////////////////////////////
	// factories

	public static Scope createProgramScope()
	{
		return new Scope(programScopeAllocator(), nullInstance());
	}

	public static Scope createParameterScope()
	{
		return new Scope(parameterScopeAllocator(), nullInstance());
	}

	public static Scope createProcedureScope(int snl)
	{
		Scope scope = new Scope(procedureScopeAllocator(), nullInstance());
		//For procedure scopes, the runtime system will need 8 bytes of memory, so this memory should be allocated when the scope is created.
		scope.allocate(8, snl);
		return scope;
	}
	
	public static Scope createContainerScope(){
		return new Scope(containerScopeAllocator(), nullInstance());
	}
	
	public void allocate(int size, int snl){
		allocator.allocate(size, snl);
	}

	public Scope createSubscope()
	{
		return new Scope(allocator, this);
	}

	private static MemoryAllocator programScopeAllocator()
	{
		return new PositiveMemoryAllocator(MemoryAccessMethod.DIRECT_ACCESS_BASE, MemoryLocation.GLOBAL_VARIABLE_BLOCK);
	}

	private static MemoryAllocator parameterScopeAllocator()
	{
		return new ParameterMemoryAllocator(MemoryAccessMethod.INDIRECT_ACCESS_BASE, MemoryLocation.FRAME_POINTER);
	}

	private static MemoryAllocator procedureScopeAllocator()
	{		
		return new NegativeMemoryAllocator(MemoryAccessMethod.INDIRECT_ACCESS_BASE, MemoryLocation.FRAME_POINTER);
	}
	
	private static MemoryAllocator containerScopeAllocator(){
		return new PositiveMemoryAllocator(MemoryAccessMethod.CONTAINER_ACCESS_BASE, MemoryLocation.FRAME_POINTER);
	}

	// ////////////////////////////////////////////////////////////////////
	// private constructor.
	private Scope(MemoryAllocator allocator, Scope baseScope)
	{
		super();
		this.baseScope = (baseScope == null) ? this : baseScope;
		this.symbolTable = new SymbolTable();

		this.allocator = allocator;
	}

	// /////////////////////////////////////////////////////////////////////
	// basic queries
	public Scope getBaseScope()
	{
		return baseScope;
	}

	public MemoryAllocator getAllocationStrategy()
	{
		return allocator;
	}

	public SymbolTable getSymbolTable()
	{
		return symbolTable;
	}

	// /////////////////////////////////////////////////////////////////////
	// memory allocation
	// must call leave() when destroying/leaving a scope.
	public void enter()
	{
		allocator.saveState();
	}

	public void leave()
	{
		allocator.restoreState();
	}

	public int getAllocatedSize()
	{
		return allocator.getMaxAllocatedSize();
	}

	// /////////////////////////////////////////////////////////////////////
	// bindings
	public Binding createBinding(IdentifierNode identifierNode, Type type, boolean isMutable, int snl)
	{
		Token token = identifierNode.getToken();
		symbolTable.errorIfAlreadyDefined(token);

		String lexeme = token.getLexeme();
		Binding binding = allocateNewBinding(type, token.getLocation(), lexeme, isMutable, snl);
		symbolTable.install(lexeme, binding);

		return binding;
	}

	private Binding allocateNewBinding(Type type, TextLocation textLocation, String lexeme, boolean isMutable, int snl)
	{
		MemoryLocation memoryLocation = allocator.allocate(type.getSize(), snl);
		return new Binding(type, textLocation, memoryLocation, lexeme, isMutable);
	}

	// /////////////////////////////////////////////////////////////////////
	// toString
	public String toString()
	{
		String result = "scope: ";
		result += " hash " + hashCode() + "\n";
		result += symbolTable;
		return result;
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// Null Scope object
	public static Scope nullInstance()
	{
		return NullScope.getInstance();
	}

	private static class NullScope extends Scope {
		private static NullScope	instance	= null;

		private NullScope()
		{
			super(new PositiveMemoryAllocator(MemoryAccessMethod.NULL_ACCESS, "", 0), null);
		}

		public String toString()
		{
			return "scope: the-null-scope";
		}

		@Override
		public Binding createBinding(IdentifierNode identifierNode, Type type, boolean isMutable, int snl)
		{
			unscopedIdentifierError(identifierNode.getToken());
			return super.createBinding(identifierNode, type, isMutable, snl);
		}

		// subscopes of null scope need their own strategy. Assumes global block is static.
		public Scope createSubscope()
		{
			return new Scope(programScopeAllocator(), this);
		}

		public static Scope getInstance()
		{
			if (instance == null) instance = new NullScope();
			return instance;
		}
	}

	// /////////////////////////////////////////////////////////////////////
	// error reporting
	private static void unscopedIdentifierError(Token token)
	{
		TowheeLogger log = TowheeLogger.getLogger("complier.scope");
		log.severe("variable " + token.getLexeme() + " used outside of any scope at " + token.getLocation());
	}

}
