package semanticAnalyzer;

public class TypeVariable implements Type {
	
	private Type constrainType;

	@Override
	public int getSize()
	{
		return constrainType.getSize();
	}

	@Override
	public String infoString()
	{
		return "TypeVariable, " + constrainType.infoString();
	}

	public Type getConstrainType()
	{
		return constrainType;
	}

	public void setConstrainType(Type constrainType)
	{
		if(constrainType instanceof ArrayType){
			TypeVariable subTypeConstraint = new TypeVariable();
			subTypeConstraint.setConstrainType(((ArrayType) constrainType).getSubType());
		}
		this.constrainType = constrainType;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof TypeVariable)) return false;
		TypeVariable other = (TypeVariable) obj;
		if (constrainType == null) {
			if (other.constrainType != null) return false;
		}
		else if (!constrainType.equals(other.constrainType)) return false;
		return true;
	}
	
	

}
