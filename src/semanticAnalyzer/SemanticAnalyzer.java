package semanticAnalyzer;

import static parseTree.nodeTypes.MemberAccessNode.Members.HIGH;
import static parseTree.nodeTypes.MemberAccessNode.Members.INVALID;
import static parseTree.nodeTypes.MemberAccessNode.Members.LENGTH;
import static parseTree.nodeTypes.MemberAccessNode.Members.LOW;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import logging.TowheeLogger;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import parseTree.nodeTypes.ArrayCreationNode;
import parseTree.nodeTypes.ArrayIndexNode;
import parseTree.nodeTypes.ArrayTypeNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BodyNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallStatementNode;
import parseTree.nodeTypes.CharacterConstantNode;
import parseTree.nodeTypes.ContainerCreationNode;
import parseTree.nodeTypes.ContainerDeclarationNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.FloatConstantNode;
import parseTree.nodeTypes.FunctionDeclarationNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IfStatementNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.MemberAccessNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReleaseStatementNode;
import parseTree.nodeTypes.ReturnStatementNode;
import parseTree.nodeTypes.TypeConstantNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.UpdateNode;
import parseTree.nodeTypes.ValueBodyNode;
import symbolTable.Binding;
import symbolTable.Scope;
import tokens.LextantToken;
import tokens.Token;

public class SemanticAnalyzer {
	ParseNode	ASTree;
	//Map<String, SymbolTable> 

	public static ParseNode analyze(ParseNode ASTree)
	{
		SemanticAnalyzer analyzer = new SemanticAnalyzer(ASTree);
		return analyzer.analyze();
	}

	public SemanticAnalyzer(ParseNode ASTree)
	{
		this.ASTree = ASTree;
	}

	public ParseNode analyze()
	{
		ParseNodeVisitor functionSignatureVisitor = new FunctionSignatureVisitor();
		ParseNodeVisitor containerVisitor = new ContainerVisitor();
		ParseNodeVisitor typeCheckVisitor = new TypeCheckVisitor();
		ASTree.accept(functionSignatureVisitor);
		ASTree.accept(containerVisitor);
		ASTree.accept(typeCheckVisitor);
		return ASTree;
	}

	private class ContainerVisitor extends ParseNodeVisitor.Default {

		public void visitEnter(ProgramNode node)
		{
			boolean hasMainContainer = false;
			ParseNode mainContainerNode = null;

			for (ParseNode child : node.getChildren()) {
				if (child.child(0).getToken().getLexeme().equals("main")) {
					hasMainContainer = true;
					mainContainerNode = child;
				}
			}

			if (!hasMainContainer) {
				programMustHaveMainContainer();
				node.setType(PrimitiveType.ERROR);
			}
			if (mainContainerNode == null) {
				node.setType(PrimitiveType.ERROR);
			}

			if (!(mainContainerNode.child(1) instanceof ParameterListNode)) {
				return;
			}
			
			if (mainContainerNode.child(1) instanceof ParameterListNode && mainContainerNode.child(1).nChildren() == 0) {
				return;
			}
			else {
				mainMustHaveZeroParameters();
				node.setType(PrimitiveType.ERROR);
			}
		}

		public void visitEnter(ContainerDeclarationNode node)
		{
			FunctionSignature signature;
			IdentifierNode identifierNode = (IdentifierNode) node.child(0);

			if (node.child(1) instanceof ParameterListNode) {
				ParameterListNode paramList = (ParameterListNode) node.child(1);
				List<Type> paramTypes = paramList.getFunctionSignature();
				paramTypes.add(PrimitiveType.VOID);
				signature = new FunctionSignature(null, paramTypes);
			}
			else {
				List<Type> paramTypes = new ArrayList<Type>();
				paramTypes.add(PrimitiveType.VOID);
				paramTypes.add(PrimitiveType.VOID);
				signature = new FunctionSignature(null, paramTypes);
			}

			ContainerType containerType = new ContainerType(identifierNode.getIdentifier(), signature);
			node.getParent().getScope().createBinding(identifierNode, containerType, false, 0);
			node.setType(containerType);
		}

		// /////////////////////////////////////////////////////////////////////////
		// error logging/printing

		private void programMustHaveMainContainer()
		{
			logError("Program must declare an container with the name \"main\"");
		}

		private void mainMustHaveZeroParameters()
		{
			logError("Program must declare an container with the name \"main\" with 0 parameters");
		}

		private void logError(String message)
		{
			TowheeLogger log = TowheeLogger.getLogger("compiler.semanticAnalyzer.ContainerVisitor");
			log.severe(message);
		}
	}

	private class FunctionSignatureVisitor extends ParseNodeVisitor.Default {

		public void visitEnter(ProgramNode node)
		{
			openProgramScope(node);
		}

		public void visitEnter(BodyNode node)
		{
			
			openSubscope(node);
		}

		public void visitEnter(ContainerDeclarationNode node)
		{
			openContainerScope(node);
		}

		public void visitEnter(FunctionDeclarationNode node)
		{
			openParameterScope(node);
			IdentifierNode identifierNode = (IdentifierNode) node.child(0);
			ParameterListNode paramList = (ParameterListNode) node.child(1);
			List<Type> paramTypes = paramList.getFunctionSignature();
			paramTypes.add(node.getDeclaredReturnType());

			FunctionSignature signature = new FunctionSignature(null, paramTypes);

			node.getParent().getScope().createBinding(identifierNode, signature, false, calculateSNL(node));
			node.setType(signature);
		}

		public void visitEnter(ValueBodyNode node)
		{
			openProcedurescope(node);
		}

		private void openProgramScope(ParseNode node)
		{
			Scope scope = Scope.createProgramScope();
			node.setScope(scope);
		}

		private void openContainerScope(ContainerDeclarationNode node)
		{
			Scope scope = Scope.createContainerScope();
			node.setScope(scope);
		}

		private void openParameterScope(ParseNode node)
		{
			Scope scope = Scope.createParameterScope();
			node.setScope(scope);
		}

		private void openProcedurescope(ParseNode node)
		{
			Scope scope = Scope.createProcedureScope(calculateSNL(node));
			node.setScope(scope);
		}

		private void openSubscope(ParseNode node)
		{
			Scope baseScope = node.getLocalScope();
			Scope scope = baseScope.createSubscope();
			node.setScope(scope);
		}

		private int calculateSNL(ParseNode node)
		{
			int snl = 0;
			for (ParseNode parent : node.pathToRoot()) {
				if (parent instanceof FunctionDeclarationNode) {
					snl++;
				}
			}
			return snl;
		}
	}

	private class TypeCheckVisitor extends ParseNodeVisitor.Default {
		private List<String>	containerNameList	= new ArrayList<String>();

		@Override
		public void visitLeave(ParseNode node)
		{
			throw new RuntimeException("Node class unimplemented in SemanticAnalysisVisitor: " + node.getClass());
		}

		// /////////////////////////////////////////////////////////////////////////
		// constructs larger than statements
		@Override
		public void visitEnter(ProgramNode node)
		{
			enterScope(node);
			for (ParseNode child : node.getChildren()) {
				if (child instanceof ContainerDeclarationNode) {
					containerNameList.add(((IdentifierNode) child.child(0)).getIdentifier());
				}
			}
		}

		public void enterScope(ParseNode node)
		{
			node.getScope().enter();
		}

		public void visitLeave(ProgramNode node)
		{
			leaveScope(node);
		}

		public void visitEnter(BodyNode node)
		{
			enterScope(node);
		}

		public void visitLeave(BodyNode node)
		{
			leaveScope(node);
		}

		public void visitEnter(FunctionDeclarationNode node)
		{
			enterScope(node);
		}

		public void visitLeave(FunctionDeclarationNode node)
		{
			// Similarly, parameter scopes will need (in Towhee3) 4 bytes of memory,
			// below the parameters, and this memory should be allocated when one is about to leave the scope.
			node.getScope().allocate(4, calculateSNL(node));
			if (!node.getDeclaredReturnType().equals(node.getValueBodyReturnType())) {
				ParseNodeDef_returnDeclaredNotSameAsValueBodyReturn(node, node.getDeclaredReturnType(), node.getValueBodyReturnType());
			}
			leaveScope(node);
		}

		public void visitLeave(ParameterListNode node)
		{
			for (ParseNode child : node.getChildren()) {
				IdentifierNode identifier = (IdentifierNode) child;
				Type declarationType = getChildType(child.child(0));
				identifier.setType(declarationType);
				addBinding(identifier, declarationType, false, calculateSNL(node));
			}
		}

		private Type getChildType(ParseNode child)
		{
			if (child instanceof ArrayTypeNode) {
				return new ArrayType(getChildType(child.child(0)));
			}
			return child.getType();
		}

		public void visitEnter(ValueBodyNode node)
		{
			enterScope(node);
		}

		public void visitLeave(ValueBodyNode node)
		{
			leaveScope(node);

			Type returnType = null;
			for (ParseNode returnStatement : node.getReturnStatement()) {
				if (returnType == null) {
					returnType = returnStatement.getType();

					if (returnStatement.getType().equals(PrimitiveType.VOID)) {
						boolean parentIsFunDef = false;

						for (ParseNode parent : returnStatement.pathToRoot()) {
							if (parent instanceof FunctionDeclarationNode) {
								parentIsFunDef = true;
								break;
							}
						}
						if (!parentIsFunDef) {
							returnVoidNotFromParseNode(node);
							node.setType(PrimitiveType.ERROR);
						}
					}
				}
				else if (!returnStatement.getType().equals(returnType)) {

					if (returnStatement.getType().equals(PrimitiveType.VOID)) {
						boolean parentIsFunDef = false;

						for (ParseNode parent : returnStatement.pathToRoot()) {
							if (parent instanceof FunctionDeclarationNode) {
								parentIsFunDef = true;
								break;
							}
						}
						if (!parentIsFunDef) {
							returnVoidNotFromParseNode(node);
						}
					}
					valueBodyNodeNotAllReturnTypesAreEqual(node);
					node.setType(PrimitiveType.ERROR);
				}
			}
			if (!node.lastChildIsReturnStmt()) {
				valueBodyNodelastStatementMustBeReturn(node);
				node.setType(PrimitiveType.ERROR);
			}
			else {
				node.setType(returnType);
			}
		}

		private void leaveScope(ParseNode node)
		{
			node.getScope().leave();
		}

		// /////////////////////////////////////////////////////////////////////////
		// statements and declarations

		public void visitLeave(ReturnStatementNode node)
		{
			boolean locatedWithinValueBodyNode = false;
			for (ParseNode parent : node.pathToRoot()) {
				if (parent instanceof ValueBodyNode) {
					locatedWithinValueBodyNode = true;
					((ValueBodyNode) parent).addReturnStatement(node);
					break;
				}
			}
			if (!locatedWithinValueBodyNode) {
				returnNotAllowedOutsideValueBody(node);
			}
			if (node.nChildren() > 0) {
				node.setType(node.child(0).getType());
			}
			else {
				node.setType(PrimitiveType.VOID);
			}
			if (node.nChildren() > 1) {
				returnStatementMorethenOneExpression(node);
				node.setType(PrimitiveType.ERROR);
			}
		}

		@Override
		public void visitLeave(PrintStatementNode node)
		{
		}

		@Override
		public void visitLeave(ArrayCreationNode node)
		{
			if (node.getToken().isLextant(Punctuator.OPEN_SQUARE_BRACKET)) {
				assert node.nChildren() > 0;
				Type initalType = node.child(0).getType();

				for (ParseNode child : node.getChildren()) {
					if (!child.getType().equals(initalType)) {
						arrayNotSameTypeError(node);
						node.setType(PrimitiveType.ERROR);
					}
				}
				ArrayType type = new ArrayType(initalType, node.nChildren());
				if (node.isPersistant()) {
					type.setPersist();
				}
				node.setType(type);
			}
			else if (node.getToken().isLextant(Punctuator.AT_SIGN)) {
				assert node.nChildren() >= 2;
				Type initalType = node.child(0).getType();

				if (!(initalType instanceof ArrayType)) {
					emptyArrayCreationTypeNotArrayType(node);
				}
				else {
					ArrayType type = (ArrayType) initalType;

					if (type.subTypeIsVoid()) {
						node.setType(PrimitiveType.ERROR);
						arrayCreationVoidType(node);
						return;
					}

					if (node.isPersistant()) {
						type.setPersist();
					}
					node.setType(type);
				}
			}
		}

		@Override
		public void visitLeave(ContainerCreationNode node)
		{
			IdentifierNode name = (IdentifierNode) node.child(0);

			Type initalType = node.child(0).getType();
			
			ContainerType containerType = (ContainerType) initalType;
			
			if (!containerNameList.contains(name.getIdentifier())) {
				identifierMustBeContainer(node);
				node.setType(PrimitiveType.ERROR);
			}

			if (node.isPersistant()) {
				containerType.setPersist();
			}
			
			if(containerType.accepts(node.getParameterTypes())){
				node.setType(containerType);
			}else{
				typeCheckError(name, node.getParameterTypes());
				node.setType(PrimitiveType.ERROR);
			}

		}
		
		@Override
		public void visitLeave(ContainerDeclarationNode node){
			ContainerType containerType = (ContainerType) node.getType();
			containerType.setScopeSize(node.getScope().getAllocatedSize());
		}

		@Override
		public void visitLeave(ArrayIndexNode node)
		{
			ParseNode target = node.child(0);
			if (!(target.getType() instanceof ArrayType)) {
				arrayTypeIndexError(node);
				node.setType(PrimitiveType.ERROR);
			}
			else {
				ArrayType childType = (ArrayType) target.getType();
				node.setType(childType.getSubType());
			}

			ParseNode index = node.child(1);
			if (index.getType() != PrimitiveType.INTEGER) {
				arrayIndexTypeError(node);
				node.setType(PrimitiveType.ERROR);
			}
		}

		@Override
		public void visitLeave(ArrayTypeNode node)
		{
			assert node.nChildren() == 1;
			ArrayType type = new ArrayType(node.child(0).getType());
			node.setType(type);
		}

		@Override
		public void visitLeave(DeclarationNode node)
		{
			IdentifierNode identifier = (IdentifierNode) node.child(0);
			ParseNode initializer = node.child(1);

			Type declarationType = initializer.getType();
			node.setType(declarationType);

			identifier.setType(declarationType);
			addBinding(identifier, declarationType, node.isMutable(), calculateSNL(identifier));
		}

		private int calculateSNL(ParseNode node)
		{
			int snl = 0;
			for (ParseNode parent : node.pathToRoot()) {
				if (parent instanceof FunctionDeclarationNode) {
					snl++;
				}
			}
			return snl;
		}

		@Override
		public void visitLeave(UpdateNode node)
		{
			ParseNode identifier = node.child(0);
			ParseNode updatedValue = node.child(1);

			Type declarationType = updatedValue.getType();

			if (identifier instanceof FunctionInvocationNode) {
				updateTargetError(identifier);
			}

			if (!declarationType.equals(identifier.getType())) {
				updateTypeError(identifier, identifier.getType(), declarationType);
			}
			else {
				node.setType(declarationType);
			}
		}

		@Override
		public void visitLeave(MemberAccessNode node)
		{
			assert node.nChildren() == 2;
			if (!(node.child(0).getType() instanceof ArrayType)) {
				memberAccessTypeError(node);
			}

			String lexeme = node.child(1).getToken().getLexeme();
			if (lexeme.equals("low")) {
				node.setMember(LOW);
				node.setType(PrimitiveType.INTEGER);
				node.removeChild(node.child(1));
			}
			else if (lexeme.equals("high")) {
				node.setMember(HIGH);
				node.setType(PrimitiveType.INTEGER);
				node.removeChild(node.child(1));
			}
			else if (lexeme.equals("length")) {
				node.setMember(LENGTH);
				node.setType(PrimitiveType.INTEGER);
				node.removeChild(node.child(1));
			}
			else {
				node.setMember(INVALID);
				node.setType(PrimitiveType.ERROR);
				memberAccessIdentifierError(node);
			}
		}

		@Override
		public void visitLeave(IfStatementNode node)
		{
			assert node.nChildren() == 2 || node.nChildren() == 3;
			ParseNode condition = node.child(0);

			if (condition.getType() != PrimitiveType.BOOLEAN) {
				typeCheckError(node, Arrays.asList(condition.getType()));
				node.setType(PrimitiveType.ERROR);
			}
		}

		@Override
		public void visitLeave(ReleaseStatementNode node)
		{
			for (ParseNode child : node.getChildren()) {
				if (!(child.getType() instanceof ArrayType)) {
					releaseWrongType(node);
				}
			}
		}

		@Override
		public void visitLeave(CallStatementNode node)
		{
			IdentifierNode funtionID = (IdentifierNode) node.child(0);

			if (!(funtionID.getBinding().getType() instanceof FunctionSignature)) {
				callStmtCanOnlyTargetParseNodes(node);
				node.setType(PrimitiveType.ERROR);
			}

			FunctionSignature ParseNodeSig = (FunctionSignature) funtionID.getBinding().getType();

			if (!ParseNodeSig.resultType().equals(PrimitiveType.VOID)) {
				callStmtCanOnlyCallVoidReturnParseNodes(node);
				node.setType(PrimitiveType.ERROR);
			}

			List<Type> arguementTypes = new ArrayList<Type>();
			for (ParseNode child : node.getArguments()) {
				arguementTypes.add(child.getType());
			}

			if (!ParseNodeSig.accepts(arguementTypes)) {
				ParseNodeArgumentMismatch(node, ParseNodeSig.paramType(), arguementTypes);
				node.setType(PrimitiveType.ERROR);
			}

			node.setType(ParseNodeSig.resultType());
		}

		@Override
		public void visitLeave(FunctionInvocationNode node)
		{
			IdentifierNode funtionID = (IdentifierNode) node.child(0);

			if (!(funtionID.getBinding().getType() instanceof FunctionSignature)) {
				callStmtCanOnlyTargetParseNodes(node);
				node.setType(PrimitiveType.ERROR);
			}

			FunctionSignature ParseNodeSig = (FunctionSignature) funtionID.getBinding().getType();

			List<Type> arguementTypes = new ArrayList<Type>();
			for (ParseNode child : node.getArguments()) {
				arguementTypes.add(child.getType());
			}

			if (!ParseNodeSig.accepts(arguementTypes)) {
				ParseNodeArgumentMismatch(node, ParseNodeSig.paramType(), arguementTypes);
				node.setType(PrimitiveType.ERROR);
			}

			node.setType(ParseNodeSig.resultType());
		}

		// /////////////////////////////////////////////////////////////////////////
		// expressions
		@Override
		public void visitLeave(BinaryOperatorNode node)
		{
			assert node.nChildren() == 2;
			ParseNode left = node.child(0);
			ParseNode right = node.child(1);

			if (left.getType() instanceof ArrayType && right.getType() instanceof ArrayType) {
				ArrayType leftType = (ArrayType) left.getType();
				ArrayType rightType = (ArrayType) right.getType();

				if (!leftType.equals(rightType)) {
					typeCheckError(node, Arrays.asList((Type) leftType, (Type) rightType));
					node.setType(PrimitiveType.ERROR);
				}
				else {
					List<Type> childTypes = Arrays.asList((Type) new ArrayType(), (Type) new ArrayType());

					Lextant operator = operatorFor(node);
					FunctionSignatures signature = FunctionSignatures.signaturesOf(operator);

					if (signature.accepts(childTypes)) {
						node.setType(signature.acceptingSignature(childTypes).resultType());
						node.setSignature(signature.acceptingSignature(childTypes));
					}
					else {
						typeCheckError(node, childTypes);
						node.setType(PrimitiveType.ERROR);
					}
				}

			}
			else {
				List<Type> childTypes = Arrays.asList(left.getType(), right.getType());

				Lextant operator = operatorFor(node);
				if (operator.equals(Punctuator.COLON) && !(right instanceof TypeConstantNode)) {
					typeCheckError(node, left.getType());
					node.setType(PrimitiveType.ERROR);
					return;
				}
				FunctionSignatures signature = FunctionSignatures.signaturesOf(operator);

				if (signature.accepts(childTypes)) {
					node.setType(signature.acceptingSignature(childTypes).resultType());
					node.setSignature(signature.acceptingSignature(childTypes));
				}
				else {
					typeCheckError(node, childTypes);
					node.setType(PrimitiveType.ERROR);
				}
			}
		}

		private Lextant operatorFor(BinaryOperatorNode node)
		{
			LextantToken token = (LextantToken) node.getToken();
			return token.getLextant();
		}

		// expressions
		@Override
		public void visitLeave(UnaryOperatorNode node)
		{
			assert node.nChildren() == 1;
			ParseNode left = node.child(0);
			List<Type> childTypes = Arrays.asList(left.getType(), node.getResultType());

			Lextant operator = operatorFor(node);
			FunctionSignatures signature = FunctionSignatures.signaturesOf(operator);

			if (signature.accepts(childTypes)) {
				node.setType(signature.acceptingSignature(childTypes).resultType());
				node.setSignature(signature.acceptingSignature(childTypes));
			}
			else {
				typeCheckError(node, childTypes);
				node.setType(PrimitiveType.ERROR);
			}
		}

		private Lextant operatorFor(UnaryOperatorNode node)
		{
			LextantToken token = (LextantToken) node.getToken();
			return token.getLextant();
		}

		// /////////////////////////////////////////////////////////////////////////
		// simple leaf nodes
		@Override
		public void visit(BooleanConstantNode node)
		{
			node.setType(PrimitiveType.BOOLEAN);
		}

		@Override
		public void visit(ErrorNode node)
		{
			node.setType(PrimitiveType.ERROR);
		}

		@Override
		public void visit(IntegerConstantNode node)
		{
			node.setType(PrimitiveType.INTEGER);
		}

		@Override
		public void visit(FloatConstantNode node)
		{
			node.setType(PrimitiveType.FLOAT);
		}

		@Override
		public void visit(CharacterConstantNode node)
		{
			node.setType(PrimitiveType.CHAR);
		}

		// /////////////////////////////////////////////////////////////////////////
		// IdentifierNodes, with helper methods
		@Override
		public void visit(IdentifierNode node)
		{
			if (isBeingDeclared(node)) {
				// parent DeclarationNode does the processing.
				return;
			}
			if (isMemberAccess(node)) {
				// parent MemberAccessNode does the processing
				return;
			}
			if (isParameterValue(node)) {
				// parent ParameterListNode does the processing
				return;
			}
			Binding binding = node.findVariableBinding();

			node.setType(binding.getType());
			node.setBinding(binding);
		}

		private boolean isParameterValue(IdentifierNode node)
		{
			ParseNode parent = node.getParent();
			return (parent instanceof ParameterListNode);
		}

		private boolean isMemberAccess(IdentifierNode node)
		{
			ParseNode parent = node.getParent();
			return (parent instanceof MemberAccessNode) && (node == parent.child(1));
		}

		private boolean isBeingDeclared(IdentifierNode node)
		{
			ParseNode parent = node.getParent();
			return (parent instanceof DeclarationNode) && (node == parent.child(0));
		}

		private void addBinding(IdentifierNode identifierNode, Type type, boolean isMutable, int snl)
		{
			Scope scope = identifierNode.getLocalScope();
			Binding binding = scope.createBinding(identifierNode, type, isMutable, snl);
			identifierNode.setBinding(binding);
		}

		// /////////////////////////////////////////////////////////////////////////
		// error logging/printing

		private void identifierMustBeContainer(ParseNode node)
		{
			Token token = node.getToken();

			logError("only identifiers with type container can be instantiated " + token.getLocation());
		}

		private void ParseNodeArgumentMismatch(ParseNode node, List<Type> ParseNodeTypes, List<Type> givenTypes)
		{
			Token token = node.getToken();

			logError("argument type mismatch at " + token.getLocation() + " tried to pass " + givenTypes + " into ParseNode that accepts " + ParseNodeTypes);
		}

		private void callStmtCanOnlyCallVoidReturnParseNodes(ParseNode node)
		{
			Token token = node.getToken();

			logError("call statement can only target ParseNodes that return void " + token.getLocation());
		}

		private void callStmtCanOnlyTargetParseNodes(ParseNode node)
		{
			Token token = node.getToken();

			logError("call statement at " + token.getLocation() + " is not targeting a ParseNode");
		}

		private void ParseNodeDef_returnDeclaredNotSameAsValueBodyReturn(ParseNode node, Type declaredType, Type actualType)
		{
			Token token = node.getToken();

			logError("invalid ParseNode declaration; return type declared " + declaredType.infoString() + " does not match with actual returned type "
					+ actualType.infoString() + " at " + token.getLocation());
		}

		private void returnVoidNotFromParseNode(ParseNode node)
		{
			Token token = node.getToken();

			logError("void can only be returned from a ParseNode " + token.getLocation());
		}

		private void returnNotAllowedOutsideValueBody(ParseNode node)
		{
			Token token = node.getToken();

			logError("return statements are only allowed inside of value bodies " + token.getLocation());
		}

		private void returnStatementMorethenOneExpression(ParseNode node)
		{
			Token token = node.getToken();

			logError("return statement at " + token.getLocation() + " returns more then one expression");
		}

		private void valueBodyNodelastStatementMustBeReturn(ParseNode node)
		{
			Token token = node.getToken();

			logError("value body at " + token.getLocation() + " does not RETURN as the last statment");
		}

		private void valueBodyNodeNotAllReturnTypesAreEqual(ParseNode node)
		{
			Token token = node.getToken();

			logError("value body node at " + token.getLocation() + " does not have all return types of same type");
		}

		private void memberAccessIdentifierError(ParseNode node)
		{
			Token token = node.getToken();

			logError("member access statement tried to access unknown identifier" + token.getLocation());
		}

		private void memberAccessTypeError(ParseNode node)
		{
			Token token = node.getToken();

			logError("member access statement can only target varibles of type Array" + token.getLocation());
		}

		private void releaseWrongType(ParseNode node)
		{
			Token token = node.getToken();

			logError("release statement can only target varibles of type Array" + token.getLocation());
		}

		private void arrayIndexTypeError(ParseNode node)
		{
			Token token = node.getToken();

			logError("only integers can be used to access arrays through indexing " + token.getLocation());
		}

		private void emptyArrayCreationTypeNotArrayType(ParseNode node)
		{
			Token token = node.getToken();

			logError("empty array creation must declare an arrayType [ type ] " + token.getLocation());
		}

		private void arrayCreationVoidType(ParseNode node)
		{
			Token token = node.getToken();

			logError("arrays cannont be created of type void " + token.getLocation());
		}

		private void arrayTypeIndexError(ParseNode node)
		{
			Token token = node.getToken();

			logError("only arrays can be accessed though indexing " + token.getLocation());
		}

		private void arrayNotSameTypeError(ParseNode node)
		{
			Token token = node.getToken();

			logError("elements of array are not all of the same type at " + token.getLocation());
		}

		private void updateTypeError(ParseNode node, Type oldType, Type NewType)
		{
			Token token = node.getToken();

			logError("variable " + token.getLexeme() + " cannot be assigned type " + NewType.infoString() + " when it has already been declared as type "
					+ oldType.infoString());
		}

		private void updateTargetError(ParseNode node)
		{
			Token token = node.getToken();

			logError("update statement cannot target function invocation " + token.getLocation());
		}

		private void typeCheckError(ParseNode node, Type leftType)
		{
			Token token = node.getToken();

			logError("operator " + token.getLexeme() + " not defined for types [" + leftType + ", VALUEBODY] at " + token.getLocation());
		}

		private void typeCheckError(ParseNode node, List<Type> operandTypes)
		{
			Token token = node.getToken();

			logError("operator " + token.getLexeme() + " not defined for types " + operandTypes + " at " + token.getLocation());
		}

		private void logError(String message)
		{
			TowheeLogger log = TowheeLogger.getLogger("compiler.semanticAnalyzer.TypeCheckVisitor");
			log.severe(message);
		}
	}
}
