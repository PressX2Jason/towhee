package semanticAnalyzer;

public class ContainerLabeller {
		private int	containerSequenceNumber;
		
		public ContainerLabeller(){
			this.containerSequenceNumber = 128;
		}

		public int getNewContainerLabel(){
			return containerSequenceNumber++;
		}
}
