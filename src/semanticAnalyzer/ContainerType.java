package semanticAnalyzer;

import java.util.ArrayList;
import java.util.List;

public class ContainerType implements Type {

	private String						name;
	private int							typeID				= 0;
	private FunctionSignature			fuctionSig;
	private int							statusFlag			= 0;
	private static ContainerLabeller	containerLabeller	= new ContainerLabeller();
	private int scopeSize = 0;
	private static List<String> containerNames = new ArrayList<String>();

	public ContainerType(String name, FunctionSignature fuctionSig)
	{
		super();
		this.name = name;
		containerNames.add(name);
		this.fuctionSig = fuctionSig;
		this.typeID = containerLabeller.getNewContainerLabel();
	}

	@Override
	public int getSize()
	{
		return 4;
	}

	@Override
	public String infoString()
	{
		return "container: " + name;
	}

	public static ContainerLabeller getContainerLabeller()
	{
		return containerLabeller;
	}

	public String getName()
	{
		return name;
	}

	public int getTypeID()
	{
		return typeID;
	}

	public void generateTypeID()
	{
		typeID = getContainerLabeller().getNewContainerLabel();
	}

	public boolean accepts(List<Type> types)
	{
		return fuctionSig.accepts(types);
	}

	public int getStatusFlag()
	{
		return this.statusFlag;
	}

	public void setPersist()
	{
		this.statusFlag = this.statusFlag | 0x1;
	}

	public int getScopeSize()
	{
		return scopeSize;
	}

	public void setScopeSize(int scopeSize)
	{
		this.scopeSize = scopeSize;
	}

	public static List<String> getContainerNames()
	{
		return containerNames;
	}

}
