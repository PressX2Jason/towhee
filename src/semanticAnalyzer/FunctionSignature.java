package semanticAnalyzer;

import java.util.Arrays;
import java.util.List;

// immutable
public class FunctionSignature implements Type {
	private static final boolean	ALL_TYPES_ACCEPT_ERROR_TYPES	= true;
	private Type					resultType;
	private Type[]					paramTypes;
	Object							whichVariant;

	// /////////////////////////////////////////////////////////////
	// construction

	public FunctionSignature(Object whichVariant, Type... types)
	{
		assert (types.length >= 1);
		storeParamTypes(types);
		resultType = types[types.length - 1];
		this.whichVariant = whichVariant;
	}

	private void storeParamTypes(Type[] types)
	{
		paramTypes = new Type[types.length - 1];
		for (int i = 0; i < types.length - 1; i++) {
			paramTypes[i] = types[i];
		}
	}

	public FunctionSignature(Object whichVariant, List<Type> types)
	{
		assert (types.size() >= 1);
		storeParamTypes(types);
		resultType = types.get(types.size() - 1);
		this.whichVariant = whichVariant;
	}

	private void storeParamTypes(List<Type> types)
	{
		paramTypes = new Type[types.size() - 1];
		for (int i = 0; i < types.size() - 1; i++) {
			paramTypes[i] = types.get(i);
		}
	}

	// /////////////////////////////////////////////////////////////
	// accessors

	public Object getVariant()
	{
		return whichVariant;
	}

	public List<Type> paramType()
	{
		return Arrays.asList(paramTypes);
	}

	public Type resultType()
	{
		return resultType;
	}

	public boolean isNull()
	{
		return false;
	}

	// /////////////////////////////////////////////////////////////
	// main query

	public boolean accepts(List<Type> types)
	{
		if (types.size() != paramTypes.length) {
			return false;
		}

		for (int i = 0; i < paramTypes.length; i++) {
			if (!assignableTo(paramTypes[i], types.get(i))) {
				return false;
			}
		}
		return true;
	}

	private boolean assignableTo(Type variableType, Type valueType)
	{
		if (valueType == PrimitiveType.ERROR && ALL_TYPES_ACCEPT_ERROR_TYPES) {
			return true;
		}
		return variableType.equals(valueType);
	}

	// Null object pattern
	private static FunctionSignature	neverMatchedSignature	= new FunctionSignature(1, PrimitiveType.ERROR) {
																	public boolean accepts(List<Type> types)
																	{
																		return false;
																	}

																	public boolean isNull()
																	{
																		return true;
																	}
																};

	public static FunctionSignature nullInstance()
	{
		return neverMatchedSignature;
	}

	// inherited methods

	@Override
	public int getSize()
	{
		return 0;
	}

	@Override
	public String infoString()
	{
		String paramList = "(";
		if (paramTypes.length != 0) {
			for (int i = 0; i < paramTypes.length - 1; i++) {
				paramList += paramTypes[i].infoString() + ", ";
			}
			paramList += paramTypes[paramTypes.length - 1].infoString() + ")";
		}else{
			paramList = "( )";
		}
		return "function: " + paramList + " -> " + resultType;
	}
}
