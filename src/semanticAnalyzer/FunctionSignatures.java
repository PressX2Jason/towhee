package semanticAnalyzer;

import static semanticAnalyzer.PrimitiveType.BOOLEAN;
import static semanticAnalyzer.PrimitiveType.CHAR;
import static semanticAnalyzer.PrimitiveType.FLOAT;
import static semanticAnalyzer.PrimitiveType.INTEGER;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lexicalAnalyzer.Punctuator;
import asmCodeGenerator.CastCodeGenerator;
import asmCodeGenerator.DivisionCodeGenerator;
import asmCodeGenerator.EqualToCodeGenerator;
import asmCodeGenerator.GreaterThanCodeGenerator;
import asmCodeGenerator.GreaterThanOrEqualCodeGenerator;
import asmCodeGenerator.LessThanCodeGenerator;
import asmCodeGenerator.LessThanOrEqualCodeGenerator;
import asmCodeGenerator.NotEqualToCodeGenerator;
import asmCodeGenerator.ShortCircuitAndCodeGenerator;
import asmCodeGenerator.ShortCircuitOrCodeGenerator;
import asmCodeGenerator.codeStorage.ASMOpcode;

public class FunctionSignatures extends ArrayList<FunctionSignature> {
	private static final long						serialVersionUID	= -4907792488209670697L;
	private static Map<Object, FunctionSignatures>	signaturesForKey	= new HashMap<Object, FunctionSignatures>();

	Object											key;

	public FunctionSignatures(Object key, FunctionSignature... functionSignatures)
	{
		this.key = key;
		for (FunctionSignature functionSignature : functionSignatures) {
			add(functionSignature);
		}
		signaturesForKey.put(key, this);
	}

	public Object getKey()
	{
		return key;
	}

	public boolean hasKey(Object key)
	{
		return this.key.equals(key);
	}

	public FunctionSignature acceptingSignature(List<Type> types)
	{
		for (FunctionSignature functionSignature : this) {
			if (functionSignature.accepts(types)) {
				return functionSignature;
			}
		}
		return FunctionSignature.nullInstance();
	}

	public boolean accepts(List<Type> types)
	{
		return !acceptingSignature(types).isNull();
	}

	// ///////////////////////////////////////////////////////////////////////////////
	// access to FunctionSignatures by key object.

	public static FunctionSignatures	nullSignatures	= new FunctionSignatures(0, FunctionSignature.nullInstance());

	public static FunctionSignatures signaturesOf(Object key)
	{
		if (signaturesForKey.containsKey(key)) {
			return signaturesForKey.get(key);
		}
		return nullSignatures;
	}

	public static FunctionSignature signature(Object key, List<Type> types)
	{
		FunctionSignatures signatures = FunctionSignatures.signaturesOf(key);
		return signatures.acceptingSignature(types);
	}

	// ///////////////////////////////////////////////////////////////////////////////
	// Put the signatures for towhee operators in the following static block.

	static {
		// one example to get you started with FunctionSignatures: the signatures for addition.
		// for this to work, you should statically import PrimitiveType.*

		// new FunctionSignatures(Punctuator.ADD,
		// new FunctionSignature(ASMOpcode.Add, INTEGER, INTEGER, INTEGER),
		// new FunctionSignature(ASMOpcode.FAdd, FLOAT, FLOAT, FLOAT)
		// );

		// First, we use the operator itself (in this case the Punctuator ADD) as the key.
		// Then, we give that key two signatures: one an (INT x INT -> INT) and the other
		// a (FLOAT x FLOAT -> FLOAT). Each signature has a "whichVariant" parameter where
		// I'm placing the instruction (ASMOpcode) that needs to be executed.
		//
		// I'll follow the convention that if a signature has an ASMOpcode for its whichVariant,
		// then to generate code for the operation, one only needs to generate the code for
		// the operands (in order) and then add to that the Opcode. For instance, the code for
		// floating addition should look like:
		//
		// (generate argument 1) : may be many instructions
		// (generate argument 2) : ditto
		// FAdd : just one instruction
		//
		// If the code that an operator should generate is more complicated than this, then
		// I will not use an ASMOpcode for the whichVariant. In these cases I typically use
		// a small object with one method (the "Command" design pattern) that generates the
		// required code.

		//@formatter:off
		// arithmetic expressions
		new FunctionSignatures(Punctuator.ADD,
				new FunctionSignature(ASMOpcode.Add, INTEGER, INTEGER, INTEGER),
				new FunctionSignature(ASMOpcode.FAdd, FLOAT, FLOAT, FLOAT));

		new FunctionSignatures(Punctuator.SUBTRACT,
				new FunctionSignature(ASMOpcode.Subtract, INTEGER, INTEGER, INTEGER),
				new FunctionSignature(ASMOpcode.FSubtract, FLOAT, FLOAT, FLOAT));

		new FunctionSignatures(Punctuator.MULTIPLY,
				new FunctionSignature(ASMOpcode.Multiply, INTEGER, INTEGER, INTEGER),
				new FunctionSignature(ASMOpcode.FMultiply, FLOAT, FLOAT, FLOAT));

		new FunctionSignatures(Punctuator.DIVIDE, 
				new FunctionSignature(new DivisionCodeGenerator(), INTEGER, INTEGER, INTEGER),
				new FunctionSignature(new DivisionCodeGenerator(), FLOAT, FLOAT, FLOAT));

		// boolean expressions - the ASMOpcode corresponds to what type of subtraction it is
		new FunctionSignatures(Punctuator.GREATER_THAN,
				new FunctionSignature(new GreaterThanCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new GreaterThanCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new GreaterThanCodeGenerator(), FLOAT, FLOAT, BOOLEAN));

		new FunctionSignatures(Punctuator.GREATER_THAN_OR_EQUAL,
				new FunctionSignature(new GreaterThanOrEqualCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new GreaterThanOrEqualCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new GreaterThanOrEqualCodeGenerator(), FLOAT, FLOAT, BOOLEAN));

		new FunctionSignatures(Punctuator.LESS_THAN,
				new FunctionSignature(new LessThanCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new LessThanCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new LessThanCodeGenerator(), FLOAT, FLOAT, BOOLEAN));

		new FunctionSignatures(Punctuator.LESS_THAN_OR_EQUAL,
				new FunctionSignature(new LessThanOrEqualCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new LessThanOrEqualCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new LessThanOrEqualCodeGenerator(), FLOAT, FLOAT, BOOLEAN));

		new FunctionSignatures(Punctuator.EQUAL,
				new FunctionSignature(new EqualToCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new EqualToCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new EqualToCodeGenerator(), FLOAT, FLOAT, BOOLEAN),
				new FunctionSignature(new EqualToCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN),
				new FunctionSignature(new EqualToCodeGenerator(), new ArrayType(), new ArrayType(), BOOLEAN));
		
		new FunctionSignatures(Punctuator.NOT_EQUAL,
				new FunctionSignature(new NotEqualToCodeGenerator(), CHAR, CHAR, BOOLEAN),
				new FunctionSignature(new NotEqualToCodeGenerator(), INTEGER, INTEGER, BOOLEAN),
				new FunctionSignature(new NotEqualToCodeGenerator(), FLOAT, FLOAT, BOOLEAN),
				new FunctionSignature(new NotEqualToCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN),
				new FunctionSignature(new NotEqualToCodeGenerator(), new ArrayType(), new ArrayType(), BOOLEAN));

		// Casting
		new FunctionSignatures(Punctuator.COLON,
				new FunctionSignature(ASMOpcode.ConvertF, INTEGER, FLOAT, FLOAT),
				new FunctionSignature(ASMOpcode.ConvertI, FLOAT, INTEGER, INTEGER),
				new FunctionSignature(ASMOpcode.Nop, CHAR, INTEGER, INTEGER),
				new FunctionSignature(new CastCodeGenerator(), INTEGER, CHAR, CHAR));

		// Boolean Operators
		new FunctionSignatures(Punctuator.SHORT_CIRCUIT_AND,
				new FunctionSignature(new ShortCircuitAndCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN));

		new FunctionSignatures(Punctuator.SHORT_CIRCUIT_OR,
				new FunctionSignature( new ShortCircuitOrCodeGenerator() , BOOLEAN, BOOLEAN, BOOLEAN));

		new FunctionSignatures(Punctuator.NOT,
				new FunctionSignature(ASMOpcode.BNegate, BOOLEAN, BOOLEAN, BOOLEAN));
	}
}
