package semanticAnalyzer;

public class ArrayType implements Type {

	private int		headerSize			= 20;

	private int		TypeID				= 5;
	private int		statusFlag			= 0;
	private Type	subType;
	private int		subTypeSize;
	private int		lowerIndex;
	private int		numberOfElements	= 0;

	public ArrayType()
	{
		this.subType = PrimitiveType.NO_TYPE;
	}

	public ArrayType(Type subtype)
	{
		TypeID = 5; // array id is 5
		this.subType = subtype;
		if (subType instanceof ArrayType) {
			subTypeSize = 4;
			statusFlag = statusFlag | 0x2;
		}
		else {
			subTypeSize = subType.getSize();
		}
	}

	public ArrayType(Type subtype, int numberOfElements)
	{
		TypeID = 5; // array id is 5
		this.subType = subtype;
		if (subType instanceof ArrayType) {
			subTypeSize = 4;
			statusFlag = statusFlag | 0x2;
		}
		else {
			subTypeSize = subType.getSize();
		}
		this.numberOfElements = numberOfElements;
	}

	@Override
	public int getSize()
	{
		return 4;
	}

	public int getRecordSize()
	{
		return getHeaderSize() + subTypeSize * numberOfElements;
	}

	public int getHeaderSize()
	{
		return headerSize;
	}

	@Override
	public String infoString()
	{
		return "array[" + subType.infoString() + "]";
	}

	// getters
	public int getTypeID()
	{
		return TypeID;
	}

	public int getStatusFlag()
	{
		return statusFlag;
	}

	public void setPersist()
	{
		this.statusFlag = this.statusFlag | 0x1;
	}

	public Type getSubType()
	{
		return subType;
	}

	public int getSubTypeSize()
	{
		return subTypeSize;
	}

	public int getLowerIndex()
	{
		return lowerIndex;
	}

	public int nElements()
	{
		return numberOfElements;
	}

	public boolean subTypeIsVoid()
	{
		if (subType instanceof ArrayType) {
			return ((ArrayType) subType).subTypeIsVoid();
		}
		if (subType.equals(PrimitiveType.VOID)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof ArrayType)) return false;
		ArrayType other = (ArrayType) obj;
		if (subType == null) {
			if (other.subType != null) return false;
		}
		else if (!subType.equals(other.subType)) return false;
		return true;
	}

	@Override
	public String toString()
	{
		return this.infoString();
	}
}
