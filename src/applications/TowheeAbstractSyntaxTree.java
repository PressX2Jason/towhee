package applications;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import lexicalAnalyzer.LexicalAnalyzer;
import lexicalAnalyzer.Scanner;
import parseTree.ParseNode;
import parseTree.ParseTreePrinter;
import parser.Parser;
import tokens.Tokens;

public class TowheeAbstractSyntaxTree extends TowheeApplication {
	/**
	 * Prints abstract syntax tree of a Towhee file. Prints errors if syntax incorrect.
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException
	{
		checkArguments(args, "TowheeAbstractSyntaxTree");

		ParseTreePrinter.setPrintLevel(ParseTreePrinter.Level.FULL);
		Tokens.setPrintLevel(Tokens.Level.TYPE_VALUE_SEQ);
		parseFileToAST(args[0], System.out);
	}

	/**
	 * analyzes a file specified by filename.
	 * 
	 * @param filename
	 *            the name of the file to be analyzed.
	 * @throws FileNotFoundException
	 */
	public static void parseFileToAST(String filename, PrintStream out) throws FileNotFoundException
	{
		// factory method that creates a new LexicalAnalyzer object
		Scanner scanner = LexicalAnalyzer.make(filename);
		ParseNode syntaxTree = Parser.parse(scanner);

		out.print(syntaxTree);
	}
}
