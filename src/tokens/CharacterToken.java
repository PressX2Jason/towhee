package tokens;

import inputHandler.TextLocation;

public class CharacterToken extends TokenImp {
	protected char	value;

	protected CharacterToken(TextLocation location, String lexeme)
	{
		super(location, lexeme);
	}

	protected void setValue(char value)
	{
		this.value = value;
	}

	public char getValue()
	{
		return value;
	}

	public static CharacterToken make(TextLocation location, char lexeme)
	{
		CharacterToken result = new CharacterToken(location, Character.toString(lexeme));
		result.setValue(lexeme);
		return result;
	}

	@Override
	protected String rawString()
	{
		return "char, " + value;
	}

}
