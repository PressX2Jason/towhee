package tokens;

import inputHandler.TextLocation;
import lexicalAnalyzer.Lextant;

public interface Token {
	public String getLexeme();

	public TextLocation getLocation();

	public String fullString();

	public boolean isLextant(Lextant... lextants);
}
