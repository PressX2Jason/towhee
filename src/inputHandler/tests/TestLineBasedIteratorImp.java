package inputHandler.tests;

import static inputHandler.tests.FixtureDefinitions.NONEXISTENT_FILENAME;
import static inputHandler.tests.FixtureDefinitions.SIMPLE_FIXTURE_FILENAME;
import static inputHandler.tests.FixtureDefinitions.simpleFixtureStrings;
import inputHandler.LineBasedReader;
import junit.framework.TestCase;

public class TestLineBasedIteratorImp extends TestCase {

	protected LineBasedReader factory(String filename)
	{
		return new LineBasedReader(filename);
	}

	public void testHappyPath()
	{
		LineBasedReader reader = factory(SIMPLE_FIXTURE_FILENAME);

		for (String lineExpected : simpleFixtureStrings) {
			assertTrue(reader.hasNext());
			String lineRead = reader.next();
			assertEquals(lineExpected, lineRead);
		}
		assertFalse(reader.hasNext());
	}

	public void testFileNotFound()
	{
		try {
			factory(NONEXISTENT_FILENAME);
			fail();
		}
		catch (IllegalArgumentException e) {
		}
	}
}
