package inputHandler;

/**
 * Value object for holding a character and its location in the input text. Contains delegates to select character operations.
 * 
 */
public class LocatedChar {
	Character		character;
	TextLocation	location;

	public LocatedChar(Character character, TextLocation location)
	{
		super();
		this.character = character;
		this.location = location;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// getters

	public Character getCharacter()
	{
		return character;
	}

	public TextLocation getLocation()
	{
		return location;
	}

	public boolean isChar(char c)
	{
		return character == c;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// toString

	public String toString()
	{
		return "(" + charString() + ", " + location + ")";
	}

	private String charString()
	{
		if (Character.isWhitespace(character)) {
			int i = character;
			return String.format("'\\%d'", i);
		}
		else {
			return character.toString();
		}
	}

	// ////////////////////////////////////////////////////////////////////////////
	// delegates

	public boolean isLowerCase()
	{
		return Character.isLowerCase(character);
	}

	public boolean isUpperCase()
	{
		return Character.isUpperCase(character);
	}

	public boolean isDigit()
	{
		return Character.isDigit(character);
	}

	public boolean isDecimalPoint()
	{
		return character.equals('.');
	}

	public boolean isHyphenMinus()
	{
		return character.equals('-');
	}

	public boolean isWhitespace()
	{
		return Character.isWhitespace(character);
	}

	public boolean isUnderScore()
	{
		return character.equals('_');
	}

	public boolean isTilde()
	{
		return character.equals('~');
	}

	public boolean isBackQuote()
	{
		return character.equals('`');
	}

	public boolean isSingleQuote()
	{
		return character.equals('\'');
	}

	public boolean isNewLine()
	{
		return character.equals('\n');
	}

	public boolean isExponentStart()
	{
		return character.equals('e');
	}

	// ASCII decimal 32-126
	public boolean isCharacter()
	{
		return (((int) character) >= 32) && (((int) character) <= 126);
	}

	// [a..zA..Z_]
	public boolean isIdentifier()
	{
		return isLowerCase() || isUpperCase() || isUnderScore();
	}

}
