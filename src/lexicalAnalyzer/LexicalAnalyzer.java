package lexicalAnalyzer;

import static lexicalAnalyzer.PunctuatorScanningAids.isPunctuatorStartingCharacter;
import inputHandler.InputHandler;
import inputHandler.LocatedChar;
import inputHandler.LocatedCharStream;
import inputHandler.PushbackCharStream;
import logging.TowheeLogger;
import tokens.CharacterToken;
import tokens.FloatToken;
import tokens.IdentifierToken;
import tokens.IntegerToken;
import tokens.LextantToken;
import tokens.NullToken;
import tokens.Token;

public class LexicalAnalyzer extends ScannerImp implements Scanner {
	public static LexicalAnalyzer make(String filename)
	{
		InputHandler handler = InputHandler.fromFilename(filename);
		// factory method takes a InputHander and returns a new
		// PushbackCharStream object
		PushbackCharStream charStream = PushbackCharStream.make(handler);
		return new LexicalAnalyzer(charStream);
	}

	public LexicalAnalyzer(PushbackCharStream input)
	{
		super(input);
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Token-finding main dispatch

	@Override
	protected Token findNextToken()
	{
		LocatedChar ch = nextNonWhitespaceChar(); // locatedChar has a character, and a text location
		if (ch.isHyphenMinus()) {
			if (input.peek().isWhitespace()) {
				if (isPunctuatorStart(ch)) {
					return PunctuatorScanner.scan(ch, input);
				}
			}
			else if (input.peek().isDigit()) {
				return scanNegativeNumber(ch);
			}
		}
		else if (ch.isBackQuote() && input.peek().isBackQuote()) {
			return scanComment();
		}
		else if (ch.isDigit()) {
			return scanNumber(ch);
		}
		else if (ch.isIdentifier()) {
			return scanIdentifier(ch);
		}
		else if (isPunctuatorStart(ch)) {
			return PunctuatorScanner.scan(ch, input);
		}
		else if (ch.isSingleQuote()) {
			return scanChar(ch);
		}
		else if (isEndOfInput(ch)) {
			return NullToken.make(ch.getLocation());
		}
		lexicalError(ch);
		return findNextToken();
	}

	private LocatedChar nextNonWhitespaceChar()
	{
		LocatedChar ch = input.next();
		while (ch.isWhitespace()) {
			ch = input.next();
		}
		return ch;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Character lexical analysis

	private Token scanChar(LocatedChar firstChar)
	{
		// discard the singleQuote
		LocatedChar c = input.next();

		if (c.isCharacter()) {
			return CharacterToken.make(firstChar.getLocation(), c.getCharacter());
		}
		else {
			lexicalError(firstChar);
			return findNextToken();
		}

	}

	// ////////////////////////////////////////////////////////////////////////////
	// Comment lexical analysis

	// discard the unneeded comments
	private Token scanComment()
	{
		// eat both `` to start the comment
		LocatedChar c = input.next();
		c = input.next();
		// [^`\n]*
		while (!c.isBackQuote() && !c.isNewLine()) {
			c = input.next();
		}
		// (`[^`\n]+)*
		while (c.isBackQuote() && !(input.peek().isBackQuote() || input.peek().isNewLine())) {
			c = input.next();
			while (!c.isBackQuote() && !c.isNewLine()) {
				c = input.next();
			}
		}
		// (`` | `\n | \n)
		if ((c.isBackQuote() && input.peek().isBackQuote()) || (c.isBackQuote() && input.peek().isNewLine())) {
			c = input.next();
			c = input.next();
		}
		else if (c.isNewLine()) {
			c = input.next();
		}

		input.pushback(c);
		return findNextToken();
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Integer lexical analysis

	private Token scanNumber(LocatedChar firstChar)
	{
		return appendSubsequentDigits(firstChar);
	}

	// returns true if it is just a integer, false if it is a float
	private Token appendSubsequentDigits(LocatedChar firstChar)
	{
		StringBuffer buffer = new StringBuffer();
		LocatedChar c = input.next();
		// -? [0..9]+
		if (firstChar.isDigit() || firstChar.isHyphenMinus()) {
			buffer.append(firstChar.getCharacter());
			while (c.isDigit()) {
				buffer.append(c.getCharacter());
				c = input.next();
			}
		}
		else {
			lexicalError(firstChar);
			return findNextToken();
		}
		// .
		if (c.isDecimalPoint()) {
			// consume the '.'
			buffer.append(c.getCharacter());
			c = input.next();
			// [0..9]+
			if (c.isDigit()) {
				while (c.isDigit()) {
					buffer.append(c.getCharacter());
					c = input.next();
				}
			}
			else {
				lexicalError(c);
				return findNextToken();
			}
			// (e -? [0..9]+)?
			if (c.isExponentStart()) {
				// consume the 'e'
				buffer.append(c.getCharacter());
				c = input.next();
				if (c.isHyphenMinus()) {
					// consume the '-'
					buffer.append(c.getCharacter());
					c = input.next();
				}
				if (c.isDigit()) {
					while (c.isDigit()) {
						buffer.append(c.getCharacter());
						c = input.next();
					}
				}
				else {
					lexicalError(c);
					return findNextToken();
				}
			}
			input.pushback(c);
			return FloatToken.make(firstChar.getLocation(), buffer.toString());
		}
		input.pushback(c);
		return IntegerToken.make(firstChar.getLocation(), buffer.toString());
	}

	private Token scanNegativeNumber(LocatedChar firstChar)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(firstChar.getCharacter());
		return appendSubsequentDigits(firstChar);
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Identifier and keyword lexical analysis

	private Token scanIdentifier(LocatedChar firstChar)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(firstChar.getCharacter());
		if (appendSubsequentIdentifers(buffer) > 36) {
			identiferError(firstChar, buffer.toString());
		}

		String lexeme = buffer.toString();
		if (Keyword.isAKeyword(lexeme)) {
			return LextantToken.make(firstChar.getLocation(), lexeme, Keyword.forLexeme(lexeme));
		}
		else {
			return IdentifierToken.make(firstChar.getLocation(), lexeme);
		}
	}

	private int appendSubsequentIdentifers(StringBuffer buffer)
	{
		LocatedChar c = input.next();

		int identiferLengthCounter = 1;
		// [a.zA..Z_~0..9]* max 36 chars
		while (c.isLowerCase() || c.isUpperCase() || c.isUnderScore() || c.isDigit() || c.isTilde()) {
			identiferLengthCounter++;
			buffer.append(c.getCharacter());
			c = input.next();
		}
		input.pushback(c);
		return identiferLengthCounter;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Character-classification routines specific to Towhee scanning.

	private boolean isPunctuatorStart(LocatedChar lc)
	{
		char c = lc.getCharacter();
		return isPunctuatorStartingCharacter(c);
	}

	private boolean isEndOfInput(LocatedChar lc)
	{
		return lc == LocatedCharStream.FLAG_END_OF_INPUT;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// Error-reporting

	private void lexicalError(LocatedChar ch)
	{
		TowheeLogger log = TowheeLogger.getLogger("compiler.lexicalAnalyzer");
		log.severe("Lexical error: invalid character " + ch);
	}

	private void identiferError(LocatedChar ch, String token)
	{
		TowheeLogger log = TowheeLogger.getLogger("compiler.lexicalAnalyzer");
		log.severe("Lexcial error: " + token + " is over 36 chars " + ch.getLocation());
	}

}
