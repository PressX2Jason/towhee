package lexicalAnalyzer;

import tokens.LextantToken;
import tokens.Token;

public enum Keyword implements Lextant
{
	// declarations
	IMMUTABLE("final"), MUTABLE("var"),

	// update
	UPDATE("update"),

	// print statements
	PRINT("print"), PRINTLN("println"), SPLAT("splat"), SPLATLN("splatln"),

	// types
	BOOLEAN("bool"), CHAR("char"), INTEGER("int"), FLOAT("float"), VOID("void"),

	// boolean values
	TRUE("true"), FALSE("false"),

	// container
	CONTAINER("container"), NULL_KEYWORD(""),

	// loops
	IF("if"), ELSE("else"), WHILE("while"),
	
	//array
	PERSIST("persist"), RELEASE("release"),
	
	//function
	RETURN("return"), CALL("call"), DEFINE("def"), THIS("this");

	private String	lexeme;
	private Token	prototype;

	private Keyword(String lexeme)
	{
		this.lexeme = lexeme;
		this.prototype = LextantToken.make(null, lexeme, this);
	}

	public String getLexeme()
	{
		return lexeme;
	}

	public Token prototype()
	{
		return prototype;
	}

	public static Keyword forLexeme(String lexeme)
	{
		for (Keyword keyword : values()) {
			if (keyword.lexeme.equals(lexeme)) {
				return keyword;
			}
		}
		return NULL_KEYWORD;
	}

	public static boolean isAKeyword(String lexeme)
	{
		return forLexeme(lexeme) != NULL_KEYWORD;
	}

	/*
	 * the following can replace the implementation of forLexeme above. It is faster but less clear. private static LexemeMap<Keyword> lexemeToKeyword = new LexemeMap<Keyword>(values(), NULL_KEYWORD);
	 * public static Keyword forLexeme(String lexeme) { return lexemeToKeyword.forLexeme(lexeme); }
	 */
}
