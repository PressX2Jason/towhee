package lexicalAnalyzer;

import tokens.LextantToken;
import tokens.Token;

public enum Punctuator implements Lextant
{
	// arithmetic operators
	ADD("+"), SUBTRACT("-"), MULTIPLY("*"), DIVIDE("/"),

	// Comparison operators
	GREATER_THAN(">"), GREATER_THAN_OR_EQUAL(">="), EQUAL("=="), NOT_EQUAL("!="), LESS_THAN("<"), LESS_THAN_OR_EQUAL("<="),
	
	//Boolean Operators
	 SHORT_CIRCUIT_AND("&&"), SHORT_CIRCUIT_OR("||"), NOT("!"),

	// punctuation
	TERMINATOR(";"), SPLICE(","), OPEN_BRACE("{"), CLOSE_BRACE("}"), OPEN_PARENTHESE("("), CLOSE_PARENTHESE(")"), OPEN_SQUARE_BRACKET("["), CLOSE_SQUARE_BRACKET("]"), ASSIGN(":="), COLON(":"), AT_SIGN("@"),
	
	//Member operator
	MEMBER_ACCESS("."),	
	
	//value body
	OPEN_VALUE_BODY("{["), CLOSE_VALUE_BODY("]}"),
	

	// null
	NULL_PUNCTUATOR("");

	private String	lexeme;
	private Token	prototype;

	private Punctuator(String lexeme)
	{
		this.lexeme = lexeme;
		this.prototype = LextantToken.make(null, lexeme, this);
	}

	public String getLexeme()
	{
		return lexeme;
	}

	public Token prototype()
	{
		return prototype;
	}

	public static Punctuator forLexeme(String lexeme)
	{
		for (Punctuator punctuator : values()) {
			if (punctuator.lexeme.equals(lexeme)) {
				return punctuator;
			}
		}
		return NULL_PUNCTUATOR;
	}

	public static boolean isComparisonOperator(Token token)
	{
		return token.isLextant(Punctuator.GREATER_THAN) || token.isLextant(Punctuator.GREATER_THAN_OR_EQUAL) ||
				token.isLextant(Punctuator.LESS_THAN) || token.isLextant(Punctuator.LESS_THAN_OR_EQUAL) ||
				token.isLextant(Punctuator.EQUAL) || token.isLextant(Punctuator.NOT_EQUAL);
	}
	
	public static boolean isAdditiveOperater(Token token){
		return token.isLextant(Punctuator.ADD) || token.isLextant(Punctuator.SUBTRACT);
	}
	
	public static boolean isBooleanOperator(Lextant operation){
		return operation.equals(Punctuator.SHORT_CIRCUIT_AND) || operation.equals(Punctuator.SHORT_CIRCUIT_OR) || operation.equals(Punctuator.NOT);
	}

	public static boolean isComparisonOperator(Lextant operator)
	{
		return operator.equals(Punctuator.GREATER_THAN) || operator.equals(Punctuator.GREATER_THAN_OR_EQUAL) ||
				operator.equals(Punctuator.LESS_THAN) || operator.equals(Punctuator.LESS_THAN_OR_EQUAL) ||
				operator.equals(Punctuator.EQUAL) || operator.equals(Punctuator.NOT_EQUAL);
	}

	/*
	 * // the following can replace the implementation of forLexeme above. It is faster but less clear. private static LexemeMap<Punctuator> lexemeToPunctuator = new LexemeMap<Punctuator>(values(),
	 * NULL_PUNCTUATOR); public static Punctuator forLexeme(String lexeme) { return lexemeToPunctuator.forLexeme(lexeme); }
	 */

}
