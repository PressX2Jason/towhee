package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFalse;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static semanticAnalyzer.PrimitiveType.BOOLEAN;

import java.util.Arrays;

import parseTree.nodeTypes.WhileStatementNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class WhileCodeGenerator extends CodeGenerator {

	public WhileCodeGenerator()
	{
	}

	public static ASMCodeFragment generateCode(WhileStatementNode node, ASMCodeFragment condition, ASMCodeFragment whileBody)
	{
		assert node.nChildren() == 2;
		assert condition != null && whileBody != null;

		Type arg1Type = node.child(0).getType();

		Labeller labeller = getLabeller();
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);

		String startLabel = labeller.newLabel("-while-start-", "");
		String contitionLabel = labeller.newLabel("-while-condition-", "");
		String whileBodyLabel = labeller.newLabelSameNumber("-while-ifBody-", "");
		String exitLabel = labeller.newLabelSameNumber("-while-exit-", "");

		if (arg1Type == BOOLEAN) {
			code.add(Label, startLabel);
			code.add(Label, contitionLabel);
			code.append(condition);
			code.add(JumpFalse, exitLabel);
			code.add(Label, whileBodyLabel);
			code.append(whileBody);
			code.add(Jump, startLabel);
			code.add(Label, exitLabel);
		}
		else {
			typeCheckError(node, Arrays.asList(arg1Type));
		}
		return code;
	}
}
