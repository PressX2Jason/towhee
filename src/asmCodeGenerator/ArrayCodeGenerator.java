package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.Add;
import static asmCodeGenerator.codeStorage.ASMOpcode.Duplicate;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpTrue;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadI;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Subtract;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.runtime.RunTime;

public class ArrayCodeGenerator extends CodeGenerator {

	public static void pointerToTypeID(ASMCodeFragment code)
	{
		// [ ... p ]
		code.add(LoadI);															// [ ... typeID ]
	}

	public static void pointerToStatusFlag(ASMCodeFragment code)
	{
		code.add(PushI, 4);															// [ ... p 4 ]
		code.add(Add);																// [ ... p+4 ]
		code.add(LoadI);															// [ ... statusFlag ]
	}

	public static void pointerToSubTypeSize(ASMCodeFragment code)
	{
		code.add(PushI, 8);															// [ ... p 8 ]
		code.add(Add);																// [ ... p+8 ]
		code.add(LoadI);															// [ ... SubTypeSize ]
	}

	public static void pointerToLowIndex(ASMCodeFragment code)
	{
		code.add(PushI, 12);														// [ ... p 12 ]
		code.add(Add);																// [ ... p+12 ]
		code.add(LoadI);															// [ ... LowIndex ]
	}

	public static void pointerToNumberElements(ASMCodeFragment code)
	{
		code.add(PushI, 16);														// [ ... p 16 ]
		code.add(Add);																// [ ... p+16 ]
		code.add(LoadI);															// [ ... arrayLength ]
	}

	public static void pointerToEndOfHeader(ASMCodeFragment code)
	{
		code.add(PushI, 20);														// [ ... p 20 ]
		code.add(Add);																// [ ... p+20 ]
	}

	public static void storePointer(ASMCodeFragment code)
	{
		Macros.storeITo(code, RunTime.ARRAY_POINTER);								// [ ... ]
	}

	public static void loadPointer(ASMCodeFragment code)
	{
		Macros.loadIFrom(code, RunTime.ARRAY_POINTER);								// [ ... p ]
	}

	public static void storeIndex(ASMCodeFragment code)
	{
		Macros.storeITo(code, RunTime.ARRAY_INDEX_POINTER);							// [ ... ]
	}

	public static void loadIndex(ASMCodeFragment code)
	{
		Macros.loadIFrom(code, RunTime.ARRAY_INDEX_POINTER);						// [ ... i ]
	}

	public static void pointerToArrayIsValidToPointer(ASMCodeFragment code)
	{
		// [ ... p ]
		code.add(Duplicate);														// [ ... p p ]
		pointerToTypeID(code);														// [ ... p typeID ]
		code.add(PushI, 5);															// [ ... p typeID 5]
		code.add(Subtract);															// [ ... p result ]
		code.add(JumpTrue, RunTime.ARRAY_DEALLOCATED_RUNTIME_ERROR);				// [ ... p ]
	}

	public static void jumpIfPointerIsInvalid(ASMCodeFragment code, String truelabel, String FalseLabel)
	{
		// [ ... p ]
		code.add(Duplicate);														// [ ... p p ]
		pointerToTypeID(code);														// [ ... p typeID ]
		code.add(PushI, 5);															// [ ... p typeID 5]
		code.add(Subtract);															// [ ... p result ]
		code.add(JumpTrue, truelabel);												// [ ... p ]
		code.add(Jump, FalseLabel);													// [ ... p ]
	}

	public static void pointerSetTypeIDToInvalid(ASMCodeFragment code)
	{
		// [ ... p ]
		code.add(Duplicate);														// [ ... p p ]
		code.add(PushI, 0);															// [ ... p p 0 ]
		code.add(StoreI);															// [ ... p ]
	}

	public static void pointerToHighIndex(ASMCodeFragment code)
	{
		// [ ... p ]
		storePointer(code);															// [ ... ]
		loadPointer(code);															// [ ... p ]
		pointerToNumberElements(code);												// [ ... nElements ]
		loadPointer(code);															// [ ... nElements p ]
		pointerToLowIndex(code);													// [ ... nElements lowIndex ]
		code.add(PushI, 1);															// [ ... nElements lowIndex 1 ]
		code.add(Subtract);															// [ ... nElements lowIndex-1 ]
		code.add(Add);																// [ ... highIndex]
	}
}
