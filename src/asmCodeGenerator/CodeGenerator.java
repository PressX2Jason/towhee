package asmCodeGenerator;

import java.util.List;

import logging.TowheeLogger;
import parseTree.ParseNode;
import semanticAnalyzer.Type;
import tokens.Token;

public class CodeGenerator {

	public static Labeller getLabeller()
	{
		return ASMCodeGenerator.getLabeller();
	}

	// /////////////////////////////////////////////////////////////////
	// Error Reporting

	protected static void typeCheckError(ParseNode node, List<Type> operandTypes)
	{
		Token token = node.getToken();

		logError("operator " + token.getLexeme() + " not defined for types " + operandTypes + " at " + token.getLocation());
	}

	private static void logError(String message)
	{
		TowheeLogger log = TowheeLogger.getLogger("asmCodeGenerator.CodeGenerator");
		log.severe(message);
	}
}
