package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFalse;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static semanticAnalyzer.PrimitiveType.BOOLEAN;

import java.util.Arrays;

import parseTree.nodeTypes.IfStatementNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class IfCodeGenerator extends CodeGenerator {

	public IfCodeGenerator()
	{
	}

	public static ASMCodeFragment generateCode(IfStatementNode node, ASMCodeFragment condition, ASMCodeFragment ifBody, ASMCodeFragment elseBody)
	{
		assert node.nChildren() == 2 || node.nChildren() == 3;
		assert condition != null && ifBody != null;
		if (node.nChildren() == 3) {
			assert elseBody != null;
		}

		Type arg1Type = node.child(0).getType();

		Labeller labeller = getLabeller();
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);

		String contitionLabel = labeller.newLabel("-if-condition-", "");
		String ifBodyLabel = labeller.newLabelSameNumber("-if-ifBody-", "");
		String elseLabel = labeller.newLabelSameNumber("-if-elseBody-", "");
		String joinLabel = labeller.newLabelSameNumber("-if-join-", "");

		if (arg1Type == BOOLEAN) {
			if (node.nChildren() == 2) {
				code.add(Label, contitionLabel);
				code.append(condition);
				code.add(JumpFalse, joinLabel);
				code.add(Label, ifBodyLabel);
				code.append(ifBody);
				code.add(Label, joinLabel);
			}
			else {
				code.add(Label, contitionLabel);
				code.append(condition);
				code.add(JumpFalse, elseLabel);
				code.add(Label, ifBodyLabel);
				code.append(ifBody);
				code.add(Jump, joinLabel);
				code.add(Label, elseLabel);
				code.append(elseBody);
				code.add(Label, joinLabel);
			}
		}
		else {
			typeCheckError(node, Arrays.asList(arg1Type));
		}
		return code;
	}
}
