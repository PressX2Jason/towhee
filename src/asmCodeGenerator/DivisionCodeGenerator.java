package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.Divide;
import static asmCodeGenerator.codeStorage.ASMOpcode.Duplicate;
import static asmCodeGenerator.codeStorage.ASMOpcode.FDivide;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFZero;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFalse;
import static semanticAnalyzer.PrimitiveType.FLOAT;
import static semanticAnalyzer.PrimitiveType.INTEGER;

import java.util.Arrays;

import parseTree.nodeTypes.OperatorNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.runtime.RunTime;

public class DivisionCodeGenerator extends CodeGenerator {

	public DivisionCodeGenerator()
	{
	}

	public ASMCodeFragment generateCode(OperatorNode node)
	{
		assert node.nChildren() == 2;

		Type arg1Type = node.child(0).getType();
		Type arg2Type = node.child(1).getType();

		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);
		// A != B -> A - B != 0; jumpTrue if result is zero. false otherwise
		if ((arg1Type == INTEGER && arg2Type == INTEGER)) {
			code.add(Duplicate);
			code.add(JumpFalse, RunTime.INTEGER_DIVIDE_BY_ZERO_RUNTIME_ERROR);
			code.add(Divide);
		}
		else if (arg1Type == FLOAT && arg2Type == FLOAT) {
			code.add(Duplicate);
			code.add(JumpFZero, RunTime.FLOAT_DIVIDE_BY_ZERO_RUNTIME_ERROR);
			code.add(FDivide);
		}
		else {
			typeCheckError(node, Arrays.asList(arg1Type, arg2Type));
		}
		return code;
	}

}
