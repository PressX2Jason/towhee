package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFalse;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static semanticAnalyzer.PrimitiveType.BOOLEAN;

import java.util.Arrays;

import parseTree.nodeTypes.OperatorNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class ShortCircuitAndCodeGenerator extends CodeGenerator {

	private static String	DESCRIPTION	= "-short-circuit-and-";

	public ShortCircuitAndCodeGenerator()
	{
	}

	public ASMCodeFragment generateCode(OperatorNode node, ASMCodeFragment arg1, ASMCodeFragment arg2)
	{
		assert node.nChildren() == 2;
		assert arg1 != null && arg2 != null;

		Type arg1Type = node.child(0).getType();
		Type arg2Type = node.child(1).getType();

		Labeller labeller = getLabeller();
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);

		String startLabel = labeller.newLabel("-compare-arg1-", DESCRIPTION);
		String arg2Label = labeller.newLabelSameNumber("-compare-arg2-", DESCRIPTION);
		String falseLabel = labeller.newLabelSameNumber("-compare-false-", DESCRIPTION);
		String joinLabel = labeller.newLabelSameNumber("-compare-join-", DESCRIPTION);

		/*if(a)
		 *	b;
		 *else
		 *	false;
		 */
		if (arg1Type == BOOLEAN && arg2Type == BOOLEAN){
			code.add(Label, startLabel);
			code.append(arg1);
			code.add(JumpFalse, falseLabel);
			code.add(Label, arg2Label);
			code.append(arg2);
			code.add(Jump, joinLabel);
			code.add(Label, falseLabel);
			code.add(PushI, 0);
			code.add(Label, joinLabel);
		}else {
			typeCheckError(node, Arrays.asList(arg1Type, arg2Type));
		}
		return code;
	}
}
