package asmCodeGenerator;

import static asmCodeGenerator.ArrayCodeGenerator.jumpIfPointerIsInvalid;
import static asmCodeGenerator.ArrayCodeGenerator.loadIndex;
import static asmCodeGenerator.ArrayCodeGenerator.loadPointer;
import static asmCodeGenerator.ArrayCodeGenerator.pointerSetTypeIDToInvalid;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToArrayIsValidToPointer;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToEndOfHeader;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToHighIndex;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToLowIndex;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToNumberElements;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToStatusFlag;
import static asmCodeGenerator.ArrayCodeGenerator.pointerToSubTypeSize;
import static asmCodeGenerator.ArrayCodeGenerator.storeIndex;
import static asmCodeGenerator.ArrayCodeGenerator.storePointer;
import static asmCodeGenerator.FramePointerCodeGenerator.loadAndDecrementFP;
import static asmCodeGenerator.FramePointerCodeGenerator.loadAndIncrementFP;
import static asmCodeGenerator.FramePointerCodeGenerator.loadFP;
import static asmCodeGenerator.Macros.storeITo;
import static asmCodeGenerator.StackPointerCodeGenerator.decrementAndStoreSP;
import static asmCodeGenerator.StackPointerCodeGenerator.getReturnValue;
import static asmCodeGenerator.StackPointerCodeGenerator.incrementAndStoreSP;
import static asmCodeGenerator.StackPointerCodeGenerator.loadAndDecrementSP;
import static asmCodeGenerator.StackPointerCodeGenerator.loadAndIncrementSP;
import static asmCodeGenerator.StackPointerCodeGenerator.loadSP;
import static asmCodeGenerator.StackPointerCodeGenerator.storeReturnValue;
import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_ADDRESS;
import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VOID;
import static asmCodeGenerator.codeStorage.ASMOpcode.Add;
import static asmCodeGenerator.codeStorage.ASMOpcode.BTAnd;
import static asmCodeGenerator.codeStorage.ASMOpcode.Call;
import static asmCodeGenerator.codeStorage.ASMOpcode.DLabel;
import static asmCodeGenerator.codeStorage.ASMOpcode.DataZ;
import static asmCodeGenerator.codeStorage.ASMOpcode.Duplicate;
import static asmCodeGenerator.codeStorage.ASMOpcode.Exchange;
import static asmCodeGenerator.codeStorage.ASMOpcode.Halt;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFalse;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpNeg;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpPos;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpTrue;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadC;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadF;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Multiply;
import static asmCodeGenerator.codeStorage.ASMOpcode.Pop;
import static asmCodeGenerator.codeStorage.ASMOpcode.Printf;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushD;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushF;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Return;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreC;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreF;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Subtract;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import parseTree.nodeTypes.ArrayCreationNode;
import parseTree.nodeTypes.ArrayIndexNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BodyNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallStatementNode;
import parseTree.nodeTypes.CharacterConstantNode;
import parseTree.nodeTypes.ContainerCreationNode;
import parseTree.nodeTypes.ContainerDeclarationNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.FloatConstantNode;
import parseTree.nodeTypes.FunctionDeclarationNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IfStatementNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.MemberAccessNode;
import parseTree.nodeTypes.OperatorNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReleaseStatementNode;
import parseTree.nodeTypes.ReturnStatementNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.UpdateNode;
import parseTree.nodeTypes.ValueBodyNode;
import parseTree.nodeTypes.WhileStatementNode;
import semanticAnalyzer.ArrayType;
import semanticAnalyzer.ContainerType;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import symbolTable.Binding;
import symbolTable.Scope;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMOpcode;
import asmCodeGenerator.runtime.MemoryManager;
import asmCodeGenerator.runtime.RunTime;

// do not call the code generator if any errors have occurred during analysis.
public class ASMCodeGenerator {
	private static Labeller		labeller			= new Labeller();
	ParseNode					root;
	private static ParseNode	mainContainerNode	= null;

	public static ASMCodeFragment generate(ParseNode syntaxTree)
	{
		ASMCodeGenerator codeGenerator = new ASMCodeGenerator(syntaxTree);
		return codeGenerator.makeASM();
	}

	public ASMCodeGenerator(ParseNode root)
	{
		super();
		this.root = root;
	}

	public static Labeller getLabeller()
	{
		return labeller;
	}

	public ASMCodeFragment makeASM()
	{
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.append(MemoryManager.codeForInitialization());
		code.append(RunTime.getEnvironment());
		code.append(globalVariableBlockASM());
		code.append(programASM());
		code.append(MemoryManager.codeForAfterApplication());

		return code;
	}

	private ASMCodeFragment globalVariableBlockASM()
	{
		assert root.hasScope();
		Scope scope = root.getScope();
		int globalBlockSize = scope.getAllocatedSize();

		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.add(DLabel, RunTime.GLOBAL_MEMORY_BLOCK);
		code.add(DataZ, globalBlockSize);
		return code;
	}

	private ASMCodeFragment programASM()
	{
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);

		code.add(Label, RunTime.MAIN_PROGRAM_LABEL);
		code.append(programCode());

		// generate mainContainer

		// allocating a record (of size 8+identifierNode's-scope-size) and pushing the pointer to that record on the frame stack
		// so that it becomes the this pointer to the creation code function
		ContainerType containerType = (ContainerType) mainContainerNode.getType();

		code.add(PushI, 8 + containerRecordSize(mainContainerNode));									// [ ... size ]
		code.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE);												// [ ... p ]

		// set up the header
		code.add(Duplicate);																			// [ ... p p ]
		code.add(PushI, containerType.getTypeID());														// [ ... p p typeID ]
		code.add(StoreI);																				// [ ... p ]

		code.add(Duplicate);																			// [ ... p p statusFlag ]
		code.add(PushI, containerType.getStatusFlag());													// [ ... p p statusFlag ]
		code.add(StoreI);																				// [ ... p ]

		decrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... p ]
		loadSP(code);																					// [ ... p sp ]
		code.add(Exchange);																				// [ ... sp p ]
		code.add(StoreI);																				// [ ... ]

		// placing an arbitrary (e.g. null) static link on the frame stack,
		code.add(PushI, -1);																			// [ ... nullStaticLink ]
		decrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... nullStaticLink ]
		loadSP(code);																					// [ ... nullStaticLink sp ]
		code.add(Exchange);																				// [ ... sp nullStaticLink ]
		code.add(StoreI);																				// [ ... ]

		code.add(Call, mainContainerNode.child(0).getBinding().getUniqueLabel());
		code.add(Halt);

		return code;
	}
	
	private int containerRecordSize(ParseNode node)
	{
		final int CONTAINER_HEADER_SIZE = 8;
		ContainerType containerType = (ContainerType) node.getType();
		return CONTAINER_HEADER_SIZE + containerType.getScopeSize();
	}

	private ASMCodeFragment programCode()
	{
		CodeVisitor visitor = new CodeVisitor();
		root.accept(visitor);

		for (ParseNode child : root.getChildren()) {
			if (child instanceof ContainerDeclarationNode) {
				if (child.child(0).getToken().getLexeme().equals("main")) {
					mainContainerNode = child;
					break;
				}
			}
		}

		return visitor.removeRootCode(root);
	}

	private class CodeVisitor extends ParseNodeVisitor.Default {
		private Map<ParseNode, ASMCodeFragment>	codeMap;
		ASMCodeFragment							code;

		public CodeVisitor()
		{
			codeMap = new HashMap<ParseNode, ASMCodeFragment>();
		}

		// //////////////////////////////////////////////////////////////////
		// Make the field "code" refer to a new fragment of different sorts.
		private void newAddressCode(ParseNode node)
		{
			code = new ASMCodeFragment(GENERATES_ADDRESS); // Address code puts new address on the stack
															// (Abstract State Machine stack)
			codeMap.put(node, code);
		}

		private void newValueCode(ParseNode node)
		{
			code = new ASMCodeFragment(GENERATES_VALUE); // value code puts a new value on the stack
			codeMap.put(node, code);
		}

		private void newVoidCode(ParseNode node)
		{
			code = new ASMCodeFragment(GENERATES_VOID); // void code does not put anything on the stack
			codeMap.put(node, code);
		}

		// //////////////////////////////////////////////////////////////////
		// Get code from the map.
		private ASMCodeFragment getAndRemoveCode(ParseNode node)
		{
			ASMCodeFragment result = codeMap.get(node);
			codeMap.remove(result);
			return result;
		}

		public ASMCodeFragment removeRootCode(ParseNode tree)
		{
			return getAndRemoveCode(tree);
		}

		private ASMCodeFragment removeValueCode(ParseNode node)
		{
			ASMCodeFragment frag = getAndRemoveCode(node);
			makeFragmentValueCode(frag, node);
			return frag;
		}

		private ASMCodeFragment removeAddressCode(ParseNode node)
		{
			ASMCodeFragment frag = getAndRemoveCode(node);
			assert frag.isAddress();
			return frag;
		}

		private ASMCodeFragment removeVoidCode(ParseNode node)
		{
			ASMCodeFragment frag = getAndRemoveCode(node);
			assert frag.isVoid();
			return frag;
		}

		// //////////////////////////////////////////////////////////////////
		// convert code to value-generating code.
		private void makeFragmentValueCode(ASMCodeFragment code, ParseNode node)
		{
			assert !code.isVoid();

			if (code.isAddress()) {
				turnAddressIntoValue(code, node);
			}
		}

		private void turnAddressIntoValue(ASMCodeFragment code, ParseNode node)
		{
			if (node.getType() == PrimitiveType.INTEGER) {
				code.add(LoadI);
			}
			else if (node.getType() == PrimitiveType.BOOLEAN) {
				code.add(LoadC);
			}
			else if (node.getType() == PrimitiveType.FLOAT) {
				code.add(LoadF);
			}
			else if (node.getType() == PrimitiveType.CHAR) {
				code.add(LoadC);
			}
			else if (node.getType() instanceof ArrayType) {
				code.add(LoadI);
			}
			code.markAsValue();
		}

		// //////////////////////////////////////////////////////////////////
		// ensures all types of ParseNode in given AST have at least a visitLeave
		public void visitLeave(ParseNode node)
		{
			assert false : "node " + node + " not handled in ASMCodeGenerator";
		}

		// /////////////////////////////////////////////////////////////////////////
		// constructs larger than statements
		public void visitLeave(ProgramNode node)
		{
			newVoidCode(node);
			for (ParseNode child : node.getChildren()) {
				ASMCodeFragment childCode = removeVoidCode(child);
				code.append(childCode);
			}
		}

		public void visitLeave(ContainerDeclarationNode node)
		{
			newVoidCode(node);
			IdentifierNode idNode = (IdentifierNode) node.child(0);
			Labeller labeller = getLabeller();

			String containerStart = idNode.getBinding().getUniqueLabel();
			String containerEnd = labeller.newLabel("$container-" + idNode.getIdentifier() + "-end", "");

			code.add(Jump, containerEnd);
			code.add(Label, containerStart);
			containerCalleeEntranceHandShake(node);
			// performs the operations specified by the creation code,
			code.add(Label, labeller.newLabel("debug",""));
			code.append(removeVoidCode(node.getBodyNode()));												// [ ... p value]
			// returns its this pointer
			containerCalleeExitHandShake(containerEnd);
			code.add(Label, containerEnd);
		}

		private void containerCalleeEntranceHandShake(ContainerDeclarationNode node)
		{
			// performs the usual callee-function-enter handshaking, but using a frame size of 8, which is
			loadAndDecrementSP(code, 4);																		// [ ... rtnAddr sp-4 ]
			loadFP(code);																						// [ ... rtnAddr sp-4 fp ]
			code.add(StoreI);																					// [ ... rtnAddr ]
			loadAndDecrementSP(code, 8);																		// [ ... rtnAddr sp-8 ]
			code.add(Exchange);																					// [ ... sp-8 rtnAddr ]
			code.add(StoreI);																					// [ ... ]
			loadSP(code);																						// [ ... sp ]
			storeITo(code, RunTime.FRAME_POINTER);																// [ ... ]
			decrementAndStoreSP(code, 8);																		// [ ... ]

			// Copies the function arguments (but not the this pointer or static link) into the appropriate place in the record.
			if (node.hasParameters()) {
				int frameOffset = -node.getParameters().get(0).getType().getSize();
				int recordOffset = 0;
				for (ParseNode child : node.getParameters()) {
					frameOffset += child.getType().getSize();
					loadAndIncrementSP(code, 12);																	// [ ... sp-12 ]
					code.add(LoadI);																				// [ ... thisPtr ]
					code.add(PushI, 8 + recordOffset);																// [ ... thisPtr 8 ]
					code.add(Add);																					// [ ... thisPtr+8 ]
					loadAndIncrementFP(code, distanceToParameters(node, frameOffset)); 								// [ ... thisPtr+8 fp+4+parameterScope ]
					code.add(opcodeForLoad(child.getType()));														// [ ... thisPtr+8 param ]
					code.add(opcodeForStore(child.getType()));														// [ ... ]
					recordOffset += child.getType().getSize();
				}
			}
		}

		private int distanceToParameters(ContainerDeclarationNode node, int frameOffset)
		{
			final int STATIC_LINK_SIZE = 4;
			return STATIC_LINK_SIZE + node.getParamterScopeSize() - frameOffset - node.getContainedVariableSize();
		}

		private void containerCalleeExitHandShake(String containerEnd)
		{
			loadAndIncrementSP(code, 12);																	// [ ... sp+12 ]
			code.add(LoadI);																				// [ ... thisPtr ]
			
			// performs the usual callee-function-return handshaking, using a frame size of 8
			loadAndDecrementFP(code, 8);																	// [ ... thisPtr fp-8 ]
			code.add(LoadI);																				// [ ... thisPtr rtnAddr ]
			loadAndDecrementFP(code, 4);																	// [ ... thisPtr rtnAddr fp-4 ]
			code.add(LoadI);																				// [ ... thisPtr rtnAddr dynamiclink ]
			storeITo(code, RunTime.FRAME_POINTER);															// [ ... thisPtr rtnAddr ]
			
			incrementAndStoreSP(code, 8);																	// [ ... thisPtr rtnAddr ]
			decrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... thisPtr rtnAddr ]
			storeReturnValue(code, PrimitiveType.INTEGER);													// [ ... rtnAddr ]
			code.add(Return);																				// [ ... ]
		}

		public void visitLeave(ContainerCreationNode node)
		{
			newValueCode(node);
			containerCallerEntranceHandShake(node);
			// calling creationCode, which should return its this pointer
			code.add(Call, node.child(0).getBinding().getUniqueLabel());									// [ ... ]
			containerCallerExitHandShake();
		}

		private void containerCallerExitHandShake()
		{
			loadSP(code);																					// [ ... sp ]
			code.add(LoadI);																				// [ ... returnValue ]
			incrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... returnValue ]
		}

		public void containerCallerEntranceHandShake(ContainerCreationNode node)
		{
			// calculating the argument expressions, in order, and pushing them on the frame stack
			for (ParseNode argument : node.getArguments()) {
				code.append(removeValueCode(argument));														// [ ... arg ]
				decrementAndStoreSP(code, argument.getType());												// [ ... arg ]
				loadSP(code);																				// [ ... arg sp ]
				code.add(Exchange);																			// [ ... sp arg ]
				code.add(opcodeForStore(argument.getType()));												// [ ... ]
			}

			// allocating a record (of size 8+identifierNode's-scope-size) and pushing the pointer to that record on the frame stack
			// so that it becomes the this pointer to the creation code function
			ContainerType containerType = (ContainerType) node.getType();

			code.add(PushI, containerRecordSize(node));														// [ ... size ]
			code.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE);												// [ ... p ]

			// set up the header
			code.add(Duplicate);																			// [ ... p p typeID ]
			code.add(PushI, containerType.getTypeID());														// [ ... p p typeID ]
			code.add(StoreI);																				// [ ... p ]

			code.add(Duplicate);																			// [ ... p p statusFlag ]
			code.add(PushI, containerType.getStatusFlag());													// [ ... p p statusFlag ]
			code.add(StoreI);																				// [ ... p ]

			decrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... p ]
			loadSP(code);																					// [ ... p sp ]
			code.add(Exchange);																				// [ ... sp p ]
			code.add(StoreI);																				// [ ... ]

			// placing an arbitrary (e.g. null) static link on the frame stack,
			code.add(PushI, -1);																			// [ ... nullStaticLink ]
			decrementAndStoreSP(code, PrimitiveType.INTEGER);												// [ ... nullStaticLink ]
			loadSP(code);																					// [ ... nullStaticLink sp ]
			code.add(Exchange);																				// [ ... sp nullStaticLink ]
			code.add(StoreI);																				// [ ... ]
		}

		private int containerRecordSize(ContainerCreationNode node)
		{
			final int CONTAINER_HEADER_SIZE = 8;
			ContainerType containerType = (ContainerType) node.getType();
			return CONTAINER_HEADER_SIZE + containerType.getScopeSize();
		}

		public void visitLeave(BodyNode node)
		{
			newVoidCode(node);
			for (ParseNode child : node.getChildren()) {
				ASMCodeFragment childCode = removeVoidCode(child);
				code.append(childCode);
			}
			List<Binding> arrayBindingList = node.getScope().getSymbolTable().getAllArrays();
			for (Binding binding : arrayBindingList) {
				implicitReleaseArray(binding);
			}
		}

		private void implicitReleaseArray(Binding binding)
		{
			binding.getMemoryLocation().getAccessor()
					.generateAddress(code, binding.getMemoryLocation().getBaseAddress(), binding.getMemoryLocation().getOffset(), 0);
			code.add(LoadI);
			appendImplicitReleaseCode(binding.getType());								// [ ... ]
		}

		private void appendImplicitReleaseCode(Type type)
		{
			if (type instanceof ArrayType) {
				ArrayType arrayType = (ArrayType) type;
				if (arrayType.getSubType() instanceof ArrayType) {
					appendImplicitReleaseReferenceCode((ArrayType) type);
				}
				else {
					appendImplicitReleaseValueCode(type);
				}
			}
		}

		private void appendImplicitReleaseValueCode(Type type)
		{
			Labeller labeller = getLabeller();
			String validTypeID = labeller.newLabel("$implicit-release-valid-typeID-", "");
			String invalidTypeID = labeller.newLabelSameNumber("$implicit-release-invalid-typeID-", "");
			String presistBitSet = labeller.newLabelSameNumber("$implicit-release-persist-bit-set-", "");
			String end = labeller.newLabelSameNumber("$implicit-release-end-", "");

			jumpIfPointerIsInvalid(code, invalidTypeID, validTypeID);						// [ ... p ]

			code.add(Label, validTypeID);
			code.add(Duplicate);															// [ ... p p ]
			pointerToStatusFlag(code);														// [ ... p sFlag ]
			code.add(PushI, 0x1);															// [ ... p sFlag 1]
			code.add(BTAnd);																// [ ... p firstbit ]
			code.add(PushI, 1);																// [ ... p firstbit 1 ]
			code.add(Subtract);																// [ ... p result ]
			code.add(JumpFalse, presistBitSet);												// [ ... p ]

			pointerSetTypeIDToInvalid(code);												// [ ... p ]
			code.add(Call, MemoryManager.MEM_MANAGER_DEALLOCATE);							// [ ... ]
			code.add(Jump, end);

			code.add(Label, presistBitSet);													// [ .. p ]
			code.add(Label, invalidTypeID);													// [ .. p ]
			code.add(Pop);																	// [ ... ]
			code.add(Label, end);
		}

		private void appendImplicitReleaseReferenceCode(ArrayType type)
		{
			Labeller labeller = getLabeller();
			String startLoop = labeller.newLabel("$start-array-loop", "");
			String exitLoop = labeller.newLabelSameNumber("$exit-array-loop", "");

			pointerToArrayIsValidToPointer(code);											// [ ... p ]

			// assuming p is on the top of the stack
			storePointer(code);																// [ ... ]
			loadPointer(code);																// [ ... p ]
			pointerToNumberElements(code);													// [ ... nElement]
			code.add(JumpFalse, exitLoop);													// [ ... ]
			code.add(PushI, 0);																// [ ... index ]

			// $start-loop
			code.add(Label, startLoop);

			// load an array element, e.g. LoadI arrayAddress + elementSize*index + headerOffset
			code.add(Duplicate);															// [ ... index index ]
			loadPointer(code);																// [ ... index index p ]
			pointerToSubTypeSize(code);														// [ ... index index elementSize ]
			code.add(Multiply);																// [ ... index index*elementSize ]
			loadPointer(code);																// [ ... index index*elementSize p ]
			pointerToEndOfHeader(code);														// [ ... index index*elementSize endOfHeaderLocation*]
			code.add(Add);																	// [ ... index array[index]* ]
			code.add(opcodeForLoad(type.getSubType()));        								// [ ... index element ]

			// recursively call append array code, this with print a value or recurse back into this routine
			loadPointer(code);                 											    // [ ... index element p]
			code.add(Exchange);                                      						// [ ... index p element]
			appendExplicitReleaseCode(type.getSubType());											// [ ... index p ]

			// store p now that the recursion is done
			storePointer(code);               												// [ ... index ]

			// increment index
			code.add(PushI, 1);																// [ ... index 1 ]
			code.add(Add);																	// [ ... index+1 ]

			// if index < numElement not done so print ' ,' else jump to $exit-loop
			code.add(Duplicate);															// [ ... index+1 index+1 ]
			loadPointer(code);																// [ ... index+1 index+1 p ]
			pointerToNumberElements(code);													// [ ... index+1 index+1 nElement]

			// A < B -> A - B < 0
			code.add(Subtract);																// [ ... index+1 index+1-nElement]
			code.add(JumpNeg, startLoop);													// [ ... index+1 ]
			code.add(Jump, exitLoop);														// [ ... index+1 ]

			// $exit-loop
			code.add(Label, exitLoop);														// [ ... index+1 ]
			code.add(Pop);																	// [ ... ]
		}

		public void visitEnter(ValueBodyNode node)
		{
			Labeller labeller = getLabeller();
			node.setEndBodyLabel(labeller.newLabel("$valueBody-end-", ""));
		}

		public void visitLeave(ValueBodyNode node)
		{
			newValueCode(node);
			for (ParseNode child : node.getChildren()) {
				code.append(removeVoidCode(child));
			}
			code.add(Label, node.getEndBodyLabel());

			List<Binding> arrayBindingList = node.getScope().getSymbolTable().getAllArrays();
			for (Binding binding : arrayBindingList) {
				implicitReleaseArray(binding);
			}
		}

		public void visitLeave(FunctionDeclarationNode node)
		{
			newVoidCode(node);
			String label = node.child(0).getBinding().getUniqueLabel();
			String functionEnd = getLabeller().newLabel("$ParseNode-end", "");

			code.add(Jump, functionEnd);

			code.add(Label, label);
			calleeEntranceHandshake(node);
			code.append(removeValueCode(node.child(3)));
			calleeExitHandshake(node);
			code.add(Label, functionEnd);
		}

		private void calleeEntranceHandshake(FunctionDeclarationNode node)
		{
			loadAndDecrementSP(code, 4);																	// [ ... rtnAddr sp-4 ]
			loadFP(code);																					// [ ... rtnAddr sp-4 fp ]
			code.add(StoreI);																				// [ ... rtnAddr ]
			loadAndDecrementSP(code, 8);																	// [ ... rtnAddr sp-8 ]
			code.add(Exchange);																				// [ ... sp-8 rtnAddr ]
			code.add(StoreI);																				// [ ... ]
			loadSP(code);																					// [ ... sp ]
			storeITo(code, RunTime.FRAME_POINTER);															// [ ... ]
			decrementAndStoreSP(code, node.getProcedureScopeSize());										// [ ... ]
		}

		private void calleeExitHandshake(FunctionDeclarationNode node)
		{
			loadAndDecrementFP(code, 8);																	// [ ... rtnValue fp-8 ]
			code.add(LoadI);																				// [ ... rtnValue rtnAddr ]
			loadAndDecrementFP(code, 4);																	// [ ... rtnValue rtnAddr fp-4 ]
			code.add(LoadI);																				// [ ... rtnValue rtnAddr dynamiclink ]
			storeITo(code, RunTime.FRAME_POINTER);															// [ ... rtnValue rtnAddr ]
			incrementAndStoreSP(code, node.getParameterScope() + node.getProcedureScopeSize());				// [ ... rtnValue rtnAddr ]
			decrementAndStoreSP(code, node.getValueBodyReturnType());										// [ ... rtnValue rtnAddr ]
			storeReturnValue(code, node.getValueBodyReturnType());											// [ ... rtnAddr ]
			code.add(Return);																				// [ ... ]
		}

		// /////////////////////////////////////////////////////////////////////////
		// statements and declarations

		public void visitLeave(CallStatementNode node)
		{
			newVoidCode(node);
			callerEntranceHandShake(node);
			code.add(Call, node.child(0).getBinding().getUniqueLabel());
			callerExitHandShake(node);
		}

		public void visitLeave(FunctionInvocationNode node)
		{
			newValueCode(node);
			callerEntranceHandShake(node);
			code.add(Call, node.child(0).getBinding().getUniqueLabel());
			callerExitHandShake(node);
		}

		private void callerEntranceHandShake(FunctionNode node)
		{
			for (ParseNode argument : node.getArguments()) {
				code.append(removeValueCode(argument));													// [ ... arg ]
				decrementAndStoreSP(code, argument.getType());												// [ ... arg ]
				loadSP(code);																				// [ ... arg sp ]
				code.add(Exchange);																			// [ ... sp arg ]
				code.add(opcodeForStore(argument.getType()));												// [ ... ]
			}
			decrementAndStoreSP(code, 4);																	// [ ... ]
			loadSP(code);																					// [ ... sp ]
			createStaticLink(node);																			// [ ... sp staticLink ]
			code.add(StoreI);																				// [ ... ]
		}

		private void createStaticLink(FunctionNode node)
		{
			int callerSNL = 0;
			for (ParseNode parent : node.pathToRoot()) {
				if (parent instanceof FunctionDeclarationNode) {
					callerSNL++;
				}
			}

			if (callerSNL == 0) {
				code.add(PushI, 0);
				return;
			}

			int calleeSNL = node.child(0).getBinding().getMemoryLocation().getSNL();
			int n = callerSNL - calleeSNL;

			// System.out.println("n: " + n);
			code.add(PushD, RunTime.FRAME_POINTER);
			for (int i = 0; i < n + 1; i++) {
				code.add(LoadI);
			}
		}

		private void callerExitHandShake(ParseNode node)
		{
			getReturnValue(code, node.getType());															// [ ... returnValue ]
		}

		public void visitLeave(ReturnStatementNode node)
		{
			ValueBodyNode owner = node.findOwner();

			newVoidCode(node);
			if (node.nChildren() == 0) {
				return;
			}
			code.append(removeValueCode(node.child(0)));
			code.add(Jump, owner.getEndBodyLabel());
		}

		public void visitLeave(PrintStatementNode node)
		{
			newVoidCode(node);

			for (ParseNode child : node.getChildren()) {
				appendPrintCode(child);
				appendSpacerCode(node);
			}
			appendNewlineCode(node);
		}

		private void appendNewlineCode(PrintStatementNode node)
		{
			if (node.hasNewline()) {
				code.add(PushD, RunTime.NEWLINE_PRINT_FORMAT);
				code.add(Printf);
			}
		}

		private void appendSpacerCode(PrintStatementNode node)
		{
			if (node.hasSpaces()) {
				code.add(PushD, RunTime.PRINT_SPACER_STRING);
				code.add(Printf);
			}
		}

		private void appendPrintCode(ParseNode node)
		{
			if (node.getType() instanceof ArrayType) {
				code.append(removeValueCode(node));
				printValueCode(node.getType());
			}
			else if (node.getType() instanceof PrimitiveType) {
				String format = printFormat(node.getType());
				code.append(removeValueCode(node));
				convertToStringIfBoolean(node);
				code.add(PushD, format);
				code.add(Printf);
			}
		}

		private void printValueCode(Type type)
		{
			if (type instanceof PrimitiveType) {
				appendPrintPrimitiveCode(type);
			}
			else if (type instanceof ArrayType) {
				appendPrintArrayCode((ArrayType) type);
			}
		}

		private void appendPrintPrimitiveCode(Type type)
		{
			String format = printFormat(type);
			convertToStringIfBoolean(type);
			code.add(PushD, format);
			code.add(Printf);
		}

		private void appendPrintArrayCode(ArrayType type)
		{
			Labeller labeller = getLabeller();
			String startLoop = labeller.newLabel("$start-array-loop", "");
			String exitLoop = labeller.newLabelSameNumber("$exit-array-loop", "");
			String exitLoopNoPop = labeller.newLabelSameNumber("$exit-array-loop-no-pop", "");
			String continueLoop = labeller.newLabelSameNumber("$continue-array-loop", "");

			pointerToArrayIsValidToPointer(code);											// [ ... p ]

			// print '['
			code.add(PushD, RunTime.PRINT_ARRAY_START_STRING);
			code.add(Printf);

			// assuming p is on the top of the stack
			storePointer(code);																// [ ... ]
			loadPointer(code);																// [ ... p ]
			pointerToNumberElements(code);													// [ ... nElement]
			code.add(JumpFalse, exitLoopNoPop);												// [ ... ]
			code.add(PushI, 0);																// [ ... index ]

			// $start-loop
			code.add(Label, startLoop);

			// load an array element, e.g. LoadI arrayAddress + elementSize*index + headerOffset
			code.add(Duplicate);															// [ ... index index ]
			loadPointer(code);																// [ ... index index p ]
			pointerToSubTypeSize(code);														// [ ... index index elementSize ]
			code.add(Multiply);																// [ ... index index*elementSize ]
			loadPointer(code);																// [ ... index index*elementSize p ]
			pointerToEndOfHeader(code);														// [ ... index index*elementSize endOfHeaderLocation*]
			code.add(Add);																	// [ ... index array[index]* ]
			code.add(opcodeForLoad(type.getSubType()));        								// [ ... index element ]

			// recursively call append array code, this with print a value or recurse back into this routine
			loadPointer(code);                 											    // [ ... index element p]
			code.add(Exchange);                                      						// [ ... index p element]
			printValueCode(type.getSubType());												// [ ... index p ]

			// store p now that the recursion is done
			storePointer(code);               												// [ ... index ]

			// increment index
			code.add(PushI, 1);																// [ ... index 1 ]
			code.add(Add);																	// [ ... index+1 ]

			// if index < numElement not done so print ' ,' else jump to $exit-loop
			code.add(Duplicate);															// [ ... index+1 index+1 ]
			loadPointer(code);																// [ ... index+1 index+1 p ]
			pointerToNumberElements(code);													// [ ... index+1 index+1 nElement]

			// A < B -> A - B < 0
			code.add(Subtract);																// [ ... index+1 index+1-nElement]
			code.add(JumpNeg, continueLoop);												// [ ... index+1 ]
			code.add(Jump, exitLoop);														// [ ... index+1 ]

			code.add(Label, continueLoop);
			// ','
			code.add(PushD, RunTime.PRINT_SPLICE_STRING);
			code.add(Printf);

			// ' '
			code.add(PushD, RunTime.PRINT_SPACER_STRING);
			code.add(Printf);

			// Jump $start-loop
			code.add(Jump, startLoop);

			// $exit-loop
			code.add(Label, exitLoop);														// [ ... index+1 ]
			code.add(Pop);																	// [ ... ]
			code.add(Label, exitLoopNoPop);

			// print ']'
			code.add(PushD, RunTime.PRINT_ARRAY_END_STRING);
			code.add(Printf);
		}

		private void convertToStringIfBoolean(ParseNode node)
		{
			if (node.getType() != PrimitiveType.BOOLEAN) {
				return;
			}

			String trueLabel = labeller.newLabel("-print-boolean-true", "");
			String endLabel = labeller.newLabelSameNumber("-print-boolean-join", "");

			code.add(JumpTrue, trueLabel);
			code.add(PushD, RunTime.BOOLEAN_FALSE_STRING);
			code.add(Jump, endLabel);
			code.add(Label, trueLabel);
			code.add(PushD, RunTime.BOOLEAN_TRUE_STRING);
			code.add(Label, endLabel);
		}

		private void convertToStringIfBoolean(Type type)
		{
			if (type != PrimitiveType.BOOLEAN) {
				return;
			}

			String trueLabel = labeller.newLabel("-print-boolean-true", "");
			String endLabel = labeller.newLabelSameNumber("-print-boolean-join", "");

			code.add(JumpTrue, trueLabel);
			code.add(PushD, RunTime.BOOLEAN_FALSE_STRING);
			code.add(Jump, endLabel);
			code.add(Label, trueLabel);
			code.add(PushD, RunTime.BOOLEAN_TRUE_STRING);
			code.add(Label, endLabel);
		}

		private String printFormat(Type type)
		{
			assert type instanceof PrimitiveType;
			switch ((PrimitiveType) type)
			{
				case INTEGER:
					return RunTime.INTEGER_PRINT_FORMAT;
				case BOOLEAN:
					return RunTime.BOOLEAN_PRINT_FORMAT;
				case FLOAT:
					return RunTime.FLOAT_PRINT_FORMAT;
				case CHAR:
					return RunTime.CHAR_PRINT_FORMAT;
				default:
					assert false : "Primitive Type " + type + " unimplemented in ASMCodeGenerator.printFormat()";
					return "";
			}
		}

		public void visitLeave(ArrayCreationNode node)
		{
			assert node.getType() instanceof ArrayType;

			newValueCode(node);
			ArrayType arrayType = (ArrayType) node.getType();
			// set up the header
			// allocate enough memory
			if (node.getToken().isLextant(Punctuator.AT_SIGN)) {
				// size of the array has to be calculated at run time for empty array creation
				code.append(removeValueCode(node.child(1)));							// [ ... nElements ]
				// array size cannot be negaive
				code.add(Duplicate);													// [ ... nElements nElements ]
				code.add(Duplicate);													// [ ... nElements nElements nElements]
				code.add(JumpNeg, RunTime.ARRAY_LENGTH_LESS_THAN_ZERO_RUNTIME_ERROR);	// [ ... nElements nElements ]
				code.add(PushI, arrayType.getSubTypeSize());							// [ ... nElements nElements subElementSize ]
				code.add(Multiply);														// [ ... nElements nElements*subElementSize ]
				code.add(PushI, arrayType.getHeaderSize());								// [ ... nElements nElements*subElementSize 20]
				code.add(Add);															// [ ... nElements nElements memoryNeeded ]
				code.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE); 					// [ ... nElements p ]

				// push typeId
				code.add(Duplicate); 													// [ ... nElements p p ]
				code.add(PushI, arrayType.getTypeID()); 								// [ ... nElements p p 5 ] <- the typeID
				code.add(StoreI); 														// [ ... nElements p ]

				// push statusFlag
				code.add(Duplicate); 													// [ ... nElements p p ]
				code.add(PushI, 4); 													// [ ... nElements p p 4 ]
				code.add(Add); 															// [ ... nElements p p+4 ]
				code.add(PushI, arrayType.getStatusFlag()); 							// [ ... nElements p p+4 statusFlag ]
				code.add(StoreI); 														// [ ... nElements p ]

				// push subTypeSize
				code.add(Duplicate); 													// [ ... nElements p p ]
				code.add(PushI, 8); 													// [ ... nElements p p 8 ]
				code.add(Add); 															// [ ... nElements p p+8 ]
				code.add(PushI, arrayType.getSubTypeSize());	 						// [ ... nElements p p+8 subTypeSize ]
				code.add(StoreI); 														// [ ... nElements p ]

				// push lowerIndex
				if (node.nChildren() == 3) {
					code.add(Duplicate); 												// [ ... nElements p p ]
					code.add(PushI, 12); 												// [ ... nElements p p 12 ]
					code.add(Add); 														// [ ... nElements p p+12 ]
					code.append(removeValueCode(node.child(2))); 						// [ ... nElements p p+12 lowerIndex]
					code.add(StoreI); 													// [ ... nElements p ]
				}
				else {
					code.add(Duplicate); 												// [ ... nElements p p ]
					code.add(PushI, 12); 												// [ ... nElements p p 12 ]
					code.add(Add); 														// [ ... nElements p p+12 ]
					code.add(PushI, 0); 												// [ ... nElements p p+12 0] <- default is 0
					code.add(StoreI); 													// [ ... nElements p ]
				}

				// push length
				code.add(Duplicate); 													// [ ... nElements p p ]
				code.add(PushI, 16); 													// [ ... nElements p p 16 ]
				code.add(Add); 															// [ ... nElements p p+16 ]
				code.add(Exchange);														// [ ... nElements p+16 p ]
				storePointer(code);														// [ ... nElements p+16 ]
				code.add(Exchange);														// [ ... p+16 nElements ]
				code.add(StoreI); 														// [ ... ]
				loadPointer(code);														// [ ... p ]

			}
			else if (node.getToken().isLextant(Punctuator.OPEN_SQUARE_BRACKET)) {
				code.add(PushI, arrayType.getRecordSize()); 							// [ ... size ]
				code.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE); 					// [ ... p ]

				// push typeId
				code.add(Duplicate); 													// [ ... p p ]
				code.add(PushI, arrayType.getTypeID()); 								// [ ... p p 5 ] <- the typeID
				code.add(StoreI); 														// [ ... p ]

				// push statusFlag
				code.add(Duplicate); 													// [ ... p p ]
				code.add(PushI, 4); 													// [ ... p p 4 ]
				code.add(Add); 															// [ ... p p+4 ]
				code.add(PushI, arrayType.getStatusFlag()); 							// [ ... p p+4 statusFlag ]
				code.add(StoreI); 														// [ ... p ]

				// push subTypeSize
				code.add(Duplicate); 													// [ ... p p ]
				code.add(PushI, 8); 													// [ ... p p 8 ]
				code.add(Add); 															// [ ... p p+8 ]
				code.add(PushI, arrayType.getSubTypeSize());		 					// [ ... p p+8 subTypeSize ]
				code.add(StoreI); 														// [ ... p ]

				// push lowerIndex
				code.add(Duplicate); 													// [ ... p p ]
				code.add(PushI, 12); 													// [ ... p p 12 ]
				code.add(Add); 															// [ ... p p+12 ]
				code.add(PushI, arrayType.getLowerIndex()); 							// [ ... p p+12 lowerIndex]
				code.add(StoreI); 														// [ ... p ]

				// push length
				code.add(Duplicate); 													// [ ... p p ]
				code.add(PushI, 16); 													// [ ... p p 16 ]
				code.add(Add); 															// [ ... p p+16 ]
				code.add(PushI, arrayType.nElements()); 								// [ ... p p+16 length]
				code.add(StoreI); 														// [ ... p ]

				if (node.nChildren() == 0) {
					assert false : "ASMCodeGenerator Error: empty array creation cannot have zero elements";
				}

				ASMOpcode storeCode = opcodeForStore(arrayType.getSubType());
				int i = 0;
				for (ParseNode child : node.getChildren()) {
					code.add(Duplicate); 												// [ ... p p ]
					code.add(PushI, 20 + (i++ * arrayType.getSubTypeSize())); 			// [ ... p p offset]
					code.add(Add); 														// [ ... p p+offset ]
					code.append(removeValueCode(child)); 								// [ ... p p+offset childCode]
					code.add(storeCode); 												// [ ... p ]
				}
			}
		}

		public void visitLeave(ArrayIndexNode node)
		{
			newAddressCode(node);

			ASMCodeFragment pointerToArray = removeValueCode(node.child(0));
			ASMCodeFragment Index = removeValueCode(node.child(1));

			code.append(pointerToArray);										// [ ... p ]
			pointerToArrayIsValidToPointer(code);										// [ ... p ]
			storePointer(code);															// [ ... ]
			loadPointer(code);															// [ ... p ]
			pointerToHighIndex(code);													// [ ... highIndex ]
			code.append(Index);															// [ ... highIndex index ]
			storeIndex(code);															// [ ... highIndex ]
			loadIndex(code);															// [ ... highIndex index ]
			code.add(Exchange);															// [ ... index highIndex ]
			code.add(Subtract);															// [ ... 0BasedIndex ]

			// if index-highIndex > 0 then Runtime array indexing error
			code.add(JumpPos, RunTime.ARRAY_INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);			// [ ... ]

			loadPointer(code);															// [ ... p ]
			pointerToLowIndex(code);													// [ ... lowIndex ]
			loadIndex(code);															// [ ... lowIndex index]
			code.add(Exchange);															// [ ... index lowIndex]

			// if i - lowIdex < 0 then Runtime array indexing error
			code.add(Subtract);															// [ ... 0BasedIndex ]
			code.add(Duplicate);														// [ ... 0BasedIndex 0BasedIndex ]
			code.add(JumpNeg, RunTime.ARRAY_INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);			// [ ... 0BasedIndex ]

			loadPointer(code);															// [ ... 0BasedIndex p ]
			pointerToSubTypeSize(code);													// [ ... 0BasedIndex subTypeSize ]
			code.add(Multiply);															// [ ... offset ]

			loadPointer(code);															// [ ... p ]
			pointerToEndOfHeader(code);													// [ ... offset endOfHeaderLocation*]
			code.add(Add);																// [ ... array[offset]* ]
		}

		public void visitLeave(ReleaseStatementNode node)
		{
			newVoidCode(node);

			for (ParseNode child : node.getChildren()) {
				code.append(removeValueCode(child));									// [ ... p ]
				appendExplicitReleaseCode(child.getType());
			}
		}

		private void appendExplicitReleaseCode(Type type)
		{
			if (type instanceof ArrayType) {
				ArrayType arrayType = (ArrayType) type;
				if (arrayType.getSubType() instanceof ArrayType) {
					appendExplicitReleaseReferenceCode((ArrayType) type);
				}
				else {
					appendExplicitReleaseValueCode(type);
				}
			}
		}

		private void appendExplicitReleaseValueCode(Type type)
		{
			Labeller labeller = getLabeller();
			String validTypeID = labeller.newLabel("$explicit-release-value-valid-typeID-", "");
			String invalidTypeID = labeller.newLabelSameNumber("$explicit-release-value-invalid-typeID-", "");
			String end = labeller.newLabelSameNumber("$explicit-release-value-end", "");

			jumpIfPointerIsInvalid(code, invalidTypeID, validTypeID);						// [ ... p ]

			code.add(Label, validTypeID);
			pointerSetTypeIDToInvalid(code);												// [ ... p ]
			code.add(Call, MemoryManager.MEM_MANAGER_DEALLOCATE);							// [ ... ]
			code.add(Jump, end);

			code.add(Label, invalidTypeID);													// [ .. p ]
			code.add(Pop);																	// [ ... ]
			code.add(Label, end);
		}

		private void appendExplicitReleaseReferenceCode(ArrayType type)
		{
			Labeller labeller = getLabeller();
			String startLoop = labeller.newLabel("$start-array-loop", "");
			String exitLoop = labeller.newLabelSameNumber("$exit-array-loop", "");

			pointerToArrayIsValidToPointer(code);											// [ ... p ]

			// assuming p is on the top of the stack
			storePointer(code);																// [ ... ]
			loadPointer(code);																// [ ... p ]
			pointerToNumberElements(code);													// [ ... nElement]
			code.add(JumpFalse, exitLoop);													// [ ... ]
			code.add(PushI, 0);																// [ ... index ]

			// $start-loop
			code.add(Label, startLoop);

			// load an array element, e.g. LoadI arrayAddress + elementSize*index + headerOffset
			code.add(Duplicate);															// [ ... index index ]
			loadPointer(code);																// [ ... index index p ]
			pointerToSubTypeSize(code);														// [ ... index index elementSize ]
			code.add(Multiply);																// [ ... index index*elementSize ]
			loadPointer(code);																// [ ... index index*elementSize p ]
			pointerToEndOfHeader(code);														// [ ... index index*elementSize endOfHeaderLocation*]
			code.add(Add);																	// [ ... index array[index]* ]
			code.add(opcodeForLoad(type.getSubType()));        								// [ ... index element ]

			// recursively call append array code, this with print a value or recurse back into this routine
			loadPointer(code);                 											    // [ ... index element p]
			code.add(Exchange);                                      						// [ ... index p element]
			appendExplicitReleaseCode(type.getSubType());											// [ ... index p ]

			// store p now that the recursion is done
			storePointer(code);               												// [ ... index ]

			// increment index
			code.add(PushI, 1);																// [ ... index 1 ]
			code.add(Add);																	// [ ... index+1 ]

			// if index < numElement not done so print ' ,' else jump to $exit-loop
			code.add(Duplicate);															// [ ... index+1 index+1 ]
			loadPointer(code);																// [ ... index+1 index+1 p ]
			pointerToNumberElements(code);													// [ ... index+1 index+1 nElement]

			// A < B -> A - B < 0
			code.add(Subtract);																// [ ... index+1 index+1-nElement]
			code.add(JumpNeg, startLoop);													// [ ... index+1 ]
			code.add(Jump, exitLoop);														// [ ... index+1 ]

			// $exit-loop
			code.add(Label, exitLoop);														// [ ... index+1 ]
			code.add(Pop);																	// [ ... ]
		}

		public void visitLeave(DeclarationNode node)
		{
			newVoidCode(node);
			ASMCodeFragment lvalue = removeAddressCode(node.child(0));
			ASMCodeFragment rvalue = removeValueCode(node.child(1));

			code.append(lvalue);
			code.append(rvalue);

			Type type = node.getType();
			code.add(opcodeForStore(type));
		}

		public void visitLeave(UpdateNode node)
		{
			newVoidCode(node);

			if (node.child(0).getType() instanceof ArrayType) {
				ASMCodeFragment lvalue = removeAddressCode(node.child(0));
				ASMCodeFragment rvalue = removeValueCode(node.child(1));

				code.append(lvalue);													// [ ... p* ]
				code.add(Duplicate);													// [ ... p* p* ]
				code.add(LoadI);														// [ ... p* p ]
				appendImplicitReleaseCode(node.child(0).getType());						// [ ... p* ]
				code.append(rvalue);
			}
			else {

				ASMCodeFragment lvalue = removeAddressCode(node.child(0));
				ASMCodeFragment rvalue = removeValueCode(node.child(1));

				code.append(lvalue);
				code.append(rvalue);
			}

			Type type = node.getType();
			code.add(opcodeForStore(type));
		}

		public void visitLeave(MemberAccessNode node)
		{
			newValueCode(node);
			ASMCodeFragment target = removeValueCode(node.child(0));
			code.append(target);
			// [ ... p ]
			// check if array is valid
			pointerToArrayIsValidToPointer(code);
			switch (node.getMember())
			{
				case HIGH:
					pointerToHighIndex(code);												// [ ... highIndex ]
					break;
				case LENGTH:
					pointerToNumberElements(code);											// [ ... length ]
					break;
				case LOW:
					pointerToLowIndex(code);												// [ ... lowIndex ]
					break;
				default:
					break;
			}
		}

		public void visitLeave(IfStatementNode node)
		{
			newVoidCode(node);

			ASMCodeFragment condition = removeValueCode(node.child(0));
			ASMCodeFragment ifBody = new ASMCodeFragment(GENERATES_VOID);
			for (ParseNode child : node.child(1).getChildren()) {
				ifBody.append(removeVoidCode(child));
			}

			ASMCodeFragment elseBody = null;

			if (node.nChildren() == 3) {
				elseBody = removeVoidCode(node.child(2));
			}
			code.append(IfCodeGenerator.generateCode(node, condition, ifBody, elseBody));

		}

		public void visitLeave(WhileStatementNode node)
		{
			newVoidCode(node);

			ASMCodeFragment condition = removeValueCode(node.child(0));
			ASMCodeFragment whileBody = removeVoidCode(node.child(1));

			code.append(WhileCodeGenerator.generateCode(node, condition, whileBody));

		}

		private ASMOpcode opcodeForStore(Type type)
		{
			if (type == PrimitiveType.INTEGER) {
				return StoreI;
			}
			if (type == PrimitiveType.BOOLEAN) {
				return StoreC;
			}
			if (type == PrimitiveType.FLOAT) {
				return StoreF;
			}
			if (type == PrimitiveType.CHAR) {
				return StoreC;
			}
			if (type instanceof ArrayType) {
				return StoreI;
			}
			if (type instanceof ContainerType) {
				return StoreI;
			}
			assert false : "Type " + type + " unimplemented in opcodeForStore()";
			return null;
		}

		private ASMOpcode opcodeForLoad(Type type)
		{
			if (type == PrimitiveType.INTEGER) {
				return LoadI;
			}
			if (type == PrimitiveType.BOOLEAN) {
				return LoadC;
			}
			if (type == PrimitiveType.FLOAT) {
				return LoadF;
			}
			if (type == PrimitiveType.CHAR) {
				return LoadC;
			}
			if (type instanceof ArrayType) {
				return LoadI;
			}
			assert false : "Type " + type + " unimplemented in opcodeForLoad()";
			return null;
		}

		// /////////////////////////////////////////////////////////////////////////
		// expressions
		public void visitLeave(BinaryOperatorNode node)
		{
			Lextant operator = node.getOperator();

			if (Punctuator.isComparisonOperator(operator)) {
				visitComparisonOperatorNode(node, operator);
			}
			else if (operator.equals(Punctuator.COLON)) {
				visitCastBinaryOperatorNode(node);
			}
			else if (Punctuator.isBooleanOperator(operator)) {
				visitBooleanBinaryOperatorNode(node);
			}
			else {
				visitNormalBinaryOperatorNode(node);
			}
		}

		private void visitBooleanBinaryOperatorNode(BinaryOperatorNode node)
		{
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = removeValueCode(node.child(1));
			getAndAppendOpcode(node, arg1, arg2);
		}

		private void visitCastBinaryOperatorNode(BinaryOperatorNode node)
		{
			newValueCode(node);
			Type castToType = node.child(1).getType();
			node.setType(castToType);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			code.append(arg1);
			getAndAppendOpcode(node);
		}

		public void visitLeave(UnaryOperatorNode node)
		{
			visitNormalUnaryOperatorNode(node);
		}

		// expressions
		private void visitComparisonOperatorNode(BinaryOperatorNode node, Lextant operator)
		{
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = removeValueCode(node.child(1));
			newValueCode(node);
			getAndAppendOpcode(node, arg1, arg2);
		}

		private void visitNormalBinaryOperatorNode(BinaryOperatorNode node)
		{
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = removeValueCode(node.child(1));
			code.append(arg1);
			code.append(arg2);
			getAndAppendOpcode(node);
		}

		private void visitNormalUnaryOperatorNode(UnaryOperatorNode node)
		{
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			code.append(arg1);
			getAndAppendOpcode(node);
		}

		private void getAndAppendOpcode(OperatorNode node)
		{
			getAndAppendOpcode(node, null, null);
		}

		private void getAndAppendOpcode(OperatorNode node, ASMCodeFragment arg1, ASMCodeFragment arg2)
		{
			Object opcodeObject = node.getSignature().getVariant();
			if (opcodeObject instanceof ASMOpcode) {
				code.add((ASMOpcode) node.getSignature().getVariant());
			}
			else if (opcodeObject instanceof CodeGenerator) {
				// Arithmetic operators
				if (opcodeObject instanceof DivisionCodeGenerator) {
					code.append(((DivisionCodeGenerator) opcodeObject).generateCode(node));
				}

				// Casting operator
				else if (opcodeObject instanceof CastCodeGenerator) {
					code.append(((CastCodeGenerator) opcodeObject).generateCode(node));
				}

				// Comparison operators
				else if (opcodeObject instanceof EqualToCodeGenerator) {
					code.append(((EqualToCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof NotEqualToCodeGenerator) {
					code.append(((NotEqualToCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof GreaterThanCodeGenerator) {
					code.append(((GreaterThanCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof GreaterThanOrEqualCodeGenerator) {
					code.append(((GreaterThanOrEqualCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof LessThanCodeGenerator) {
					code.append(((LessThanCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof LessThanOrEqualCodeGenerator) {
					code.append(((LessThanOrEqualCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}

				// Boolean operators
				else if (opcodeObject instanceof ShortCircuitAndCodeGenerator) {
					code.append(((ShortCircuitAndCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else if (opcodeObject instanceof ShortCircuitOrCodeGenerator) {
					code.append(((ShortCircuitOrCodeGenerator) opcodeObject).generateCode(node, arg1, arg2));
				}
				else {
					assert false : "getAndAppendOpcode not defined for " + node.toString();
				}
			}
		}

		// /////////////////////////////////////////////////////////////////////////
		// leaf nodes (ErrorNode not necessary)
		public void visit(BooleanConstantNode node)
		{
			newValueCode(node);
			code.add(PushI, node.getValue() ? 1 : 0);
		}

		public void visit(IdentifierNode node)
		{
			newAddressCode(node);
			Binding binding = node.getBinding();

			binding.generateAddress(code, calculateSNL(node));
		}

		private int calculateSNL(ParseNode node)
		{
			int snl = 0;
			for (ParseNode parent : node.pathToRoot()) {
				if (parent instanceof FunctionDeclarationNode) {
					snl++;
				}
			}
			return snl;
		}

		public void visit(IntegerConstantNode node)
		{
			newValueCode(node);
			code.add(PushI, node.getValue());
		}

		public void visit(FloatConstantNode node)
		{
			newValueCode(node);
			code.add(PushF, node.getValue());
		}

		public void visit(CharacterConstantNode node)
		{
			newValueCode(node);
			code.add(PushI, node.getValue() & 0x7F);
		}

	}

}
