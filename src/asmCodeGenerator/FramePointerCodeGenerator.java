package asmCodeGenerator;

import static asmCodeGenerator.Macros.loadIFrom;
import static asmCodeGenerator.codeStorage.ASMOpcode.Add;
import static asmCodeGenerator.codeStorage.ASMOpcode.PStack;
import static asmCodeGenerator.codeStorage.ASMOpcode.Pop;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Subtract;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.runtime.RunTime;

public class FramePointerCodeGenerator extends CodeGenerator {

	public static void loadFP(ASMCodeFragment code)
	{
		// [ ... ]
		loadIFrom(code,  RunTime.FRAME_POINTER); 				//[ ... fp ]
	}
	
	public static void loadAndDecrementFP(ASMCodeFragment code, int value)
	{
		loadFP(code);										// [ ... fp ]
		code.add(PushI, value);								// [ ... fp value ]
		code.add(Subtract);									// [ ... fp-value ]
	}
	
	public static void printFP(ASMCodeFragment code){
		loadFP(code);
		code.add(PStack);
		code.add(Pop);
	}
	
	public static void loadAndIncrementFP(ASMCodeFragment code, int value)
	{
		loadFP(code);										// [ ... fp ]
		code.add(PushI, value);								// [ ... fp value ]
		code.add(Add);										// [ ... fp+value ]
	}
}