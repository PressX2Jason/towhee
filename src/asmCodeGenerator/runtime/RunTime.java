package asmCodeGenerator.runtime;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VOID;
import static asmCodeGenerator.codeStorage.ASMOpcode.DLabel;
import static asmCodeGenerator.codeStorage.ASMOpcode.DataD;
import static asmCodeGenerator.codeStorage.ASMOpcode.DataI;
import static asmCodeGenerator.codeStorage.ASMOpcode.DataS;
import static asmCodeGenerator.codeStorage.ASMOpcode.DataZ;
import static asmCodeGenerator.codeStorage.ASMOpcode.Halt;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static asmCodeGenerator.codeStorage.ASMOpcode.Memtop;
import static asmCodeGenerator.codeStorage.ASMOpcode.Printf;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushD;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreI;
import semanticAnalyzer.ContainerType;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class RunTime {
	public static final String	EAT_LOCATION_ZERO							= "$eat-location-zero";			// helps us distinguish null
																												// pointers from real ones.
	public static final String	ARRAY_POINTER								= "$array-pointer";
	public static final String	ARRAY_INDEX_POINTER							= "$array-index-pointer";

	public static final String	INTEGER_PRINT_FORMAT						= "$print-format-integer";
	public static final String	FLOAT_PRINT_FORMAT							= "$print-format-float";
	public static final String	CHAR_PRINT_FORMAT							= "$print-format-char";
	public static final String	BOOLEAN_PRINT_FORMAT						= "$print-format-boolean";
	public static final String	NEWLINE_PRINT_FORMAT						= "$print-format-newline";
	public static final String	BOOLEAN_TRUE_STRING							= "$boolean-true-string";
	public static final String	BOOLEAN_FALSE_STRING						= "$boolean-false-string";
	public static final String	PRINT_SPACER_STRING							= "$print-spacer-string";
	public static final String	PRINT_SPLICE_STRING							= "$print-splice-string";

	public static final String	PRINT_ARRAY_START_STRING					= "$print-array-start-string";
	public static final String	PRINT_ARRAY_END_STRING						= "$print-array-end-string";

	public static final String	GLOBAL_MEMORY_BLOCK							= "$global-memory-block";
	public static final String	FRAME_POINTER								= "$frame-pointer";
	public static final String	STACK_POINTER								= "$stack-pointer";
	public static final String	USABLE_MEMORY_START							= "$usable-memory-start";
	public static final String	MAIN_PROGRAM_LABEL							= "$$main";

	public static final String	GENERAL_RUNTIME_ERROR						= "$$general-runtime-error";
	public static final String	INTEGER_DIVIDE_BY_ZERO_RUNTIME_ERROR		= "$$i-divide-by-zero";
	public static final String	FLOAT_DIVIDE_BY_ZERO_RUNTIME_ERROR			= "$$f-divide-by-zero";
	public static final String	ARRAY_INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR		= "$$array-index-out-of-bounds";
	public static final String	ARRAY_LENGTH_LESS_THAN_ZERO_RUNTIME_ERROR	= "$$array-length-less-than-zero";
	public static final String	ARRAY_DEALLOCATED_RUNTIME_ERROR				= "$$array-already-deallocated";

	private ASMCodeFragment environmentASM()
	{
		ASMCodeFragment result = new ASMCodeFragment(GENERATES_VOID);
		setStackPointer(result);
		setFramePointer(result);
		
		result.append(jumpToMain());
		result.append(stringsForPrintf());
		result.append(runtimeErrors());
		setContainertable(result);
		result.add(DLabel, RunTime.ARRAY_POINTER);
		result.add(DataI, 0);
		result.add(DLabel, RunTime.ARRAY_INDEX_POINTER);
		result.add(DataI, 0);
		result.add(DLabel, RunTime.STACK_POINTER);
		result.add(DataI, 0);
		result.add(DLabel, RunTime.FRAME_POINTER);
		result.add(DataI, 0);
		result.add(DLabel, USABLE_MEMORY_START);
		return result;
	}

	private void setContainertable(ASMCodeFragment result)
	{
		for(String name : ContainerType.getContainerNames()){
			result.add(DLabel, "$container-" + name + "-string");
			result.add(DataS, name);
		}
		
		result.add(DLabel, "$container-names-table");
		for(String name : ContainerType.getContainerNames()){
			result.add(DataD, "$container-" + name + "-string");
		}
	}

	private void setStackPointer(ASMCodeFragment result)
	{
		result.add(PushD, STACK_POINTER);				// [ ... sp* ]
		result.add(Memtop);								// [ ... sp* sizeOfMemory ]
		result.add(StoreI);								// [ ... ]
	}

	private void setFramePointer(ASMCodeFragment result)
	{
		result.add(PushD, FRAME_POINTER);				// [ ... fp* ]
		result.add(Memtop);								// [ ... fp* sizeOfMemory ]
		result.add(StoreI);								// [ ... ]
	}

	private ASMCodeFragment jumpToMain()
	{
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(Jump, MAIN_PROGRAM_LABEL);
		return frag;
	}

	private ASMCodeFragment stringsForPrintf()
	{
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(DLabel, EAT_LOCATION_ZERO);
		frag.add(DataZ, 8);
		frag.add(DLabel, INTEGER_PRINT_FORMAT);
		frag.add(DataS, "%d");
		frag.add(DLabel, FLOAT_PRINT_FORMAT);
		frag.add(DataS, "%g");
		frag.add(DLabel, CHAR_PRINT_FORMAT);
		frag.add(DataS, "%c");
		frag.add(DLabel, BOOLEAN_PRINT_FORMAT);
		frag.add(DataS, "%s");
		frag.add(DLabel, NEWLINE_PRINT_FORMAT);
		frag.add(DataS, "\n");
		frag.add(DLabel, BOOLEAN_TRUE_STRING);
		frag.add(DataS, "true");
		frag.add(DLabel, BOOLEAN_FALSE_STRING);
		frag.add(DataS, "false");
		frag.add(DLabel, PRINT_SPACER_STRING);
		frag.add(DataS, " ");
		frag.add(DLabel, PRINT_SPLICE_STRING);
		frag.add(DataS, ",");
		frag.add(DLabel, PRINT_ARRAY_START_STRING);
		frag.add(DataS, "[");
		frag.add(DLabel, PRINT_ARRAY_END_STRING);
		frag.add(DataS, "]");

		return frag;
	}

	private ASMCodeFragment runtimeErrors()
	{
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);

		generalRuntimeError(frag);
		DivideByZeroError(frag);
		ArrayErrors(frag);

		return frag;
	}

	private ASMCodeFragment generalRuntimeError(ASMCodeFragment frag)
	{
		String generalErrorMessage = "$errors-general-message";

		frag.add(DLabel, generalErrorMessage);
		frag.add(DataS, "Runtime error: %s\n");

		frag.add(Label, GENERAL_RUNTIME_ERROR);
		frag.add(PushD, generalErrorMessage);
		frag.add(Printf);
		frag.add(Halt);
		return frag;
	}

	private void DivideByZeroError(ASMCodeFragment frag)
	{
		String intDivideByZeroMessage = "$errors-int-divide-by-zero";

		frag.add(DLabel, intDivideByZeroMessage);
		frag.add(DataS, "integer divide by zero");

		frag.add(Label, INTEGER_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		frag.add(PushD, intDivideByZeroMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);

		String floatDivideByZeroMessage = "$errors-float-divide-by-zero";

		frag.add(DLabel, floatDivideByZeroMessage);
		frag.add(DataS, "float divide by zero");

		frag.add(Label, FLOAT_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		frag.add(PushD, floatDivideByZeroMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}

	private void ArrayErrors(ASMCodeFragment frag)
	{
		String arrayOutOfBoundsMessage = "$errors-array-index-out-of-bounds";

		frag.add(DLabel, arrayOutOfBoundsMessage);
		frag.add(DataS, "array index is out of bounds");

		frag.add(Label, ARRAY_INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		frag.add(PushD, arrayOutOfBoundsMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);

		String arrayLessThanZero = "$errors-array-length-less-than-zero";

		frag.add(DLabel, arrayLessThanZero);
		frag.add(DataS, "array cannot be initalized with length less then zero");

		frag.add(Label, ARRAY_LENGTH_LESS_THAN_ZERO_RUNTIME_ERROR);
		frag.add(PushD, arrayLessThanZero);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);

		String arrayAlreadyDeallocated = "$array-already-deallocated";

		frag.add(DLabel, arrayAlreadyDeallocated);
		frag.add(DataS, "array has already been deallocated");

		frag.add(Label, ARRAY_DEALLOCATED_RUNTIME_ERROR);
		frag.add(PushD, arrayAlreadyDeallocated);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}

	public static ASMCodeFragment getEnvironment()
	{
		RunTime rt = new RunTime();
		return rt.environmentASM();
	}
}
