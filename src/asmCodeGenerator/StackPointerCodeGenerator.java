package asmCodeGenerator;

import static asmCodeGenerator.Macros.loadIFrom;
import static asmCodeGenerator.Macros.storeITo;
import static asmCodeGenerator.codeStorage.ASMOpcode.Add;
import static asmCodeGenerator.codeStorage.ASMOpcode.Exchange;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadC;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadF;
import static asmCodeGenerator.codeStorage.ASMOpcode.LoadI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Nop;
import static asmCodeGenerator.codeStorage.ASMOpcode.PStack;
import static asmCodeGenerator.codeStorage.ASMOpcode.Pop;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreC;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreF;
import static asmCodeGenerator.codeStorage.ASMOpcode.StoreI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Subtract;
import semanticAnalyzer.ArrayType;
import semanticAnalyzer.PrimitiveType;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMOpcode;
import asmCodeGenerator.runtime.RunTime;

public class StackPointerCodeGenerator extends CodeGenerator {

	public static void loadSP(ASMCodeFragment code)
	{
		// [ ... ]
		loadIFrom(code, RunTime.STACK_POINTER);								// [ ... p ]
	}

	public static void decrementAndStoreSP(ASMCodeFragment code, Type type)
	{
		// [ ... ]
		if (type instanceof PrimitiveType) {
			switch ((PrimitiveType) type)
			{
				case BOOLEAN:
					decrementAndStoreSP(code, 1);							// [ ... ]
					break;
				case CHAR:
					decrementAndStoreSP(code, 1);							// [ ... ]
					break;
				case FLOAT:
					decrementAndStoreSP(code, 8);							// [ ... ]
					break;
				case INTEGER:
					decrementAndStoreSP(code, 4);							// [ ... ]
					break;
				case VOID:
					decrementAndStoreSP(code, 0);							// [ ... ]
					break;
				case NO_TYPE:
				case ERROR:
				default:
					assert false : "unknown type for Stack Pointer allocation";
					break;
			}
		}
		else {
			if (type instanceof ArrayType) {
				decrementAndStoreSP(code, 4);									// [ ... ]
			}
		}
	}

	public static void loadAndDecrementSP(ASMCodeFragment code, int value)
	{
		loadSP(code);															// [ ... sp ]
		code.add(PushI, value);													// [ ... sp value ]
		code.add(Subtract);														// [ ... sp-value ]
	}

	public static void decrementAndStoreSP(ASMCodeFragment code, int value)
	{
		loadAndDecrementSP(code, value);
		storeITo(code, RunTime.STACK_POINTER);									// [ ... ]
	}

	public static void incrementAndStoreSP(ASMCodeFragment code, Type type)
	{
		// [ ... ]
		if (type instanceof PrimitiveType) {
			switch ((PrimitiveType) type)
			{
				case BOOLEAN:
					incrementAndStoreSP(code, 1);								// [ ... ]
					break;
				case CHAR:
					incrementAndStoreSP(code, 1);								// [ ... ]
					break;
				case FLOAT:
					incrementAndStoreSP(code, 8);								// [ ... ]
					break;
				case INTEGER:
					incrementAndStoreSP(code, 4);								// [ ... ]
					break;
				case VOID:
					incrementAndStoreSP(code, 0);								// [ ... ]
					break;
				case NO_TYPE:
				case ERROR:
				default:
					assert false : "unknown type for Stack Pointer allocation";
					break;
			}
		}
		else {
			if (type instanceof ArrayType) {
				incrementAndStoreSP(code, 4);									// [ ... ]
			}
		}
	}

	public static void loadAndIncrementSP(ASMCodeFragment code, int value)
	{
		loadSP(code);															// [ ... sp ]
		code.add(PushI, value);													// [ ... sp value ]
		code.add(Add);														// [ ... sp-value ]
	}

	public static void incrementAndStoreSP(ASMCodeFragment code, int value)
	{
		loadAndIncrementSP(code, value);
		storeITo(code, RunTime.STACK_POINTER);									// [ ... ]
	}

	public static void getReturnValue(ASMCodeFragment code, Type type)
	{
		loadSP(code);															// [ ... sp ]
		code.add(opcodeForLoad(type));											// [ ... returnValue ]
		incrementAndStoreSP(code, type);										// [ ... returnValue ]
	}

	public static void storeReturnValue(ASMCodeFragment code, Type type)
	{
		// [ ... returnValue returnAddr ]
		if (type == PrimitiveType.VOID) {
			return;
		}

		code.add(Exchange);														// [ ... returnAddr returnValue ]
		loadSP(code);															// [ ... returnAddr returnValue sp ]
		code.add(Exchange);														// [ ... returnAddr sp returnValue ]
		code.add(opcodeForStore(type));											// [ ... returnAddr ]
	}

	private static ASMOpcode opcodeForLoad(Type type)
	{
		if (type == PrimitiveType.VOID) {
			return Pop;
		}
		if (type == PrimitiveType.INTEGER) {
			return LoadI;
		}
		if (type == PrimitiveType.BOOLEAN) {
			return LoadC;
		}
		if (type == PrimitiveType.FLOAT) {
			return LoadF;
		}
		if (type == PrimitiveType.CHAR) {
			return LoadC;
		}
		if (type instanceof ArrayType) {
			return LoadI;
		}
		assert false : "Type " + type + " unimplemented in opcodeForLoad()";
		return null;
	}

	public static ASMOpcode opcodeForStore(Type type)
	{
		if (type == PrimitiveType.VOID) {
			return Nop;
		}
		if (type == PrimitiveType.INTEGER) {
			return StoreI;
		}
		if (type == PrimitiveType.BOOLEAN) {
			return StoreC;
		}
		if (type == PrimitiveType.FLOAT) {
			return StoreF;
		}
		if (type == PrimitiveType.CHAR) {
			return StoreC;
		}
		if (type instanceof ArrayType) {
			return StoreI;
		}
		assert false : "Type " + type + " unimplemented in opcodeForStore()";
		return null;
	}
	
	public static void printSP(ASMCodeFragment code){
		loadSP(code);
		code.add(PStack);
		code.add(Pop);
	}
}