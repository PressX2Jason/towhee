package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.BTAnd;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static semanticAnalyzer.PrimitiveType.CHAR;
import static semanticAnalyzer.PrimitiveType.INTEGER;

import java.util.Arrays;

import parseTree.nodeTypes.OperatorNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class CastCodeGenerator extends CodeGenerator {
	public CastCodeGenerator()
	{
	}

	public ASMCodeFragment generateCode(OperatorNode node)
	{
		assert node.nChildren() == 2;

		Type arg1Type = node.child(0).getType();
		Type arg2Type = node.child(1).getType();

		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);
		// INT -> CHAR
		if (arg1Type == INTEGER && arg2Type == CHAR) {
			code.add(PushI, 0x7F);
			code.add(BTAnd);
		}
		else {
			typeCheckError(node, Arrays.asList(arg1Type, arg2Type));
		}
		return code;
	}

}
