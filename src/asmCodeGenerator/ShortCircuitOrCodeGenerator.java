package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpTrue;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static semanticAnalyzer.PrimitiveType.BOOLEAN;

import java.util.Arrays;

import parseTree.nodeTypes.OperatorNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class ShortCircuitOrCodeGenerator extends CodeGenerator{

	private static String	DESCRIPTION	= "-short-circuit-or-";

	public ShortCircuitOrCodeGenerator()
	{
	}

	public ASMCodeFragment generateCode(OperatorNode node, ASMCodeFragment arg1, ASMCodeFragment arg2)
	{
		assert node.nChildren() == 2;
		assert arg1 != null && arg2 != null;

		Type arg1Type = node.child(0).getType();
		Type arg2Type = node.child(1).getType();

		Labeller labeller = getLabeller();
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);

		String startLabel = labeller.newLabel("-compare-arg1-", DESCRIPTION);
		String arg2Label = labeller.newLabelSameNumber("-compare-arg2-", DESCRIPTION);
		String trueLabel = labeller.newLabelSameNumber("-compare-true-", DESCRIPTION);
		String joinLabel = labeller.newLabelSameNumber("-compare-join-", DESCRIPTION);

		/*if(a)
		 *	true;
		 *else
		 *	b;
		 */
		if (arg1Type == BOOLEAN && arg2Type == BOOLEAN){
			code.add(Label, startLabel);
			code.append(arg1);
			code.add(JumpTrue, trueLabel);			
			code.add(Label, arg2Label);
			code.append(arg2);
			code.add(Jump, joinLabel);
			code.add(Label, trueLabel);
			code.add(PushI, 1);
			code.add(Label, joinLabel);
		}else {
			typeCheckError(node, Arrays.asList(arg1Type, arg2Type));
		}
		return code;
	}
}
