package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.GENERATES_VALUE;
import static asmCodeGenerator.codeStorage.ASMOpcode.FSubtract;
import static asmCodeGenerator.codeStorage.ASMOpcode.Jump;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpFPos;
import static asmCodeGenerator.codeStorage.ASMOpcode.JumpPos;
import static asmCodeGenerator.codeStorage.ASMOpcode.Label;
import static asmCodeGenerator.codeStorage.ASMOpcode.PushI;
import static asmCodeGenerator.codeStorage.ASMOpcode.Subtract;
import static semanticAnalyzer.PrimitiveType.CHAR;
import static semanticAnalyzer.PrimitiveType.FLOAT;
import static semanticAnalyzer.PrimitiveType.INTEGER;

import java.util.Arrays;

import parseTree.nodeTypes.OperatorNode;
import semanticAnalyzer.Type;
import asmCodeGenerator.codeStorage.ASMCodeFragment;

public class LessThanOrEqualCodeGenerator extends CodeGenerator {

	private static String	DESCRIPTION	= "-less-than-or-equal-";

	public LessThanOrEqualCodeGenerator()
	{
	}

	public ASMCodeFragment generateCode(OperatorNode node, ASMCodeFragment arg1, ASMCodeFragment arg2)
	{
		assert node.nChildren() == 2;
		assert arg1 != null && arg2 != null;

		Type arg1Type = node.child(0).getType();
		Type arg2Type = node.child(1).getType();

		Labeller labeller = getLabeller();
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VALUE);

		String startLabel = labeller.newLabel("-compare-arg1-", DESCRIPTION);
		String arg2Label = labeller.newLabelSameNumber("-compare-arg2-", DESCRIPTION);
		String subLabel = labeller.newLabelSameNumber("-compare-sub-", DESCRIPTION);
		String trueLabel = labeller.newLabelSameNumber("-compare-true-", DESCRIPTION);
		String falseLabel = labeller.newLabelSameNumber("-compare-false-", DESCRIPTION);
		String joinLabel = labeller.newLabelSameNumber("-compare-join-", DESCRIPTION);

		code.add(Label, startLabel);
		code.append(arg1);
		code.add(Label, arg2Label);
		code.append(arg2);
		code.add(Label, subLabel);

		// A < B -> A - B < 0; jump to True Label if result is less then zero. jump to False label
		if ((arg1Type == INTEGER && arg2Type == INTEGER) || (arg1Type == CHAR && arg2Type == CHAR)) {
			code.add(Subtract);
			code.add(JumpPos, falseLabel);
			code.add(Jump, trueLabel);
		}
		else if (arg1Type == FLOAT && arg2Type == FLOAT) {
			code.add(FSubtract);
			code.add(JumpFPos, falseLabel);
			code.add(Jump, trueLabel);
		}
		else {
			typeCheckError(node, Arrays.asList(arg1Type, arg2Type));
		}

		code.add(Label, trueLabel);
		code.add(PushI, 1);
		code.add(Jump, joinLabel);
		code.add(Label, falseLabel);
		code.add(PushI, 0);
		code.add(Jump, joinLabel);
		code.add(Label, joinLabel);

		return code;
	}
}