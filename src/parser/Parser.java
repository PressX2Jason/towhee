package parser;

import java.util.Arrays;

import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import lexicalAnalyzer.Scanner;
import logging.TowheeLogger;
import parseTree.ParseNode;
import parseTree.nodeTypes.ArrayCreationNode;
import parseTree.nodeTypes.ArrayIndexNode;
import parseTree.nodeTypes.ArrayTypeNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BodyNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallStatementNode;
import parseTree.nodeTypes.CharacterConstantNode;
import parseTree.nodeTypes.ContainerCreationNode;
import parseTree.nodeTypes.ContainerDeclarationNode;
import parseTree.nodeTypes.ContainerFunctionAccessNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.FloatConstantNode;
import parseTree.nodeTypes.FunctionDeclarationNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IfStatementNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.MemberAccessNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReleaseStatementNode;
import parseTree.nodeTypes.ReturnStatementNode;
import parseTree.nodeTypes.TypeConstantNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.UpdateNode;
import parseTree.nodeTypes.ValueBodyNode;
import parseTree.nodeTypes.WhileStatementNode;
import semanticAnalyzer.PrimitiveType;
import tokens.CharacterToken;
import tokens.FloatToken;
import tokens.IdentifierToken;
import tokens.IntegerToken;
import tokens.NullToken;
import tokens.Token;

public class Parser {
	private Scanner	scanner;
	private Token	nowReading;
	private Token	previouslyRead;

	public static ParseNode parse(Scanner scanner)
	{
		Parser parser = new Parser(scanner);
		return parser.parse();
	}

	public Parser(Scanner scanner)
	{
		super();
		this.scanner = scanner;
	}

	public ParseNode parse()
	{
		readToken();
		return parseProgram();
	}

	// //////////////////////////////////////////////////////////
	// "program" is the start symbol S
	// S -> containerDeclaration+

	private ParseNode parseProgram()
	{
		if (!startsProgram(nowReading)) {
			return syntaxErrorNode("program");
		}
		ParseNode program = new ProgramNode(nowReading);

		if (!startsContainer(nowReading)) {
			return syntaxErrorNode("container");
		}

		while (startsContainer(nowReading)) {
			ParseNode container = parseContainer();
			program.appendChild(container);
		}

		if (!(nowReading instanceof NullToken)) {
			return syntaxErrorNode("end of program");
		}

		return program;
	}

	private boolean startsProgram(Token token)
	{
		return startsContainer(token);
	}

	// /////////////////////////////////////////////////////////
	// containers

	// container -> CONTAINER identifier ( (paramList) )? Body
	private ParseNode parseContainer()
	{
		if (!startsContainer(nowReading)) {
			return syntaxErrorNode("container");
		}

		Token containerToken = nowReading;
		readToken();
		ContainerDeclarationNode container = new ContainerDeclarationNode(containerToken);

		container.appendChild(parseIdentifier());

		if (startsParameterList(nowReading)) {
			parseParameterList(container);
			expect(Punctuator.CLOSE_PARENTHESE);
		}

		container.appendChild(parseBody());

		return container;
	}

	private boolean startsContainer(Token token)
	{
		return token.isLextant(Keyword.CONTAINER);
	}

	// body -> { statement* }
	private ParseNode parseBody()
	{
		if (!startsbody(nowReading)) {
			return syntaxErrorNode("body");
		}
		ParseNode container = new BodyNode(nowReading);
		expect(Punctuator.OPEN_BRACE);

		while (startsStatement(nowReading)) {
			ParseNode statement = parseStatement();
			container.appendChild(statement);
		}
		expect(Punctuator.CLOSE_BRACE);
		return container;
	}

	private boolean startsbody(Token token)
	{
		return token.isLextant(Punctuator.OPEN_BRACE);
	}

	// /////////////////////////////////////////////////////////
	// statements

	// statement-> declaration | updateStmt | releaseStmt | printStmt | ifStmt | whileStmt | ParseNodeDeclaration | returnStmt | callStmt | forStmt
	private ParseNode parseStatement()
	{
		if (!startsStatement(nowReading)) {
			return syntaxErrorNode("statement");
		}
		if (startsDeclaration(nowReading)) {
			return parseDeclaration();
		}
		if (startsUpdateStatement(nowReading)) {
			return parseUpdateStatement();
		}
		if (startsPrintStatement(nowReading)) {
			return parsePrintStatement();
		}
		if (startsReleaseStatement(nowReading)) {
			return parseReleaseStatement();
		}
		if (startsIfStatement(nowReading)) {
			return parseIfStatement();
		}
		if (startsWhileStatement(nowReading)) {
			return parseWhileStatement();
		}
		if (startsParseNodeDeclaration(nowReading)) {
			return parseParseNodeDeclaration();
		}
		if (startsReturnStatement(nowReading)) {
			return parseReturnStatment();
		}
		if (startsCallStatement(nowReading)) {
			return parseCallStatement();
		}
		assert false : "bad token " + nowReading + " in parseStatement()";
		return null;
	}

	private boolean startsStatement(Token token)
	{
		return startsDeclaration(token) || startsUpdateStatement(token) || startsPrintStatement(token) || startsReleaseStatement(token)
				|| startsIfStatement(token) || startsWhileStatement(token) || startsReturnStatement(token) || startsParseNodeDeclaration(token)
				|| startsCallStatement(token);
	}

	// CALL ParseNodeInvocation ParseNode invoked must be of type void
	private ParseNode parseCallStatement()
	{
		if (!startsCallStatement(nowReading)) {
			return syntaxErrorNode("call statement");
		}

		Token callStmtToken = nowReading;
		readToken();

		CallStatementNode callStmtNode = new CallStatementNode(callStmtToken);
		callStmtNode = (CallStatementNode) parseParseNodeInvocation(callStmtNode);
		return callStmtNode;
	}
	
	// ParseNodeInvocation -> identifier ( expressionList ) ;
	private ParseNode parseParseNodeInvocation(ParseNode parent)
	{
		if (!startsParseNodeInvocation(nowReading)) {
			parent.appendChild(syntaxErrorNode("ParseNode Declaration"));
		}

		ParseNode ParseNodeName = parseIdentifier();
		parent.appendChild(ParseNodeName);
		expect(Punctuator.OPEN_PARENTHESE);
		parseExpressionList(parent);
		expect(Punctuator.CLOSE_PARENTHESE);
		expect(Punctuator.TERMINATOR);
		return parent;
	}

	private boolean startsCallStatement(Token token)
	{
		return token.isLextant(Keyword.CALL);
	}

	private boolean startsParseNodeInvocation(Token token)
	{
		return startsIdentifier(token);
	}

	// DEF identifier (parameterList): type valueBody
	private ParseNode parseParseNodeDeclaration()
	{
		if (!startsParseNodeDeclaration(nowReading)) {
			return syntaxErrorNode("ParseNode Declaration");
		}

		Token funcDecToken = nowReading;
		readToken();
		FunctionDeclarationNode funcDecNode = new FunctionDeclarationNode(funcDecToken);

		funcDecNode.appendChild(parseIdentifier());

		funcDecNode = (FunctionDeclarationNode) parseParameterList(funcDecNode);
		expect(Punctuator.CLOSE_PARENTHESE);

		expect(Punctuator.COLON);
		funcDecNode.appendChild(parseType());
		funcDecNode.appendChild(parseExpression9());

		return funcDecNode;

	}

	// parameterList -> (parameterSpecification (, parameterSpecification)*)*
	private ParseNode parseParameterList(ParseNode parent)
	{
		if (!startsParameterList(nowReading)) {
			parent.appendChild(syntaxErrorNode("Parameter List"));
			return parent;
		}

		Token paramListToken = nowReading;
		readToken();
		ParameterListNode paramListNode = new ParameterListNode(paramListToken);

		if (startsIdentifier(nowReading)) {
			parent.appendChild(parseParameterSpecification(paramListNode));

			while (nowReading.isLextant(Punctuator.SPLICE)) {
				readToken();
				parseParameterSpecification(paramListNode);
			}
		}
		else {
			parent.appendChild(paramListNode);
		}
		return parent;
	}

	private boolean startsParameterList(Token token)
	{
		return token.isLextant(Punctuator.OPEN_PARENTHESE);
	}

	// parameterSpecification -> identifier : type
	private ParseNode parseParameterSpecification(ParseNode parent)
	{
		ParseNode parameterSpecificationNode = null;
		if (!startsIdentifier(nowReading)) {
			parameterSpecificationNode = syntaxErrorNode("ParseNode identifier");
		}
		else {
			parameterSpecificationNode = parseIdentifier();
		}

		expect(Punctuator.COLON);

		if (!startsType(nowReading)) {
			parameterSpecificationNode.appendChild(syntaxErrorNode("ParseNode type"));
		}
		else {
			parameterSpecificationNode.appendChild(parseType());
		}

		parent.appendChild(parameterSpecificationNode);

		return parent;
	}

	private ParseNode parseType()
	{
		if (!startsType(nowReading)) {
			return (syntaxErrorNode("param type"));
		}

		if (startsTypeConstant(nowReading)) {
			return parseTypeConstant();
		}
		else {
			return parseCreationType();
		}
	}

	private boolean startsType(Token token)
	{
		return token.isLextant(Punctuator.OPEN_SQUARE_BRACKET) || startsTypeConstant(token);
	}

	private boolean startsParseNodeDeclaration(Token token)
	{
		return token.isLextant(Keyword.DEFINE);
	}

	// RETURN expr?
	private ParseNode parseReturnStatment()
	{
		if (!startsReturnStatement(nowReading)) {
			return syntaxErrorNode("return statement");
		}
		ReturnStatementNode result = new ReturnStatementNode(nowReading);

		readToken();
		result = (ReturnStatementNode) parseExpressionList(result);

		expect(Punctuator.TERMINATOR);
		return result;
	}

	private boolean startsReturnStatement(Token token)
	{
		return token.isLextant(Keyword.RETURN);
	}

	// RELEASE exprList ;
	private ParseNode parseReleaseStatement()
	{
		if (!startsReleaseStatement(nowReading)) {
			return syntaxErrorNode("release statement");
		}
		ReleaseStatementNode result = new ReleaseStatementNode(nowReading);

		readToken();
		result = (ReleaseStatementNode) parseExpressionList(result);

		expect(Punctuator.TERMINATOR);
		return result;
	}

	private boolean startsReleaseStatement(Token token)
	{
		return token.isLextant(Keyword.RELEASE);
	}

	// WHILE (condition) body
	private ParseNode parseWhileStatement()
	{
		if (!startsWhileStatement(nowReading)) {
			return syntaxErrorNode("if statement");
		}
		Token whileToken = nowReading;
		readToken();
		expect(Punctuator.OPEN_PARENTHESE);
		ParseNode condition = parseExpression();
		expect(Punctuator.CLOSE_PARENTHESE);
		ParseNode whileBody = parseBody();
		return WhileStatementNode.withChildren(whileToken, condition, whileBody);
	}

	private boolean startsWhileStatement(Token token)
	{
		return token.isLextant(Keyword.WHILE);
	}

	// ifStmt -> IF (condition) body (ELSE body )?
	private ParseNode parseIfStatement()
	{
		if (!startsIfStatement(nowReading)) {
			return syntaxErrorNode("if statement");
		}
		Token ifToken = nowReading;
		readToken();
		expect(Punctuator.OPEN_PARENTHESE);
		ParseNode condition = parseExpression();
		expect(Punctuator.CLOSE_PARENTHESE);
		ParseNode ifBody = parseBody();
		ParseNode elseBody = null;
		if (startsElseStatement(nowReading)) {
			readToken();
			elseBody = parseBody();
		}
		return IfStatementNode.withChildren(ifToken, condition, ifBody, elseBody);
	}

	private boolean startsIfStatement(Token token)
	{
		return token.isLextant(Keyword.IF);
	}

	private boolean startsElseStatement(Token token)
	{
		return token.isLextant(Keyword.ELSE);
	}

	// updateStmt -> UPDATE targetIdentifier <- expression
	private ParseNode parseUpdateStatement()
	{
		if (!startsUpdateStatement(nowReading)) {
			return syntaxErrorNode("update statement");
		}
		Token updateToken = nowReading;
		readToken();
		ParseNode updatableExpression = parseTargetableExpression();

		expect(Punctuator.ASSIGN);
		ParseNode updatedValue = parseExpression();
		expect(Punctuator.TERMINATOR);
		return UpdateNode.withChildren(updateToken, updatableExpression, updatedValue);
	}

	private boolean startsTargetableStatement(Token token)
	{
		return startsIdentifier(token) || nowReading.isLextant(Punctuator.OPEN_SQUARE_BRACKET) || nowReading.isLextant(Punctuator.OPEN_PARENTHESE);
	}

	// identifier | arrayindex
	private ParseNode parseTargetableExpression()
	{
		if (!startsTargetableStatement(nowReading)) {
			return syntaxErrorNode("targetable expression");
		}
		ParseNode left = parseExpression9();
		while (nowReading.isLextant(Punctuator.OPEN_SQUARE_BRACKET)) {
			Token indexAccessToken = nowReading;
			readToken();
			ParseNode right = parseExpression();
			expect(Punctuator.CLOSE_SQUARE_BRACKET);
			left = ArrayIndexNode.withChildren(indexAccessToken, left, right);
		}
		return left;
	}

	private boolean startsUpdateStatement(Token token)
	{
		return token.isLextant(Keyword.UPDATE);
	}

	// printStmt -> (PRINT | PRINTLN | SPLAT | SPLATLN) expressionList ;
	private ParseNode parsePrintStatement()
	{
		if (!startsPrintStatement(nowReading)) {
			return syntaxErrorNode("print statement");
		}
		PrintStatementNode result = new PrintStatementNode(nowReading);

		readToken();
		result = (PrintStatementNode) parseExpressionList(result);

		expect(Punctuator.TERMINATOR);
		return result;
	}

	private boolean startsPrintStatement(Token token)
	{
		return token.isLextant(Keyword.PRINT, Keyword.PRINTLN, Keyword.SPLAT, Keyword.SPLATLN);
	}

	// This adds the expressions found to the children of the given parent
	// expressionList -> (expr (, expr)*)?
	private ParseNode parseExpressionList(ParseNode parent)
	{
		if (!startsExpressionList(nowReading)) {
			parent.appendChild(syntaxErrorNode("expressionList"));
			return parent;
		}

		if (startsExpression(nowReading)) {
			ParseNode firstExpression = parseExpression();
			parent.appendChild(firstExpression);

			while (nowReading.isLextant(Punctuator.SPLICE)) {
				readToken();
				ParseNode expression = parseExpression();
				parent.appendChild(expression);
			}
		}
		return parent;
	}

	private boolean startsExpressionList(Token token)
	{
		return true;
	}

	// declaration -> IMMUTABLE | MUTABLE identifier <- expression ;
	private ParseNode parseDeclaration()
	{
		if (!startsDeclaration(nowReading)) {
			return syntaxErrorNode("declaration");
		}
		Token declarationToken = nowReading;
		readToken();
		ParseNode identifier = parseIdentifier();

		expect(Punctuator.ASSIGN);
		ParseNode initializer = parseExpression();
		expect(Punctuator.TERMINATOR);

		return DeclarationNode.withChildren(declarationToken, identifier, initializer);
	}

	private boolean startsDeclaration(Token token)
	{
		return token.isLextant(Keyword.IMMUTABLE, Keyword.MUTABLE);
	}

	// /////////////////////////////////////////////////////////
	// expressions
	// expr -> expr1

	// expr1 -> expr2 [|| expr2]? (left-assoc)
	// expr2 -> expr3 [&& expr3]? (left-assoc)
	// expr3 -> expr4 [> expr4]?[=> expr4]?[< expr4]?[<= expr4]?[== expr4]?[!= expr4][ (left-assoc)
	// expr4 -> expr5 [+ expr5]*[- expr5] (left-assoc)
	// expr5 -> expr6 [MULT expr6]*[/ expr6]* (left-assoc)
	// expr6 -> : expr6 | expr7
	// expr7 -> ! expr7 | expr8 (right-associative)
	// expr8 -> . []
	// expr9 -> () [] @[]()
	// expr10 -> terminal
	// terminal -> literal | (expr)
	// literal -> intConstant | identifier | booleanConstant | floatConstant

	// expr -> expr1
	private ParseNode parseExpression()
	{
		if (!startsExpression(nowReading)) {
			return syntaxErrorNode("expression");
		}
		return parseExpression1();
	}

	private boolean startsExpression(Token token)
	{
		return startsExpression1(token);
	}

	// expr1 -> expr2 [|| expr2]? (left-assoc)
	private ParseNode parseExpression1()
	{
		if (!startsExpression1(nowReading)) {
			return syntaxErrorNode("expression<2>");
		}
		ParseNode left = parseExpression2();
		while (nowReading.isLextant(Punctuator.SHORT_CIRCUIT_OR)) {
			Token compareToken = nowReading;
			readToken();
			ParseNode right = parseExpression2();

			left = BinaryOperatorNode.withChildren(compareToken, left, right);
		}
		return left;
	}

	private boolean startsExpression1(Token token)
	{
		return startsExpression2(token);
	}

	// expr2 -> expr3 [&& expr3]? (left-assoc)
	private ParseNode parseExpression2()
	{
		if (!startsExpression2(nowReading)) {
			return syntaxErrorNode("expression<1>");
		}
		ParseNode left = parseExpression3();
		while (nowReading.isLextant(Punctuator.SHORT_CIRCUIT_AND)) {
			Token compareToken = nowReading;
			readToken();
			ParseNode right = parseExpression3();

			left = BinaryOperatorNode.withChildren(compareToken, left, right);
		}
		return left;
	}

	private boolean startsExpression2(Token token)
	{
		return startsExpression3(token);
	}

	// expr3 -> expr4 [> expr4]?[=> expr4]?[< expr4]?[<= expr4]?[== expr4]?[!= expr4] (left-assoc)
	private ParseNode parseExpression3()
	{
		if (!startsExpression3(nowReading)) {
			return syntaxErrorNode("expression<3>");
		}
		ParseNode left = parseExpression4();
		while (Punctuator.isComparisonOperator(nowReading)) {
			Token compareToken = nowReading;
			ParseNode right = null;
			if (nowReading.isLextant(Punctuator.GREATER_THAN)) {
				readToken();
				right = parseExpression4();
			}
			else if (nowReading.isLextant(Punctuator.GREATER_THAN_OR_EQUAL)) {
				readToken();
				right = parseExpression4();
			}
			else if (nowReading.isLextant(Punctuator.LESS_THAN)) {
				readToken();
				right = parseExpression4();
			}
			else if (nowReading.isLextant(Punctuator.LESS_THAN_OR_EQUAL)) {
				readToken();
				right = parseExpression4();
			}
			else if (nowReading.isLextant(Punctuator.EQUAL)) {
				readToken();
				right = parseExpression4();
			}
			else if (nowReading.isLextant(Punctuator.NOT_EQUAL)) {
				readToken();
				right = parseExpression4();
			}
			else {
				return syntaxErrorNode("expression<3>");
			}
			left = BinaryOperatorNode.withChildren(compareToken, left, right);
		}
		return left;
	}

	private boolean startsExpression3(Token token)
	{
		return startsExpression4(token);
	}

	// expr4 -> expr5 [+ expr5]*[- expr5] (left-assoc)
	private ParseNode parseExpression4()
	{
		if (!startsExpression4(nowReading)) {
			return syntaxErrorNode("expression<4>");
		}

		ParseNode left = parseExpression5();
		while (Punctuator.isAdditiveOperater(nowReading)) {
			Token additiveToken = nowReading;
			ParseNode right = null;
			if (nowReading.isLextant(Punctuator.ADD)) {
				readToken();
				right = parseExpression5();
			}
			else if (nowReading.isLextant(Punctuator.SUBTRACT)) {
				readToken();
				right = parseExpression5();
			}
			else {
				return syntaxErrorNode("expression<7>");
			}
			left = BinaryOperatorNode.withChildren(additiveToken, left, right);
		}
		return left;
	}

	private boolean startsExpression4(Token token)
	{
		return startsExpression5(token);
	}

	// expr5 -> expr6 [MULT expr6]*[/ expr6]* (left-assoc)
	private ParseNode parseExpression5()
	{
		if (!startsExpression5(nowReading)) {
			return syntaxErrorNode("expression<5>");
		}

		ParseNode left = parseExpression6();
		while (nowReading.isLextant(Punctuator.MULTIPLY) || nowReading.isLextant(Punctuator.DIVIDE)) {
			Token Operatortoken = nowReading;
			ParseNode right = null;
			if (nowReading.isLextant(Punctuator.MULTIPLY)) {
				readToken();
				right = parseExpression6();
			}
			else if (nowReading.isLextant(Punctuator.DIVIDE)) {
				readToken();
				right = parseExpression6();
			}
			else {
				return syntaxErrorNode("expression<5>");
			}
			left = BinaryOperatorNode.withChildren(Operatortoken, left, right);
		}
		return left;
	}

	private boolean startsExpression5(Token token)
	{
		return startsExpression6(token);
	}

	// expr6 -> expr7 : type (right-associative)
	private ParseNode parseExpression6()
	{
		if (!startsExpression6(nowReading)) {
			return syntaxErrorNode("Expression<6>");
		}
		ParseNode left = parseExpression7();
		while (nowReading.isLextant(Punctuator.COLON)) {
			Token castToken = nowReading;
			readToken();
			ParseNode type = parseExpression7();
			left = BinaryOperatorNode.withChildren(castToken, left, type);
		}
		return left;
	}

	private boolean startsExpression6(Token token)
	{
		return startsExpression7(token);
	}

	// expr7 -> ! expr7 | expr8 (right-associative)
	private ParseNode parseExpression7()
	{
		if (!startsExpression7(nowReading)) {
			return syntaxErrorNode("Expression<7>");
		}
		if (nowReading.isLextant(Punctuator.NOT)) {
			Token notToken = nowReading;
			readToken();
			ParseNode right = parseExpression7();
			return UnaryOperatorNode.withChild(notToken, right, PrimitiveType.BOOLEAN);
		}
		else {
			return parseExpression8();
		}
	}

	private boolean startsExpression7(Token token)
	{
		return startsExpression8(token);
	}

	// expr8 -> expr.identifier | expr[expr]
	private ParseNode parseExpression8()
	{
		if (!startsExpression8(nowReading)) {
			return syntaxErrorNode("Expression<8>");
		}
		ParseNode left = parseExpression9();
		if (nowReading.isLextant(Punctuator.MEMBER_ACCESS)) {
			Token memberAccessToken = nowReading;
			readToken();
			ParseNode identifier = parseIdentifier();
			left = MemberAccessNode.withChildren(memberAccessToken, left, identifier);
		}
		while (nowReading.isLextant(Punctuator.OPEN_SQUARE_BRACKET)) {
			Token indexAccessToken = nowReading;
			readToken();
			ParseNode right = parseExpression();
			expect(Punctuator.CLOSE_SQUARE_BRACKET);
			left = ArrayIndexNode.withChildren(indexAccessToken, left, right);
		}
		return left;
	}

	private boolean startsExpression8(Token token)
	{
		return startsExpression9(nowReading);
	}

	// expr9 -> [expr9] PERSIST? | [exprList] PERSIST? | @[ type ]( (expr ,)? expr)) PERSIST? | {[ statement* ]} | | (container.)?expr10 ( exprList )
	private ParseNode parseExpression9()
	{
		if (!startsExpression9(nowReading)) {
			return syntaxErrorNode("Expression<9>");
		}
		if (startsPopulatedArrayCreationExpression(nowReading)) {
			Token arrayToken = nowReading;
			readToken();
			ArrayCreationNode creationNode = new ArrayCreationNode(arrayToken);
			parseExpressionList(creationNode);
			expect(Punctuator.CLOSE_SQUARE_BRACKET);

			if (nowReading.isLextant(Keyword.PERSIST)) {
				Token persistToken = nowReading;
				readToken();
				creationNode.setPersistToken(persistToken);
			}
			return creationNode;
		}
		else if (startsCreationExpression(nowReading)) {
			Token creationToken = nowReading;
			readToken();

			ParseNode typeNode = parseCreationType();
			if (typeNode instanceof IdentifierNode) {
				ContainerCreationNode creationNode = new ContainerCreationNode(creationToken);
				creationNode.appendChild(typeNode);
				if (nowReading.isLextant(Punctuator.OPEN_PARENTHESE)) {
					expect(Punctuator.OPEN_PARENTHESE);
					parseExpressionList((ParseNode) creationNode);
					expect(Punctuator.CLOSE_PARENTHESE);
				}
				if (nowReading.isLextant(Keyword.PERSIST)) {
					Token persistToken = nowReading;
					readToken();
					creationNode.setPersistToken(persistToken);
				}
				return creationNode;
			}
			else {
				expect(Punctuator.OPEN_PARENTHESE);
				ParseNode firstExpr = parseExpression();				// (expr)
				ParseNode secondExpr = null;
				if (nowReading.isLextant(Punctuator.SPLICE)) {			// (expr , expr)
					readToken();
					secondExpr = parseExpression();
				}
				expect(Punctuator.CLOSE_PARENTHESE);
				Token persistToken = null;

				if (nowReading.isLextant(Keyword.PERSIST)) {
					persistToken = nowReading;
					readToken();
					expect(Punctuator.CLOSE_VALUE_BODY);
				}
				return ArrayCreationNode.createEmptyArray(creationToken, persistToken, typeNode, firstExpr, secondExpr);
			}
		}
		else if (startsValueBody(nowReading)) {
			Token valueBodyToken = nowReading;
			readToken();

			ValueBodyNode result = new ValueBodyNode(valueBodyToken);
			while (startsStatement(nowReading)) {
				ParseNode statement = parseStatement();
				result.appendChild(statement);
			}
			expect(Punctuator.CLOSE_VALUE_BODY);
			return result;
		}
		//identifier
		else if (startsParseNodeInvocation(nowReading)) {

			ParseNode identifierNode = parseExpression10();
			if (nowReading.isLextant(Punctuator.OPEN_PARENTHESE)) {
				FunctionInvocationNode functionNodeInvocationNode = new FunctionInvocationNode(nowReading);

				functionNodeInvocationNode.appendChild(identifierNode);
				expect(Punctuator.OPEN_PARENTHESE);
				parseExpressionList(functionNodeInvocationNode);
				expect(Punctuator.CLOSE_PARENTHESE);

				return functionNodeInvocationNode;
			}
			if(nowReading.isLextant(Punctuator.MEMBER_ACCESS)){
				ContainerFunctionAccessNode cfaNode = new ContainerFunctionAccessNode(nowReading);
				cfaNode.appendChild(identifierNode);
				readToken();
				
				ParseNode functionName = parseIdentifier();
				cfaNode.appendChild(functionName);
				expect(Punctuator.OPEN_PARENTHESE);
				parseExpressionList(cfaNode);
				expect(Punctuator.CLOSE_PARENTHESE);
				return cfaNode;
			}
			return identifierNode;
		}
		else {
			return parseExpression10();
		}
	}

	private boolean startsValueBody(Token token){
		return token.isLextant(Punctuator.OPEN_VALUE_BODY);
	}

	// creationType -> [ type ] | identifier
	private ParseNode parseCreationType()
	{
		if (!startsArrayType(nowReading) && !startsIdentifier(nowReading)) {
			return syntaxErrorNode("parse creationNode type");
		}

		if (startsArrayType(nowReading)) {
			Token arrayTypeNodeToken = nowReading;
			readToken();

			ArrayTypeNode arrayType = new ArrayTypeNode(arrayTypeNodeToken);
			arrayType.appendChild(parseTypeConstant());								// [type]
			expect(Punctuator.CLOSE_SQUARE_BRACKET);
			return arrayType;
		}
		return parseIdentifier();
	}

	private boolean startsArrayType(Token token)
	{
		return token.isLextant(Punctuator.OPEN_SQUARE_BRACKET);
	}

	private boolean startsExpression9(Token token)
	{
		return startsExpression10(nowReading);
	}

	// expr10 -> terminal
	private ParseNode parseExpression10()
	{
		if (!startsExpression10(nowReading)) {
			return syntaxErrorNode("Expression<10>");
		}
		return parseTerminal();
	}

	private boolean startsExpression10(Token token)
	{
		return isTerminal(nowReading);
	}

	// terminal -> literal | (expr) | type | arrayExpression
	private ParseNode parseTerminal()
	{
		if (!isTerminal(nowReading)) {
			return syntaxErrorNode("terminal");
		}
		if (startsLiteral(nowReading)) {
			return parseLiteral();
		}
		else if (startsParenthesizedExpression(nowReading)) {
			readToken();
			ParseNode result = parseExpression();
			expect(Punctuator.CLOSE_PARENTHESE);
			return result;
		}
		else if (startsTypeConstant(nowReading)) {
			return parseTypeConstant();
		}
		return syntaxErrorNode("terminal");
	}

	// literal | (expr) | const | type | arrayExpression | {[ ]}
	private boolean isTerminal(Token token)
	{
		return startsLiteral(token) || startsParenthesizedExpression(token) || startsTypeConstant(token) || startsNotExpression(token)
				|| startsPopulatedArrayCreationExpression(token) || startsCreationExpression(token) || startsValueBody(token);
	}

	private boolean startsCreationExpression(Token token)
	{
		return token.isLextant(Punctuator.AT_SIGN);
	}

	private boolean startsNotExpression(Token token)
	{
		return token.isLextant(Punctuator.NOT);
	}

	private boolean startsPopulatedArrayCreationExpression(Token token)
	{
		return token.isLextant(Punctuator.OPEN_SQUARE_BRACKET);
	}

	private boolean startsParenthesizedExpression(Token token)
	{
		return token.isLextant(Punctuator.OPEN_PARENTHESE);
	}

	// literal -> intConstant | identifier | booleanConstant | floatConstant | charConstant
	private ParseNode parseLiteral()
	{
		if (!startsLiteral(nowReading)) {
			return syntaxErrorNode("literal");
		}
		if (startsIntegerConstant(nowReading)) {
			return parseIntegerConstant();
		}
		if (startsIdentifier(nowReading)) {
			return parseIdentifier();
		}
		if (startsBooleanConstant(nowReading)) {
			return parseBooleanConstant();
		}
		if (startsFloatConstant(nowReading)) {
			return parseFloatConstant();
		}
		if (startsCharacterConstant(nowReading)) {
			return parseCharacterConstant();
		}
		assert false : "bad token " + nowReading + " in parseLiteral()";
		return null;
	}

	private boolean startsLiteral(Token token)
	{
		return startsIntegerConstant(token) || startsIdentifier(token) || startsBooleanConstant(token) || startsFloatConstant(token)
				|| startsCharacterConstant(token);
	}

	// type (terminal)
	private ParseNode parseTypeConstant()
	{
		if (!startsTypeConstant(nowReading)) {
			return syntaxErrorNode("type constant");
		}
		if (startsPrimitiveType(nowReading)) {
			readToken();
			return new TypeConstantNode(previouslyRead);
		}
		else {
			return parseCreationType();
		}
	}

	// bool | char | int | float | arrayType
	private boolean startsTypeConstant(Token token)
	{
		return startsPrimitiveType(token) || startsArrayType(token);
	}

	// bool | char | int | float
	private boolean startsPrimitiveType(Token token)
	{
		return token.isLextant(Keyword.BOOLEAN, Keyword.CHAR, Keyword.INTEGER, Keyword.FLOAT, Keyword.VOID);
	}

	// char (terminal)
	private ParseNode parseCharacterConstant()
	{
		if (!startsCharacterConstant(nowReading)) {
			return syntaxErrorNode("char constant");
		}
		readToken();
		return new CharacterConstantNode(previouslyRead);
	}

	private boolean startsCharacterConstant(Token token)
	{
		return token instanceof CharacterToken;
	}

	// integer (terminal)
	private ParseNode parseIntegerConstant()
	{
		if (!startsIntegerConstant(nowReading)) {
			return syntaxErrorNode("integer constant");
		}
		readToken();
		return new IntegerConstantNode(previouslyRead);
	}

	private boolean startsIntegerConstant(Token token)
	{
		return token instanceof IntegerToken;
	}

	// float (terminal)
	private ParseNode parseFloatConstant()
	{
		if (!startsFloatConstant(nowReading)) {
			return syntaxErrorNode("float constant");
		}
		readToken();
		return new FloatConstantNode(previouslyRead);
	}

	private boolean startsFloatConstant(Token token)
	{
		return token instanceof FloatToken;
	}

	// identifier (terminal)
	private ParseNode parseIdentifier()
	{
		if (!startsIdentifier(nowReading)) {
			return syntaxErrorNode("identifier");
		}
		readToken();
		return new IdentifierNode(previouslyRead);
	}

	private boolean startsIdentifier(Token token)
	{
		return token instanceof IdentifierToken;
	}

	// boolean constant (terminal)
	private ParseNode parseBooleanConstant()
	{
		if (!startsBooleanConstant(nowReading)) {
			return syntaxErrorNode("boolean constant");
		}
		readToken();
		return new BooleanConstantNode(previouslyRead);
	}

	private boolean startsBooleanConstant(Token token)
	{
		return token.isLextant(Keyword.TRUE, Keyword.FALSE);
	}

	private void readToken()
	{
		previouslyRead = nowReading;
		nowReading = scanner.next();
	}

	// if the current token is one of the given lextants, read the next token.
	// otherwise, give a syntax error and read next token (to avoid endless looping).
	private void expect(Lextant... lextants)
	{
		if (!nowReading.isLextant(lextants)) {
			syntaxError(nowReading, "expecting " + Arrays.toString(lextants));
		}
		readToken();
	}

	private ErrorNode syntaxErrorNode(String expectedSymbol)
	{
		syntaxError(nowReading, "expecting " + expectedSymbol);
		ErrorNode errorNode = new ErrorNode(nowReading);
		readToken();
		return errorNode;
	}

	private void syntaxError(Token token, String errorDescription)
	{
		String message = "" + token.getLocation() + " " + errorDescription;
		error(message);
	}

	private void error(String message)
	{
		TowheeLogger log = TowheeLogger.getLogger("compiler.Parser");
		log.severe("syntax error: " + message);
	}
}
